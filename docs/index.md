# Table of Contents

## Software

### [Development Stack](./developmentstack.md)

These are the software tools that you should have installed in order to

-   Run SIS queries
-   Generate dashboards
-   Contribute to the EdOps codebase

## Style and Procedural Guides

### [SQL Style Guide](./sqlstyleguide.md)

SQL forms the vast majority of the EdOps codebase, so it is crucial that we adhere to common standards.  These standards can and have evolved over time; if you have a suggestion post it to the [#data_code-manual](https://app.slack.com/client/T04EF3A0G/C016E112DN0) channel on Slack.

### Source Code Management

Standards for source code management. If you're unfamiliar with Git and GitLab, see Git Basics.

## Function Libraries

### [Spreadsheet Function Library](./spreadsheetfunctionlibrary.md)

All EdOps Data Specialists should be familiar with these common spreadsheet functions, as they are required in order to implement the procedures in the Spreadsheet Function Guide

### [Spreadsheet Function Guide](./spreadsheetfunctionguide.md)

How-to guides for common spreadsheet actions for which EdOps has developed standardized methods.

## Tutorials

### [How to Excel at Spreadsheets](./howtoexcelatspreadsheets.md)

Our internally- (and someday maybe externally-) facing spreadsheet course. Lessons cover the functions in the [Spreadsheet Function Library](./spreadsheetfunctionlibrary.md), and many of the use cases found in the [Spreadsheet Function Guide](./spreadsheetfunctionguide.md).

### Git With the Program

An introduction to Git for code management.  All EdOps source code should be stored in GitLab.
