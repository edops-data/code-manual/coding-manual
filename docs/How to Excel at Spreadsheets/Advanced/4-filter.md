# Filter Functions

Filter functions are the second life-altering set of functions we will encounter in this course. They are amazing for two main reasons:

1.  They filter things tables for future use, which hitherto required copy-pasting a filtered table to a new sheet
2.  They turn any formulas contained within into array formulas, eliminating the need for the clunky `ARRAYFORMULA()` notation to auto-fill data

## Introduction

On its face, the `FILTER()` function does exactly what it purports to do: it filters an array by one or more criteria, including criteria which are far more complex than would be otherwise obtainable using the user interface. (Such examples are left for a later date.)

Additionally, `FILTER()` addresses a main drawback of using `ARRAYFORMULA()` to auto-fill formulas: it permits the formula to automatically start and stop filling based on whether or not data is present.

## Syntax
The examples come from [4-Filter](https://docs.google.com/spreadsheets/d/1BG5jui6XT8Bws_sfn27eyQEi-McDGg8yjyrtjnxyPbI) on Google Drive. The worked-out version is available [here](https://docs.google.com/spreadsheets/d/1P_SJtgsm_ixP70tWZTnTqMispfDIx9R4Erhuz08zTzQ).

#### File Snapshot
| StoredGrades | S1_ELA | S1_Math | HSStudents | StudyHall | MAP_Raw | MAP_ELA | Untested | MSStudents | Courses | StudyBlock |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| ![Filter StoredGrades Tab](./images/Filter_StoredGrades.png) | ![Filter S1_ELA Tab](./images/Filter_S1_ELA.png) | ![Filter S1_Math Tab](./images/Filter_S1_Math.png) | ![Filter HSStudents Tab](./images/Filter_HSStudents.png) | ![Filter StudyHall Tab](./images/Filter_StudyHall.png) | ![Filter MAP_Raw Tab](./images/Filter_MAP_Raw.png) | ![Filter MAP_ELA Tab](./images/Filter_MAP_ELA.png) | ![Filter Untested Tab](./images/Filter_Untested.png) | ![Filter MSStudents Tab](./images/Filter_MSStudents.png) | ![Filter Courses Tab](./images/Filter_Courses.png) | ![Filter StudyBlock Tab](./images/Filter_StudyBlock.png) |

### Filtering an Array By One or More Conditions
Suppose we're interested in pulling all of the **Semester 1** grades from **English** courses.

Right now, the **StoredGrades** sheet contains _Quarter 1_ and _Quarter 2_ grades, in addition to the _Semester 1_ grades that we're after. Conceivably, we could just filter the table and delete those rows, but if we ever have to update the **StoredGrades** sheet, then we would have to do it all over again.

Instead, we can use the `FILTER()` function to save some effort.

#### Example A: `S1_ELA!A2`

![Filter S1_ELA!A2](./images/Filter_S1_ELAA2.png){: style="width:400px"}

The formula in `S1_ELA!A2` reads as follows:

```
=filter(StoredGrades!A:O, StoredGrades!M:M="S1", StoredGrades!G:G="ENG")
```

The three arguments are as follows:

-   `StoredGrades!A:O` is the array that we are filtering
-   The remaining two arguments define the filters:
    -   `StoredGrades!M:M` is the column containing the `StoreCode`, and we only want rows where it is equal to `S1`
    -   `StoredGrades!G:G` is the column containing the `Credit_Type`, and we only want rows where it is equal to `ENG`

!!! hint "Syntax"

    Unlike `COUNTIFS()`, `SUMIFS()`, and related functions, `FILTER()` does not take its arguments as comma-delimited pairs. To state that Column `A:A` must equal `19`, you can enter `A:A=19` rather than `A:A, 19`.

---

#### Example B: `S1_Math!A2`

![Filter S1_Math!A2](./images/Filter_S1_MathA2.png){: style="width:300px"}

The formula in `S1_Math!A2` reads as follows:

```
=filter(StoredGrades!A:O, StoredGrades!M:M="S1", StoredGrades!G:G="MAT")
```

The three arguments are:

-   `StoredGrades!A:O` is the array that we are filtering
-   The remaining two arguments define the filters:
    -   `StoredGrades!M:M` is the column containing the `StoreCode`, and we only want rows where it is equal to `S1`
    -   `StoredGrades!G:G` is the column containing the `Credit_Type`, and we only want rows where it is equal to `MAT`

### Using `FILTER()` to auto-fill a formula

Now that we have our Semester 1 grades for English and Math on their respective sheets, it's time to use `VLOOKUP()` to attach them to students.

This provides us with an opportunity to explore another aspect of `FILTER()`: it converts most standard formulas into **array** formulas, capable of operating on multiple rows at once.

So far, we have been doing this with the `ARRAYFORMULA()` wrapper, which has two main drawbacks:

1.  We have to explicitly pass it the start and end rows, which means that if data is added or removed the formula won't automatically catch it.
2.  Any empty rows still get processed by the formula, requiring error suppression.

Both can be avoided by telling `FILTER()` to _only_ run the `VLOOKUP()` formula if there is a Student ID in Column `A`. To do so, we invoke the `ISNUMBER()` function, which is introduced below.

### `ISNUMBER()` Function: A Primer

The `ISNUMBER()` function does more or less what it says on the tin: if it is passed a number, it returns `TRUE`; otherwise, it returns `FALSE`.  Importantly, **numbers that are formatted as text will return `FALSE`**.

| _x_ | `ISNUMBER(x)` | Comments |
| --- | --- | --- |
| `21` |  `TRUE` | 21 is a number. |
| `3/7/2020` | `TRUE` | Dates are actually stored as numbers. |
| `TRUE` | `FALSE` | Booleans are not stored as numbers, but can be converted to them with `N()` |
| `Hello` | `FALSE` | Hello is not a number. |
| `'21'` | `FALSE` | This 21 was formatted as text. |

#### Example C: `HSStudents!I2`

![Filter HSStudents!I2](./images/Filter_HSStudentsI2.png){: style="width:400px"}

The formula in `HSStudents!I2` reads as follows:

```
=filter(iferror(vlookup(A:A, {S1_ELA!B:B, S1_ELA!J:J}, 2, false)), isnumber(A:A))
```

The two arguments to `FILTER()` are:

-   `iferror(vlookup(A:A, {S1_ELA!B:B, S1_ELA!J:J}, 2, false))` is a standard call to `VLOOKUP()`.  If this is unfamiliar, consult [Basic Lookup](./basiclookup.md).
-   `isnumber(A:A)` which tells `FILTER()` to keep only rows which contain numbers in the `Student_Number` column.

---

#### Example D: `HSStudents!J2`

![Filter HSStudents!J2](./images/Filter_HSStudentsJ2.png){: style="width:400px"}

The formula in `HSStudents!J2` reads as follows:

```
=filter(iferror(vlookup(A:A, {S1_Math!B:B, S1_Math!J:J}, 2, false)), isnumber(A:A))
```

The two arguments to `FILTER()` are:

-   `iferror(vlookup(A:A, {S1_Math!B:B, S1_Math!J:J}, 2, false))` is a standard call to `VLOOKUP()`.  If this is unfamiliar, consult [Basic Lookup](./basiclookup.md).
-   `isnumber(A:A)` which tells `FILTER()` to keep only rows which contain numbers in the `Student_Number` column.

### Using `FILTER()` With an `OR` Operator

All of our filters so far have implicitly had the `AND` operator on their criteria: for example we filtered by _Semester 1_ **and** _ELA_, or _Semester 1_ **and** _Math_.

In order to pull the students for Study Hall, though, we want students who failed Math **or** who failed English.

This is easy enough to accomplish using a minor change in syntax.

#### Example E: `StudyHall!A2`

![Filter StudyHall!A2](./images/Filter_StudyHallA2.png){: style="width:400px"}

The formula in `StudyHall!A2` reads as follows:

```
=filter({HSStudents!A:A, HSStudents!E:F, HSStudents!I:J}, (HSStudents!I:I="F")+(HSStudents!J:J="F"))
```

As before, this `FILTER()` call has three arguments: a source array and two criteria. However, this time the criteria are enclosed in parentheses and _added_ together, telling `FILTER()` that **either** condition is acceptable.

!!! hint "Why Addition?"

    Inside the spreadsheet engine, any `TRUE/FALSE` value gets converted to `1/0`, allowing them to be added or multiplied. When it's time to interpret the values, `0` is turned back into `FALSE` and any positive number is turned into `TRUE`.

    (Yes this is an oversimplification. Sue me.)

### Checking Existence Using `VLOOKUP()`

Often, we want to filter one set of data based on whether or not each element is present in another set (an _inner join_, if you will). To accomplish this, we can hijack the `VLOOKUP()` function. It works like this:

1.  If the key is _not_ found in the destination table, then `VLOOKUP()` throws an error, so
2.  We filter to reject(/keep) only values that _do(/not)_ throw an error from `VLOOKUP()`

#### Example F: `Untested!A2`

![Filter Untested!A2](./images/Filter_UntestedA2.png){: style="width:400px"}

The formula in `Untested!A2` reads as follows:

```
=filter({HSStudents!A:B, HSStudents!E:F, HSStudents!H:H}, isnumber(HSStudents!A:A), iserror(vlookup(HSStudents!A:A, MAP_Raw!O:O, 1, false)))
```

The first two arguments to `FILTER()` are the usual: an array, and confirming that the first column in the array is numeric. We will focus on the third argument: `iserror(vlookup(HSStudents!A:A, MAP_Raw!O:O, 1, false))`

We work from the inside out:

-   **`VLOOKUP(...)`** - attempts to look up each `Student_Number` in `MAP_Raw!O:O`. We specify only one column (and thus specify column `1`) because we're not actually seeking a corresponding value; only whether the value exists in the key column.
-   **`ISERROR(...)`** - returns `TRUE` if `VLOOKUP()` fails (i.e. the student _is not_ in the list) or returns `FALSE` if `VLOOKUP()` succeeds (i.e. the student _is_ in the list)

Thus, we are filtering for instances where `VLOOKUP()` _fails_, meaning that the student _was not_ in the MAP roster and ergo was not tested.

### Sorting

Having successfully abstracted arrays of data from their source layout, why not manipulate it further? The `SORT()` function lets us reorder an array without affecting the source data.

#### Example G: `StudyBlock!A2`

![Filter StudyBlock!A2](./images/Filter_StudyBlockA2.png){: style="width:400px"}

The formula in `StudyBlock!A2` reads as follows:

```
=sort(filter({Courses!A:B, vlookup(Courses!A:A, {MSStudents!A:A, MSStudents!H:H}, 2, false), Courses!H:H, Courses!C:D, Courses!N:N}, left(Courses!H:H, 2)="SB"), 3, true, 2, true)
```

What a monster! Fortunately we can take it in chunks. Starting from the `FILTER()` function:

-   **`FILTER(...)`** just filters a gigantic array (one which contains a `VLOOKUP()` call) based on whether the leftmost two characters of `Courses!H:H` equals `SB`. If you've made it this far, that shouldn't be an issue.
-   **`SORT(FILTER(...), 3, true, 2, true)`** proceeds to sort the filtered data, first by the third column ascending, and then by the second column, ascending.

!!! note "Column References in `SORT()`"

    You can sort by a column _number_ (as we did in this example) or by a column _name_ (e.g. `Courses!C:C`), as long as you have **not** applied a filter (i.e. as long as the entire column can be referenced in the sort). Since we are sorting a filter, we must use the numeric references.

## Practice

1.  Make a copy of the [Filter-Practice](https://docs.google.com/spreadsheets/d/1hhZpQRaSo33PZ-45I_S7-TThE8wFKxBHoXTEwDzqlSI) workbook, which contains:
    -   The stored grades of all students at _Washington Elementary School_, and
    -   Their Winter MAP scores in both Reading and Mathematics.
2.  On the **ELA_Q2** sheet, filter the **StoredGrades** sheet by _Credit\_Type_ and _StoreCode_ to isolate the `ELA` grades from `Q2`. Make sure to only pull the appropriate columns, in the right order.
3.  On the **MAT_Q2** sheet, repeat the process to pull the `MAT` grades from `Q2`.
4.  On the **Students** sheet, create a list of students by pulling the _unique_ values for the appropriate columns from the **StoredGrades** sheet. Additionally, you should sort your data, alphabetically by grade (i.e. _Grade 0 A-Z_, _Grade 1 A-Z_...)
5.  On the same sheet, look up each student's ELA and Math grades from the **ELA_Q2** and **MAT_Q2** sheets, respectively. **Note** that you should do this with a _single_ formula per column, using a `FILTER()` function to auto-fill the lookup.
6.  On the **Intervention** sheet, filter the **Students** sheet to show all students who are failing _either_ ELA or Math for Quarter 2.
7.  On the **MAP_ELA** sheet, filter the data on the **MAP_Raw** sheet to include only `Language Arts` records.
8.  On the **Intervention** sheet, populate the _ELA\_RIT_ column with each student's RIT score from the filtered **MAP** sheets.
    -   **Bonus**: if you're cunning, you can skip step 7 entirely and do the filtration inside the `VLOOKUP()` call.

### Solution
When you're ready to check out the solution, it's [here](https://docs.google.com/spreadsheets/d/1gypn1lI71httIR_OgyJ3seBvRprQs02u8Y878XpS2IM).

## Official Documentation
### `FILTER()`
Official documentation on `FILTER()` from [Google](https://support.google.com/docs/answer/3093197?hl=en)

### `ISERROR()`
Official documentation on `ISERROR()` from [Google](https://support.google.com/docs/answer/3093349?hl=en)

### `SORT()`
Official documentation on `SORT()` from [Google](https://support.google.com/docs/answer/3093150?hl=en&ref_topic=3105422)
