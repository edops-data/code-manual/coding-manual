# Advanced Lookup

The lookup functions, while powerful, do have their limitations. The key column, for example, needs to be the leftmost column in the destination array, and there's no built-in way to look up on multiple criteria. Fortunately, these limitations can be easily surmounted with arrays and a little cleverness.

## Introduction

Back in the module on [arrays](./1-arrays.md), we already saw one way that arrays improve the lookup functions, by keeping only the key and value columns and eliminating all intervening columns. In this module, we will use the same concept to perform lookups when the key is to the right of the data. We will also use arrays to return multiple values from a single lookup function. Finally, through the use of our friend the concatination function, we will generate multi-factor key columns that permit lookups on multiple criteria.

## Syntax
The examples come from [3-AdvancedLookup](https://docs.google.com/spreadsheets/d/1gJDWiABFhRtDQatLbaAERCMWCE9bjTnL62pO8nLpkR8) on Google Drive.

#### File Snapshot
| HSStudents | LiveGrades | Sections | StoredGrades | MSStudents | WinterMAP | MAPConcordance |
| --- | --- | --- | --- | --- | --- | --- |
| ![AdvancedLookup HSStudents Tab](./images/AdvancedLookup_HSStudents.png) | ![AdvancedLookup LiveGrades Tab](./images/AdvancedLookup_LiveGrades.png) | ![AdvancedLookup Sections Tab](./images/AdvancedLookup_Sections.png) | ![AdvancedLookup StoredGrades Tab](./images/AdvancedLookup_StoredGrades.png) | ![AdvancedLookup MSStudents Tab](./images/AdvancedLookup_MSStudents.png) | ![AdvancedLookup WinterMAP Tab](./images/AdvancedLookup_WinterMAP.png) | ![AdvancedLookup MAPConcordance Tab](./images/AdvancedLookup_MAPConcordance.png) |

### Lookups on Multiple Keys

The lookup functions have no built-in way of looking up on multiple keys (i.e. multiple criteria). However, by concatenating multiple key columns we can create our own mega-keys that uniquely define data based on multiple criteria.

#### Example A: `HSStudents!G2:H2`

![AdvancedLookup HSStudents!G2](./images/AdvancedLookup_HSStudentsG2.png){: style="width:3400px"}

The formula in `HSStudents!G2` reads as follows:

```
=B2&"|"&F2
```

This formula simply **concatenates** (i.e. joins) the two keys on which we want to look up data: the `Student_Number` and the `Course_Number`.

!!! note "Delimiters"

    Note that we combined the two cells using a **delimiter**, in this case the pipe character (`|`). It is **crucial** that you use a delimiter between your keys, since otherwise they can combine in unpredictable ways.

    In this course, our delimiter of choice is the pipe. The reason for this is twofold: first, the pipe character, unlike characters such as commas and semicolons, pretty much never shows up in text on its own. Second, the pipe carries no significance in spreadsheet formulas, meaning it's a "safe" character to use.


!!! warning "Corresponding Key Column"

    We must also create a corresponding **key column** in our **destination sheet**. This is accomplished in `StoredGrades!A:A`.
    ![AdvancedLookup StoredGrades!A2](./images/AdvancedLookup_StoredGradesA2.png){: style="width:150px"}


![AdvancedLookup HSStudents!H2](./images/AdvancedLookup_HSStudentsH2.png){: style="width:400px"}

The formula in `HSStudents!H2` reads as follows:

```
=arrayformula(vlookup(G2:G, {StoredGrades!A:A, StoredGrades!I:I}, 2, false))
```

The `ARRAYFORMULA()` call is used to auto-fill the formula, per the contents of [1-Arrays](./1-arrays.md) and we will skip it for now, focusing instead on the `VLOOKUP()` function within.

The `VLOOKUP()` function accepts four arguments:

-   **`G2:G`** - our hybrid key column
-   **`{StoredGrades!A:A, StoredGrades!I:I}`** - an array formed by our key and value columns. If you're unsure about this syntax, see [Example G in 1-Arrays](./1-arrays.md#example-g-ms_studentsi2).
-   **`2`** - since our array is only two columns wide - key and value - we look up Column `2`
-   **`false`** - signifies an exact match

In practice, the use of our hybrid key columns permit us to look up on the _combination_ of `Student_Number` and `Course_Number`, returning each student's English grade.

### Lookups when the key is to the right of the value

By creating a custom array inside our lookup call, we can effectively rearrange the columns such that our key column - no matter where it resides in relation to the value column - appears first.

#### Example B: `HSStudents!I2`

![AdvancedLookup HSStudents!I2](./images/AdvancedLookup_HSStudentsI2.png){: style="width:400px"}

The formula in `HSStudents!I2` reads as follows:

```
=arrayformula(vlookup(G2:G&"|"&"Q2", {LiveGrades!J:J, LiveGrades!D:D}, 2, false))
```

As before, we ignore the `ARRAYFORMULA()` clause and focus on the `VLOOKUP()` clause. Likewise, as in [Example A](#example-a-hsstudentsh2), we must create a corresponding key column in `LiveGrades!J:J`.

The `VLOOKUP()` function accepts four arguments:

-   **`G2:G&"|"&Q2`** - instead of creating a separate helper column, we perform the concatenation _inside the lookup function_. This does require the use of an `ARRAYFORMULA()` wrapper; see the note below.
-   **`{LiveGrades!J:J, LiveGrades!D:D}`** - an array formed by our key and value columns
-   **`2`** - our value column is the second of the two columns in the array
-   **`false`** - signifies an exact match

!!! note "Forming Keys Inside the Lookup Function"

    When we form the hybrid key inside the lookup function, we are acting on an entire column (`G2:G`) - that is, we are operating on an **array**. As such, the lookup function must be wrapped in `ARRAYFORMULA()` so that the spreadsheet engine knows to operate on the entire column. Omitting the `ARRAYFORMULA()` wrapper will result in only cell `G2` receiving a lookup.


In practice, this function performs the lookup on multiple keys (our hybrid `Student_Number`/`Course_Number` key, plus the `Q2` identifier), while using a key column placed to the _right_ of the data. This means that the key column does not need to interfere with the placement of the source data, allowing for easy copy-pasting of updated data.

### Returning multiple values

So far, we have used arrays to define our **destination table**. We can also use arrays to define the third parameter in the lookup function, namely the value column(s) being referenced.

#### Example C: `LiveGrades!H2`

![AdvancedLookup LiveGrades!H2](./images/AdvancedLookup_LiveGradesH2.png){: style="width:400px"}

The formula in `LiveGrades!H2` reads as follows:

```
=arrayformula(vlookup(B2:B, {Sections!D:D, Sections!A:A, Sections!B:B}, {2, 3}, false))
```

While the `ARRAYFORMULA()` clause is essential to the operation of the function (see note in [Example B](#example-b-hsstudentsi2)), we will disregard it for now and focus on the `VLOOKUP()` clause.

The `VLOOKUP()` function accepts four arguments:

-   **`B2:B`** - our key column, in this case the `SectionID`
-   **`{Sections!D:D, Sections!A:A, Sections!B:B}`** - our destination table, formed by combining three columns: one key column and two value columns
-   **`{2,3}`** - an array specifying that we want values from **both** columns `2` and `3`
-   **`false`** - specifies an exact match

In practice, this function returns **two columns** worth of data: the `Course_Number` from `Sections!A:A` (column `2` in the array) and the `Course_Name` from `Sections!B:B` (column `3` in the array).

!!! note "Separating Columns"

    Could we have just specified `Sections!A:B`? Absolutely. They were separated here just to make clear that we are creating a 3-column array.

---

### Example D: `MSStudents!H2`

![AdvancedLookup MSStudents!H2](./images/AdvancedLookup_MSStudentsH2.png){: style="width:400px"}

The formula in `MSStudents!H2` reads as follows:

```
=arrayformula(iferror(vlookup(A2:A230, {WinterMAP!O:O, WinterMAP!U:U}, 2, false)))
```

This lookup functions similarly to [Example A](#example-a-hsstudentsg2h2), so we will not go into the details of the lookup function. Instead, we focus on the `IFERROR()` function.

In this example, not every student will have a MAP score. In such instances, the `VLOOKUP()` will fail and return a `#N/A` error, which is ugly. Instead, we would prefer to suppress the error and leave a blank cell. The `IFERROR()` function does just that.

Importantly, the `IFERROR()` function belongs _outside_ the `VLOOKUP()` call and _inside_ the `ARRAYFORMULA()` call. This is so that `IFERROR()` suppresses errors from `VLOOKUP()`, but not from `ARRAYFORMULA()`.

!!! hint "`IFERROR()` Placement"

        Placing `IFERROR()` _outside_ of `ARRAYFORMULA()` would direct it to only suppress errors related to the filling-down of the `VLOOKUP()`, not with the `VLOOKUP()` itself.


### Two-dimensional lookup with `INDEX()` and `MATCH()`

!!! note "`INDEX()`/`MATCH()` Fundamentals"
    For a detailed rundown of how `INDEX()` and `MATCH()` function, see [2-Lookups](../Intermediate/2-lookups.md#indexmatch) from the Intermediate course.

For all the utility of our souped-up `VLOOKUP()` functions, sometimes you need to navigate an array not just on two criteria, but in two _dimensions_ - horizontally and vertically. For this, we need to rely on `INDEX()` and `MATCH()`.

!!! warn "Auto-Filling"

    Unfortunately, `INDEX()` does not auto-fill, even with an `ARRAYFORMULA()` wrapper. Alas.

#### Example E: `MSStudents!I2`

![AdvancedLookup MSStudents!I2](./images/AdvancedLookup_MSStudentsI2.png){: style="width:400px"}

The formula in `MSStudents!I2` reads as follows:

```
=iferror(index(MAPConcordance!$A$1:$F$1, match(H2, index(MAPConcordance!$A$2:$F$8, match(G2, MAPConcordance!$A$2:$A$8, 0), 0), 1)))
```

We will ignore the `IFERROR()` clause and focus instead on the `INDEX()` function within.

!!! note "Not Exactly 2-Dimensional"

    Strictly speaking, this is not a 2-dimensional use of `INDEX()` since we are only navigating horizontally. However, by nesting `INDEX()` and `MATCH()` we are able to, in effect, navigate two dimensions so the term stays.

A simplified view of the `INDEX()` function is:

```
INDEX(MAPConcordance!$A$1:$F$1, [horizontal offset])
```

where our **horizontal offset** is defined by another complex formula. In practice, this means that we are navigating the row containing _performance levels_ by some amount to be determined by a combination of the student's _grade level_ and _RIT score_.

We now examine the `MATCH()` function:

```
match(H2, index(MAPConcordance!$A$2:$F$8, match(G2, MAPConcordance!$A$2:$A$8, 0), 0), 1))
```

which simplifies to

```
MATCH(H2, [array], 1)
```

The three arguments are:

-   **`H2`** - the key being matched; in this case, the student's _RIT score_.
-   **`[array]`** - The array against which we are matching the RIT score, which will be examined later.
-   **`1`** - specifies an **inexact match**, i.e. the largest value that is less than the key.

!!! note "Inexact Matches"

    In order for the inexact match to work, our data needs to be in **ascending order**. Fortunately, it is.

Finally, we examine the array inside the `MATCH()` function, which doesn't look like an array at all. Read on:

```
index(MAPConcordance!$A$2:$F$8, match(G2, MAPConcordance!$A$2:$A$8, 0), 0)
```

Believe it or not, this is an array. How? Consider the `INDEX()` function:

```
INDEX([array], [vertical offset], [horizontal offset])
```

In this instance, our horizontal offset is `0`, which signals to `INDEX()` to return _all columns_. In essence, we have returned an array!

The vertical offset, meanwhile, is determined by the innermost `MATCH()` function:

```
match(G2, MAPConcordance!$A$2:$A$8, 0)
```

The three arguments are:

-   **`G2`** - the key being matched; in this case, the student's _Grade Level_
-   **`MAPConcordance!$A$2:$A$8`** - the destination column containing grade levels
-   **`0`** - specifies an exact match

Let's rebuild the function from the inside out:

1.  The innermost `MATCH()` function identifies the _row_ containing the student's _Grade Level_.
2.  The innermost `INDEX()` function returns that _entire row_ by using a horizontal offset of `0`
3.  The outermost `MATCH()` function traverses that row for the nearest value to the student's _RIT Score_
4.  The outermost `INDEX()` function traverses the header row to return the corresponding _Performance Level_

## Practice

1.  Make a copy of the [AdvancedLookup-Practice](https://docs.google.com/spreadsheets/d/1zHxS7zwb6Eycz0FzSeOd7I7SsCl8i_M_uffDPdHeEzw) workbook, which contains:
    -   A roster of all students at _Cherry Hill Middle School_,
    -   Their class schedules,
    -   Their Quarter 2 grades,
    -   The school's grading scale,
    -   Their ELA scores from the Winter MAP administration
2.  On the **Students** sheet, populate the _HomeRoomKey_ column with a key that will help you look up each student's Homeroom teacher from the **Classes** sheet. (**Hint:** Homeroom is defined as the class with `External_Expression = HR(A)`.)
3.  On the **Classes** sheet, populate the _Key_ column with a key that matches the syntax of your key from Step 2.
4.  On the **Students** sheet, look up each student's Homeroom teacher using a `VLOOKUP()` call and the key columns you created in Steps 2-3.
5.  On the same sheet, look up the teacher with whom each student took the Winter MAP exam by returning the appropriate value from the _Teacher_ column on the **WinterMAP** sheet. Your lookup must be structured such that removing unnecessary columns from the **WinterMAP** sheet will not interfere with the formula's operation.
6.  On the **PGFinalGrades** sheet, look up the corresponding _Grade_ and _GradePoints_ from the **GradeScale** sheet for each item in the _Percent_ column. **Note:** you must do this with a single formula per row, i.e. your formula must pull both the _Grade_ and _GradePoints_ values simultaneously.

### Solution
When you're ready to check out the solution, it's [here](https://docs.google.com/spreadsheets/d/1zd2rv_wSv0KGYQdOiKB_YgV2GoKTgeArLFp8ndpTIAw).

## Official Documentation
### `VLOOKUP()`
Official documentation on `VLOOKUP()` from [Google](https://support.google.com/docs/answer/3093318?hl=en)

### `HLOOKUP()`
Official documentation on `HLOOKUP()` from [Google](https://support.google.com/docs/answer/3093375?hl=en)

### `INDEX()`
Official documentation on `INDEX()` from [Google](https://support.google.com/docs/answer/3098242?hl=en)

### `MATCH()`
Official documentation on `MATCH()` from [Google](https://support.google.com/docs/answer/3093378?hl=en&ref_topic=3105472)

### `IFERROR()`
Official documentation on `IFERROR()` from [Google](https://support.google.com/docs/answer/3093304?hl=en)

### `ARRAYFORMULA()`
Official documentation on `ARRAYFORMULA()` from [Google](https://support.google.com/docs/answer/3093275?hl=en)
