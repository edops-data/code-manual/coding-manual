# Date and Time Functions

Dates and times are finicky. They look like text, they're stored like numbers, and due to their cyclical nature are hard as the dickens to work with. Fortunately, spreadsheet software comes with a convenient set of functions for handling them.

## Introduction

As mentioned above, dates and times are actually stored as **numbers**. Specifically:

-   Dates are stored as the _number of days that have elapsed since January 1, 1900_.
-   Times are stored as the _number of seconds that have elapsed since midnight, divided by 86,400_.

That way, a date/time combination (e.g. `2:04 PM, April 26, 2021`) can be represented by a single number (`44312.59`). While this is super-convenient to a computer, it's not of much utility to a human being. So we find ourselves relying on a collection of date and time parsing functions to do the heavy lifting for us.

## Syntax
The examples come from [2-DateTime](https://docs.google.com/spreadsheets/d/1EWzA7hiFIQI2TBr23JP82tz6y2aMNIveGlqcjmUzey) on Google Drive. The worked-out version is available [here](https://docs.google.com/spreadsheets/d/1HSW9E366NR8Ttj6u-tKbhd-phH5iG4X8rJ7Ds-0icAw).

#### File Snapshot
| Students | Staff | Clock | Calendar | Holidays | OutOfSessionDays |
| --- | --- | --- | --- | --- | --- |
| ![DateTime Students Tab](./images/DateTime_Students.png) | ![DateTime Staff Tab](./images/DateTime_Staff.png) | ![DateTime Clock Tab](./images/DateTime_Clock.png) | ![DateTime Calendar Tab](./images/DateTime_Calendar.png) | ![DateTime Holidays Tab](./images/DateTime_Holidays.png) | ![DateTime OutOfSessionDays Tab](./images/DateTime_OutOfSessionDays.png) |

### `TEXT()`

The `TEXT()` function takes values that are stored as numbers (including dates and times!) and renders them as text in a format dictated by the user. Functionality is best shown by example:

| `x` | Function | Result |
| --- | --- | --- |
| October 4, 2021 | `TEXT(x, "M")` | `10` |
| October 4, 2021 | `TEXT(x, "DD")` | `04` |
| October 4, 2021 | `TEXT(x, "MMM D")` | `Oct 4` |
| 6:05 PM | `TEXT(x, HH:MM)` | `18:05` |
| 1278 | `TEXT(x, 00000.0)` | `01278.0` |
| 1278 | `TEXT(x, "#,##0")` | `1,278` |

While `TEXT()` can operate on any numeric value, we will focus only on dates here.

#### Common Date Masks

!!! note Terminology

    **Mask** is the technical term for the second parameter of the `TEXT()` function - i.e. the text that determines the format of the output.

Consider the input `March 4, 2021`:

| Mask | Output |
| --- | --- |
| `M` | `3` |
| `MM` | `03` |
| `MMM` | `Mar` |
| `MMMM` | `March` |
| `D` | `4` |
| `DD` | `04` |
| `DDD` | `Thu` |
| `DDDD` | `Thursday` |
| `Y` / `YY` | `21` |
| `YYYY` | `2021` |

#### Example A: `'Students'!H2`

![DateTime Students!H2](./images/DateTime_StudentsH2.png){: style="width:300px"}

The formula in `Students!H2` reads as follows:

```
=text(D2, "MMMM")
```

This formula extracts the month name from the date in `D2`, in this case `December`.

---

#### Example B: `Students!I2`

![DateTime Students!I2](./images/DateTime_StudentsI2.png){: style="width:300px"}

The formula in `Students!I2` reads as follows:

```
=text(D2, "D")
```

This formula extracts the date, without leading zeroes, from the date in `D2`, in this case `8`.

### `DATEDIF()`

Calculating the elapsed time between dates should be as easy as subtracting them. And, if all you're interested in is the number of days, that's all there is to it. Anything more complicated, though, and you're up against the different lengths of the months, leap years, and goodness knows what else. `DATEDIF()` to the rescue!

`DATEDIF()` produces the duration between two dates in whatever set of units you choose, accounting for the lengths of months and years along the way.

#### Example C: `Students!J2`

![DateTime Students!J2](./images/DateTime_StudentsJ2.png)

The formula in `Students!J2` reads as follows:

```
=datedif(D2, today(), "Y")
```

`DATEDIF()` expects three arguments:

-   **`D2`** - the start date, in this case the student's date of birth
-   **`TODAY()`** - today, handled by a function
-   **`"Y"`** - indicates that we want the duration in years, rounded down. We could have specified **D**ays, **W**eeks, or **M**onths instead.

### Manipulating Dates by Part

#### Constructing Dates with `DATE()`
While Excel and GSheets are pretty good at identifying dates when typed in (perhaps _too_ well, in Excel's case), there needs to be a way of programmatically generating them as well. For that, we have the `DATE()` function.

`DATE()` accepts three arguments: the year, month, and day, in that order.

#### Extracting Date Parts With `YEAR()`, `MONTH()`, and `DAY()`

Inversely, given a date object, we can extract its component parts using `YEAR()`, `MONTH()`, and `DAY()`. Each returns the numeric value of that portion of the date.

Using these two sets of functions we can construct dates that have a particular relation to one another.

#### Example D: `Students!K2`

![DateTime Students!K2](./images/DateTime_StudentsK2.png){: style="width:400px"}

The formula in `Students!K2` reads as follows:

```
=date(year(D2)+18, month(D2), day(D2))
```

The `DATE()` function accepts three arguments:

-   **`YEAR(D2)+18`** - the year from the student's date of birth (`D2`), plus 18 years
-   **`MONTH(D2)`** - the student's birth month, as extracted from their date of birth
-   **`DAY(D2)`** - the student's birth day, as extracted from their date of birth

In practice, this results in a date with the same month and day as the student's birthdate, incremented by exactly 18 years.

### `DATEVALUE()`

The quartet of `DATE()`, `YEAR()`, `MONTH()`, and `DAY()` work great as long as the date is being formed using numbers. Often, though, dates come to us as text. Fortunately the misery of text parsing is handled by the `DATEVALUE()` function.

`DATEVALUE()` takes a date represented as text (as long as it's one of the conventionally-accepted date formats) and converts it into a numeric date object for further processing. It accepts a single argument: the string that we hope can be formatted as a date.

#### Example E: `Holidays!C2`

![DateTime Holidays!C2](./images/DateTime_HolidaysC2.png){: style="width:300px"}

The formula in `Holidays!C2` reads as follows:

```
=datevalue(A2)
```

The function takes a single parameter: the text-formatted date in `A2`. Because this date adopted a standard format, `DATEVALUE()` was able to recognize it and convert it into a date object.

### `NETWORKDAYS()`
Frequently, when we are counting the number of elapsed days, we are implicitly interested in the number of _business_ days instead of purely calendar days. Holidays only complicate matters further. Such situations are precisely why the `NETWORKDAYS()` function (think net work-days, _not_ network days) exists.

`NETWORKDAYS()` accepts three arguments:

-   The start date
-   The end date
-   An array of dates that should be _excluded_ from the count. By default, weekends are also excluded.

#### Example F: `Students!L2`

![DateTime Students!L2](./images/DateTime_StudentsL2.png){: style="width:400px"}

The formula in `Students!L2` reads as follows:

```
=networkdays(F2, G2, OutOfSessionDays!$A$2:$A)
```

As mentioned, `NETWORKDAYS()` accepts three arguments:

-   **`F2`** - the start date, in this case the student's Entry Date
-   **`G2`** - the end date, in this case the student's Exit Date
-   **`OutOfSessionDays!$A$2:$A`** - the out-of-session days that should be excluded from the count

In practice, this formula tells us that Jenny Adams was enrolled for `183` _school days_, even though `292` _calendar_ days had elapsed.

### Calculating Time Intervals

Unfortunately, there are no analogs for `DATEDIF()` or `NETWORKDAYS()` for time. Instead, we have to rely on good old-fashioned arithmetic. Strap in.

There are `60*60*24=86400` seconds in a day. Based on this fact, spreadsheet software stores times as the number of seconds elapsed since midnight, divided by `86400`. That is, midnight corresponds to `0.0` and `11:59:59 PM` corresponds to `1.0` (ignoring the date).

If we want to compare times, then, we must perform our arithmetic and then multiply by the appropriate number in order to get the units of choice:

| To get... | Multiply by... |
| --- | --- |
| Hours | 24 |
| Minutes | 1440 |
| Seconds | 86400 |

#### Example G: `Clock!G2`

![DateTime Clock!G2](./images/DateTime_ClockG2.png){: style="width:400px"}

The formula in `Clock!G2` reads as follows:

```
if(not(isblank(F2)), 8, (E2-D2)*24)
```

The `IF()` statement simply states that, if the employee has a code in Column `F`, then they should be paid for a full 8 hours.

The remainder of the function (`(E2-D2)*24`) indicates that we want to know the amount of time elapsed, in _hours_, between `D2` and `E2`. Multiplying by other numbers would return the number of minutes or seconds.

!!! note "Fractional Hours"

    This example produces **fractions of hours**; i.e. `7.44` is **not** 7 hours and 44 minutes, but rather 7 hours and 26 minutes.

    If you want to get your results in `H:MM` format, you have some additional arithmetic ahead.

## Practice

1.  Make a copy of the [DateTime-Practice](https://docs.google.com/spreadsheets/d/13-mcmxn2I-pNDJvKhgKgm8Todf7Y1I-KWd-DpcBspqk) workbook, which contains:
    -   A roster of all students at _Washington Elementary School_,
    -   The dates on which school was not in session during the 2021 school year,
    -   The Period Attendance data for the week of 11/2/2020
2.  On the **Students** sheet, populate the _Age on Sep. 30_ column with each student's age, in years, on `September 30, 2020`.
3.  On the same sheet, populate the _Current Age_ column with each student's current age, in years.
4.  On the **OutOfSessionDays** sheet, populate the _Date_ column with the dates given, formatted as date objects.
5.  On the **Students** sheet, populate the _Days Enrolled_ column with the number of instructional days that each student has been enrolled during the 2021 school year. An instructional day is defined as a
    -   Weekday
    -   Between the student's _Entry_ and _Exit_ dates (inclusive)
    -   That is not among the _Out-of-Session_ days
6.  On the same sheet, populate the _Truant Eligible?_ column with whether or not the student is eligible to be considered truant. A student is eligible to be considered truant if they are, as of _September 30 of the school year_, older than 5 years and younger than 18. If possible, have your formula return a pure `TRUE/FALSE` output.
7.  On the **PeriodAttendance** sheet, calculate the number of minute each student was present during each period, based on the _ClockIn/ClockOut_ values. (**Hint:** recall that times are stored as fractions of a day, incremented in seconds.)
8.  On the **DailyAttendance** sheet, populate the _TotalMinutes_ column with the total number of minutes each student was present per day.
9.  On the same sheet, populate the _Present_ column with whether or not the student was present for at least `288` minutes during that day. As before, if possible, have your formula return a pure `TRUE/FALSE` output.

### Solution
When you're ready to check out the solution, it's [here](https://docs.google.com/spreadsheets/d/1yoBE_VpfeWqYX-iBGekT3rvzwu_Fwhf9PPznjfg4RLc).

## Official Documentation
### `TEXT()`
Official documentation on `TEXT()` from [Google](https://support.google.com/docs/answer/3094139?hl=en)

### `DATEDIF()`
Official documentation on `DATEDIF()` from [Google](https://support.google.com/docs/answer/6055612?hl=en)

### `DATE()`
Official documentation on `DATE()` from [Google](https://support.google.com/docs/answer/3092969?hl=en)

### `YEAR()`
Official documentation on `YEAR()` from [Google](https://support.google.com/docs/answer/3093061?hl=en&ref_topic=3105385)

### `MONTH()`
Official documentation on `MONTH()` from [Google](https://support.google.com/docs/answer/3093052?hl=en&ref_topic=3105385)

### `DAY()`
Official documentation on `DAY()` from [Google](https://support.google.com/docs/answer/3093040?hl=en&ref_topic=3105385)

### `DATEVALUE()`
Official documentation on `DATEVALUE()` from [Google](https://support.google.com/docs/answer/3093039?hl=en&ref_topic=3105385)

### `NETWORKDAYS()`
Official documentation on `NETWORKDAYS()` from [Google](https://support.google.com/docs/answer/3092979?hl=en&ref_topic=3105385)

### `TIME()`
Official documentation on `TIME()` from [Google](https://support.google.com/docs/answer/3093056?hl=en&ref_topic=3105385)
