# Arrays and Array Literals

Arrays are what you get when you combine multiple cells (or rows, or columns). You're already familiar with arrays if you've ever taken a sum of a column - the column was the array, and out popped a single value.

Array functions open up a new world of possibilities - everything from operating on multiple cells at once to being able to dynamically rearrange data. In short, they'll change your life. Let's get started.

## Introduction

As alluded to, an **array** is spreadsheet-speak for a collection of cells, organized into rows and columns. Quite often, a formula takes in an array of cells (e.g. `SUM(A2:A16)`), but generally formulas only output a single cell. By contrast, an array function can take in a collection of cells and return _another_ collection of cells (for example, taking `LEFT(A2:A16)` and getting 15 cells back).

Additionally, arrays allow us to cut and paste large chunks of data without... well, cutting and pasting. Use cases include:

-   "gluing" smaller arrays together to form a larger array,
-   rearranging the rows/columns of an existing array into a more convenient format, or
-   "cutting" rows/columns out of an existing array to make it easier to manipulate

This doesn't seem terribly exciting since cutting and pasting perform these operations already. However, arrays permit us to perform these functions _dynamically_, so that we never need to manipulate the source data directly. As a result, if the source data changes, no steps need to be repeated.

## Syntax
The examples come from [ArrayLiterals](https://docs.google.com/spreadsheets/d/110GTHcVD7-2GobK1YezVJE3yo1hHfScyQ1L1qnrEo4M) on Google Drive. The worked-out version is available [here](https://docs.google.com/spreadsheets/d/1CmNaF9fwV2lWXdmdyl7o5hPK99_Q7kuPG009CvZNWhY).

#### File Snapshot
| Students | Students_Uniform | Roster | MS_Students | MAP_Math
| --- | --- | --- | --- | --- |
| ![ArrayLiterals Students Tab](./images/ArrayLiterals_Students.png) | ![ArrayLiterals Students_Uniform Tab](./images/ArrayLiterals_StudentsUniform.png) | ![ArrayLiterals Roster Tab](./images/ArrayLiterals_Roster.png) | ![ArrayLiterals MS_Students Tab](./images/ArrayLiterals_MSStudents.png) | ![ArrayLiterals MAP_Math Tab](./images/ArrayLiterals_MAPMath.png) |

### Commas Combine Columns
To attach columns (or arrays) to each other **horizontally**, separate them with commas.

In the **Students** sheet of the example file, grades 9, 10, and 12 have the `Student_Number` in the _leftmost_ column, while grade 11 has it in the _rightmost_ column.  Additionally, in grade 10 the `LastFirst` and `Grade_Level` columns are swapped.

In the **Students_Uniform** sheet, we want each grade's columns to appear identically.  This could conceivably be done with diligent copy-pasting, but if any data point on **Students** changed, the process would need to be repeated.  Alternatively, hundreds of cell formulas could be used to perform the processing, but again, if any rows were added to **Students** they might not appear on **Students_Uniform** unless the formulas were filled down with room to spare.

Using an array literal solves both problems, allowing us to perform the transformations with **four formulas**.

#### Example A: `'Students_Uniform'!A3`

![Array Literals Students_Uniform!A3](./images/ArrayLiterals_StudentsUniformA3.png){: style="width:300px"}

The formula in `'Students_Uniform'!A3` reads as follows:

```
={Students!A3:C}
```

!!! hint "Curly Braces Define an Array"

    An array is always surrounded by curly braces (`{}`), which denote that the resultant array is to be treated as a single object, indistinguishable from a regular range of cells.

    Arrays are recursive and can be composed of smaller arrays, each in turn defined by a pair of curly braces.

In this case, the columns were in the correct order so they were simply transferred to the target sheet.

**Note** that the starting point can be a specific cell, while the endpoint can be just a column. Doing this automatically expands the array to the end of the sheet.

---

#### Example B: `'Students_Uniform'!E3`

![Array Literals Students_Uniform!E3](./images/ArrayLiterals_StudentsUniformE3.png){: style="width:300px"}

The formula in `'Students_Uniform'!E3` reads as follows:

```
={Students!E3:E, Students!G3:G, Students!F3:F}
```

The columns are referenced in the order that we want them, separated by commas to indicate that they are to be joined **horizontally**.  The result is that the data is displayed in the order that we want.

---

#### Example C: `'Students_Uniform'!I3`


![Array Literals Students_Uniform!I3](./images/ArrayLiterals_StudentsUniformI3.png){: style="width:300px"}

The formula in `'Students_Uniform'!I3` reads as follows:

```
={Students!K3:K, Students!I3:J}
```

As before, the columns are referenced in the order that we want them, though since the last two columns are already in the correct order they can be referenced as a block.

### Semicolons Combine Rows
To attach rows (or arrays) to each other **vertically**, separate them with semicolons.

Suppose that, having cleaned up the data on **Students_Uniform**, we want to combine all four grades into a single mega-roster by "stacking" the four data sets on top of one another.  As before, this could be accomplished using copy-paste or a large number of cell formulas, but in both cases a lot of work would be wasted if even a tiny change or addition was made to the source data.

Instead, we can accomplish our goal with a **single formula**.

#### Example D: `Roster!A3`

![Array Literals Roster!A3](./images/ArrayLiterals_RosterA3.png){: style="width:400px"}

The formula in `Roster!A3` reads as follows:

```
={Students_Uniform!A3:C; Students_Uniform!E3:G; Students_Uniform!I3:K; Students_Uniform!M3:O}
```

The groups of columns are now stacked, one on top of another, using semicolons.

!!! warning "Dimensions"

    For this stacking to work, each array must be the same width - in this case, 3 columns.

!!! note "Whitespace"

    If you scroll down, you will notice significant amounts of whitespace. This is because each grade doesn't have the same number of students. This can be addressed easily enough, but doing so is outside the scope of this lesson.

### Arrays Inside Arrays
Rather than using the **Students_Uniform** sheet to get everything in the right order before forming our roster, we can perform the entire operation - column rearrangement and stacking - with a **single formula**.  Here's how.

#### Example E: `Roster!E3`

![Array Literals Roster!E3](./images/ArrayLiterals_RosterE3.png){: style="width:300px"}

The formula in `Roster!E3` reads as follows:

```
={{Students!A3:C};
  {Students!E3:E, Students!G3:G, Students!F3:F};
  {Students!K3:K, Students!I3:J};
  {Students!M3:O}}
```

Here, you can see each of the original arrays, with their columns joined _horizontally_ by _commas_, and surrounded by curly braces. Those smaller arrays are, in turn, joined _vertically_ by _semicolons_ in a larger array that is also enclosed with curly braces.

### `ARRAYFORMULA()`

The `ARRAYFORMULA()` function transforms many ordinary formulas into **array formulas**, i.e. formulas that output arrays instead of single values. This has the effect of writing a single function call for an entire array of data, rather than filling down separate formulas that must then be tracked. We will learn a more elegant way of doing this when we work with [Filters](./4-filters.md), but for now the `ARRAYFORMULA()` function will do nicely.

Consider Column `C` of the **MS_Students** sheet. We need to create a combined name (in _Last, First M_ format) for each student using the name parts provided in Columns `D:F`. Under normal circumstances, we would write the formula once, and then fill it down the column, creating 280 instances of it. Doing so carries several drawbacks, however:

-   If our list of students expands or contracts, we must remember to either fill down the formula to cover the additional data, or conversely, to delete extraneous references that will throw errors.
-   If we need to update the formula, we must fill that corrected formula down. Not a huge deal, but we've all forgotten to do so at least once.

Instead, we will use a single function call that:

-   Automatically expands or contracts to cover the available data
-   Requires only a single function call, ensuring that the same formula is applied to all cells

#### Example F: `MS_Students!C2`

![Array Literals MS_Students!C2](./images/ArrayLiterals_MSStudentsC2.png){: style="width:300px"}

The formula in `MS_Students!C2` reads as follows:

```
=arrayformula(trim(D2:D&", "&E2:E&" "&F2:F))
```

The argument to `ARRAYFORMULA()` is itself a formula that concatenates the various parts of each student's name. If this formula is unfamiliar, consult the module on [Text Functions](../Intermediate/4-text.md). The thing to notice is that, instead of accepting **cells** as arguments, the `TRIM()` function and the `&` operator are accepting _entire columns_ - i.e. **arrays** of data.

!!! note "Start Row"

    Note that these formulas start on Row `2` and then continue through the column. This is because we need to exclude the header row. When we start using Filter functions, we will be able to ignore this detail.

If you try entering just `trim(D2:D&", "&E2:E&" "&F2:F)` without the `ARRAYFORMULA()` wrapper, you'll find that the formula still works - albeit only on Row `2`. For the formula to auto-fill, we need to tell GSheets to run the formula row-by-row through an array. This is what `ARRAYFORMULA()` signifies.

#### Example G: `MS_Students!I2`

![Array Literals MS_Students!I2](./images/ArrayLiterals_MSStudentsI2.png){: style="width:300px"}

The formula in `MS_Students!I2` reads as follows:

```
=arrayformula(vlookup(B2:B, {MAP_Math!N:N, MAP_Math!T:T}, 2, false))
```

Here, we make use of arrays in two different manners. First, the `ARRAYFORMULA()` wrapper allows the `VLOOKUP()` to be performed on the entire column, much like the last example. But we also use an array literal _inside_ the `VLOOKUP()` call. Let's explore it.

Under normal circumstances, the `VLOOKUP()` call would have resembled:

```
vlookup(B2:B, MAP_Math!N:T, 7, false)
```

Doing so has two drawbacks:

1.  Counting is a pain. Since we only need columns `N:N` and `T:T`, why include all the faff between them and resort to counting columns? Granted, 7 columns isn't bad, but your worksheet could contain hundreds of columns and nobody has time for that.
2.  If any columns are inserted or removed from the array `MAP_Math!N:T`, the formula will cease to work because even though the column references will update, the `7` will not.

Instead, we use an **array literal** to include _only_ the columns that we're interested in: key column `MAP_Math!N:N` and value column `MAP_Math!T:T`. Since there are only two columns, we call the second of them with the argument `2`. Additionally, since there are only two columns being referenced, we can do anything we want to the intervening columns - add, remove, or otherwise - and the formula will continue to work.

### Errors

#### Mismatched Array Sizes

If the component arrays are not the same sizes (same number of rows if combining horizontally or same number of columns if combining vertically) then a `#VALUE` error will result.  (In this example, the last array contains four columns while the others each contain 3.)

![Array Literals #VALUE Error](./images/ArrayLiterals_ValueError.png){: style="width:400px"}

#### Collisions with Existing Content

If the array, when fully expanded, collides with existing non-empty cells, then a `#REF` error will result. (In this example, rows 4 and below were pasted while row 3 contains the array literal.)

![Array Literals #REF Error](./images/ArrayLiterals_RefError.png){: style="width:300px"}

## Practice

1.  Make a copy of the [ArrayLiterals-Practice](https://docs.google.com/spreadsheets/d/1T4pfIjoB-lUCA6KTS7J1yPCU9dP5XB0KFgC2yUqwApE) worksbook, which contains:
    -   A roster of all 8th grade students at _Cherry Hill Middle School_,
    -   A roster of all 9th-11th grade students at _Apple Grove High School_, and
    -   Their SY21 final GPAs.
2.  On the **Rising_9th** sheet, use the text-parsing functions (`LEFT()`, `RIGHT()`, `SEARCH()`, `LEN()`) to reconstruct each student's name in `Last, First` form. Bonus points if you do it using an array formula, but using one formula per row is perfectly acceptable in this instance.
3.  On the **Roster_2022* sheet, combine the Middle School and High School rosters into a single roster using array literals. While this can be done in one step, if you prefer to rearrange the columns before stacking them, placeholders are available on each of the subsidiary rosters.
4.  On the same sheet, populate the _SY21 GPA_ column with each student's SY21 GPA, as retrieved using a `VLOOKUP()` from the **GPA** sheet. **Note** that you should structure your `VLOOKUP()` call such that any addition or removal of columns will _not_ affect its output.

### Solution
When you're ready to check out the solution, it's [here](https://docs.google.com/spreadsheets/d/1fcSaqZmz90BM9i82tEoGo1dsLqe2p5eF7IvkYv0YVUQ).

## Official Documentation
### Array Literals
Official documentation on arrays from [Google](https://support.google.com/docs/answer/6208276?hl=en)
