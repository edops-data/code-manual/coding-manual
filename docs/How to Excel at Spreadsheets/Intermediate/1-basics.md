# Review of the Basics
This is a review of some basic Excel functionality; notably how to sort, filter, and format data through the user interface. (Almost) no formulas this time.

## Introduction

It's very seldom that you receive a data set in exactly the format that you want. Often things will be out of order, or the data will be formatted in a way that's not conducive to analysis. With a few basic Excel functions we can get data ready to analyze by:

-   **filtering** the data set to include only the values we're interested in
-   **sorting** the data set to improve readability
-   **converting** values from one type to another to make them easier to interact with

## Functions
The examples come from the [Basics](./assets/Basics.xlsx) Excel workbook. The worked-out version is available [here](./assets/Basics-Worked.xlsx).

#### File Snapshot
| CC |
| --- |
| ![Basics CC Tab](./images/Basics_CC.png) |

### Enabling Filtration

To enable filtering on a data set, click on the header row (in this case `Row 1`) to select it, and then click the **Filter** button in Excel's _Data_ ribbon. If successful, a series of small arrows will appear in each header column (see image).

![Enable Filtering](./images/Basics_EnableFilter.png)

!!! note "Selecting Rows/Columns"

    To select an entire row or column, click on that row/column's name (i.e. the gray cell containing either the row number or column letter).


### Filtering Data

Once filtering is enabled, you can filter the data set by one or more criteria by clicking on the arrows in the column headers. The most basic type of filter is one where you manually select which values you want to keep or hide.

#### Example A: `CC!D1`

![Basics Filter by Course](./images/Basics_FilterCourseType.png){: style="width:300px"}

In this case, we manually deselected the electives (_Band_ and _PE_), hiding those rows from view. If we wanted to select only a few courses, we could have unchecked **Select All** first, enabling us to check the courses we want, instead of unchecking those we don't.

!!! note "Hidden Rows"

    Filtering doesn't actually _remove_ data; it _hides_ it. You can tell that rows are filtered because their row numbers are now in blue; you can tell that data is hidden because the numbers are not necessarily consecutive (notice the skip from `Row 15` to `Row 17`).

    ![Basics Filtered Rows](./images/Basics_FilteredCourses.png){: style="width:400px"}

---

#### Example B: `CC!F1`

![Basics Filter by Date](./images/Basics_FilterDate.png){: style="width:300px"}

Depending on the type of data being filtered, there may be additional filtering options. For instance, `Column F` can be filtered using _date ranges_ rather than selecting individual dates.

In this case, we filtered for all dates _after_ `March 15, 2020`.

### Additional Filters

In addition to those shown in the above examples, some common types of filter include:

| Numbers | Dates | Text | All Data Types |
| --- | --- | --- | --- |
| Include/exclude individual elements | Include/exclude individual elements | Include/exclude individual elements | Include/exclude individual elements |
| Is equal to | Is equal to | Is equal to | Is equal to |
| Is not equal to | Is not equal to | Is not equal to | Is not equal to |
| Is/is not blank | Is/is not blank | Is/is not blank | Is/is not blank |
| Is greater/less than (or equal to) `x` | Is before/after `xx/yy/zzzz` | | |
| Is between `x` and `y` | Is between `xx/yy/zzzz` and `aa/bb/cccc` | | |
| | | Begins/ends with the string `x` | |
| | | Contains the string `y` | |

### Sorting Data

In addition to filtering data, it is often important to sort data for readability or for further processing. There are two ways to sort data, depending on the complexity of the sort.

#### Example C: `CC!B1`

![Basics Sort by Last Name](./images/Basics_SortLastName.png){: style="width:300px"}

If the data can be sorted based on the value of a single column, then the same arrow used for filtering can be used to sort the entire data set by that column.

In this case, we sorted the entire data set alphabetically by student name.

!!! warning "Operating on Filtered Data"

    When data is filtered out, it is **unaffected** by any additional operations. For example, if you fill a formula or value down a column, it will _skip_ any filtered rows. Likewise, if you apply formatting to a column, filtered rows will be unaffected.

    Sorting, by a column, however, affects **all rows**, hidden and visible.

#### Example D: `CC!B1`

![Basics Advanced Sort](./images/Basics_AdvancedSort.png){: style="width:300px"}

If the data need to be sorted based on values in multiple columns, then an _advanced sort_ is required. To perform such a sort, click the **Sort** button in the _Data_ ribbon. In the dialog, select the columns to sort by, in order of precedence.

In this example, we are sorting by Teacher name (`[5]LastFirst`), _followed_ by Student name (`[1]LastFirst`), resulting in an alphabetical listing of students per teacher.

![Basics Sorted Data](./images/Basics_SortedTeachers.png)

!!! warning "Sorting Filtered Data"

    While sorting by a single column **does affect** filtered rows, advanced sort **does not** filter them. This can lead to unexpected results, so you are generally best off sorting your data set **prior to filtering**.

    In this example, the author forgot that there was a filter removing all blank grades. As such, you will find the odd blank grade at an unusual location in the sort. Might as well make it a teachable moment.

### Formatting Data

Many data types such as dates and times can be represented in different ways (e.g. `July 1, 2020` vs. `7/1/2020`) without changing the underlying value. Likewise, numbers can be rounded or otherwise manipulated to make them easier to read without actually changing their values.

#### Example E: `CC!E:E`

![Basics Date Formats](./images/Basics_ISODate.png){: style="width:300px"}

From the _Home_ ribbon, there is a drop-down in the _Number_ section that provides common data types, all of which can be applied to a selected data set. More exotic formats can be found by choosing `More Number Formats` from the bottom of the drop-down, as we have done here.

In this example, we want the students' `DateEnrolled` to be formatted as European, `YYYY-MM-DD` dates instead of the American `MM/DD/YYYY`. If such an option is not available, you can enter it yourself. Applying this format keeps the date values identical from the eyes of the computer, but displays them differently for readability.

![Basics Formatted Dates](./images/Basics_FormattedDates.png){: style="width:300px"}

---

#### Example F: `CC!O:O`

![Basics Rounding](./images/Basics_Decimals.png){: style="width:300px"}

Similarly to formatting dates, numbers can be assigned different formats without changing the underlying value. To set the desired number of decimal places, click the _increase/decrease decimal_ buttons in the _Home_ ribbon until the desired number of places are shown.

In this example, we have chosen to display exactly two decimal places of each student's grade, rounding or zero-padding as necessary.

---

#### Example G: `CC!Q:Q`

![Basics Numbers Rendered as Text](./images/Basics_TextToNumber.png)

Sometimes, due to file formatting issues, a number is rendered as text. We, as humans, have no issue knowing that the digits `678` are the same as the value `six-hundred seventy-eight`, but to the computer they are different and incompatible. Excel will indicate when it suspects a value has been cast to the wrong type with a green tick mark.

This can be addressed in either of two ways:

-   Click the green tick mark and choose _Convert to Number_.
-   Create a new column in which you multiply the text value by `1`, converting it into a number.

!!! note "Creating a New Column"

    One of the doctrines of this course is that we **never modify source data** on the grounds that, if it is ever replaced, all of the same machinations will have to be repeated. In this case, we can assume that, if the data is refreshed, the numbers will be cast as text yet again. Therefore, the official course policy is to create a new column, multiply by one, and use that new, numeric column thereafter.

## Practice

1.  Download the [Session 1 Practice](./assets/Basics-Practice.xlsx) workbook, which contains a roster of every enrollment at _Apple Grove High School_, past and present.
2.  **Format** the `EntryDate` and `ExitDate` columns to the ISO format (i.e. `YYYY-MM-DD`).
3.  **Format** the GPA to display 2 decimal places.
4.  **Filter** the data set to include only students who:
    -   Are currently enrolled (i.e. `Enroll_Status=0`)
    -   Have an `EntryDate` _after_ August 20, 2020
    -   Are _missing_ a Homeroom
5.  **Sort** the data to be alphabetical by grade (i.e. _9th grade A-Z_, then _10th grade A-Z_...)
    -   This is most easily done using the `LastFirst` column, but if you want to be fancy you can use the individual `Last_Name`, `First_Name`, and `Middle_Name` columns.

### Solution
When you're ready to check out the solution, it's [here](./assets/Basics-Solution.xlsx).

## Official Documentation
### Sorting and Filtering Data
Official documentation on sorting and filtering from [Google](https://support.google.com/docs/answer/3540681?co=GENIE.Platform%3DDesktop&hl=en)

### Formatting Numbers
Official documentation on formatting numbers from [Google](https://support.google.com/docs/answer/56470?co=GENIE.Platform%3DDesktop&hl=en)
