# Aggregation Functions
Aggregation is a blanket term for any function that takes a large number of inputs, does some math on them, and returns a single output. The most common aggregation functions are summing and counting, though others exist, as we will see.

## Introduction

Our aggregation functions come in two flavors: _conditional_ and _unconditional_.

**Unconditional** functions are what you're likely accustomed to. They include `SUM()` and `COUNT()` and function by taking in a collection of inputs and returning a single output.

**Conditional** functions are what we're here to learn about. They take in a collection of data, but _selectively_ process it based on one or more criteria. Conditional aggregation functions typically have `-IF` of `-IFS` at the end, like `SUMIFS()` and `COUNTIFS()`.

## Syntax
The examples come from the [Aggregation](./assets/Aggregation.xlsx) Excel workbook. The worked-out version is available [here](./assets/Aggregation-Worked.xlsx).

!!! note "`-IF` vs `-IFS`"

    Several of our aggregation functions come in `-IF` and `-IFS` variants, e.g. `SUMIF()/SUMIFS()` and `COUNTIF()/COUNTIFS()`. We will focus only on the `-IFS` variants, since they can do everything the `-IF` variants can, plus accept multiple conditions.


#### File Snapshot
| Students | Homerooms | Stats | Attendance | Assignments |
| --- | --- | --- | --- | --- |
| ![Aggregation Students Tab](./images/Aggregation_Students.png) | ![Aggregation Homerooms Tab](./images/Aggregation_Homerooms.png) | ![Aggregation Stats Tab](./images/Aggregation_Stats.png) | ![Aggregation Attendance Tab](./images/Aggregation_Attendance.png) | ![Aggregation Assignments Tab](./images/Aggregation_Assignments.png) |

### `COUNTIFS()`

`COUNTIFS()` counts the number of records that meet one or more criteria. Criteria can come from multiple columns, as long as all of the input columns are the same size.

#### Example A: `Students!H2`

![Aggregation Students!H2](./images/Aggregation_StudentsH2.png){: style="width:300px"}

The formula in `Students!H2` reads as follows:

```
=COUNTIFS(Attendance!A:A, Students!A2)
```

!!! note "Paired Arguments"

      Conditional aggregation functions always take their conditions in _pairs_: the first argument in the pair is a range of cells; the second argument is the condition they must meet in order to be aggregated. Thus, rather than speaking of _numbers_ of arguments, we will speak of the number of _pairs_ of arguments.

This function uses one pair of arguments:

-   **`Attendance!A:A`** - the column which we are inspecting; in this case, the Student IDs.
-   **`Students!A2`** - the value which must be present in the column in order for a record to be counted.

In other words, this function will count how many times `20122028` appears in the `Student_Number` column of `Attendance`.

---

#### Example B: `Students!I2`

![Aggregation Students!I2](./images/Aggregation_StudentsI2.png){: style="width:300px"}

The formula in `Students!I2` reads as follows:

```
=COUNTIFS(Attendance!A:A, Students!A2, Attendance!H:H, "Present")
```

This function uses _two_ pairs of arguments:

-   **`Attendance!A:A`** - the first column which we are inspecting; in this case, the Student IDs.
-   **`Students!A2`** - the value which must be present in the column in order for a record to be counted.
-   **`Attendance!H:H`** - the second column which we are inspecting; in this case, the Presence Status.
-   **`"Present"`** - the value which must be present in that column in order for a record to be counted.

In other words, this function will count how many records have `20122028` in the `Student_Number` column, _and_ have `Present` in the `Presence_Status_CD` column.

!!! note "Matching Text"

    Text must be enclosed in double-quotes (`""`).

---
#### Example C: `Students!J2`

![Aggregation Students!J2](./images/Aggregation_StudentsJ2.png){: style="width:300px"}

The formula in `Students!J2` reads as follows:

```
=COUNTIFS(Attendance!A:A, Students!A2, Attendance!H:H, "Absent")
```

This function uses _two_ pairs of arguments:

-   **`Attendance!A:A`** - the first column which we are inspecting; in this case, the Student IDs.
-   **`Students!A2`** - the value which must be present in the column in order for a record to be counted.
-   **`Attendance!H:H`** - the second column which we are inspecting; in this case, the Presence Status.
-   **`"Absent"`** - the value which must be present in that column in order for a record to be counted.

In other words, this function will count how many records have `20122028` in the `Student_Number` column, _and_ have `Present` in the `Presence_Status_CD` column.

---

### `SUMIFS()`

Like `COUNIFS()`, `SUMIFS()` aggregates data based on one or more conditions, specified pairwise. The only difference is that `SUMIFS()` **also** requires a column that is to be added, based on the criteria found elsewhere. This column represents the first argument to the function.

#### Example D: `Homerooms!B2`

![Aggregation Homerooms!B2](./images/Aggregation_HomeroomsB2.png){: style="width:300px"}

The formula in `Homerooms!B2` reads as follows:

```
=SUMIFS(Students!H:H, Students!F:F, Homerooms!A2)
```

This function uses _one pair_ of arguments, plus the _summation column_.

-   **`Students!H:H`** - the column which we are summing; in this case, the number of days enrolled.
-   **`Students!F:F`** - the first column which we are inspecting; in this case, the Homeroom Teacher.
-   **`Homerooms!A2`** - the value which must be present in the column, in this case a given Homeroom Teacher.

In other words, this function will add up the number of days in each student's _Days Enrolled_ column, but _only_ if they have a `Home_Room` value equal to `Bowe`.

---

#### Example E: `Homerooms!C2`

![Aggregation Homerooms!C2](./images/Aggregation_HomeroomsC2.png){: style="width:300px"}

The formula in `Homerooms!C2` reads as follows:

```
=SUMIFS(Students!I:I, Students!F:F, Homerooms!A2)
```

This function uses _one pair_ of arguments, plus the **summation column**.

-   **`Students!I:I`** - the column which we are summing; in this case, the number of days present.
-   **`Students!F:F`** - the first column which we are inspecting; in this case, the Homeroom Teacher.
-   **`Homerooms!A2`** - the value which must be present in the column, in this case a given Homeroom Teacher.

In other words, this function will add up the number of days in each student's _Days Present_ column, but _only_ if they have a `Home_Room` value equal to `Bowe`.

### Additional Types of Condition

in the previous examples, all of our conditions have required a column to _equal_ a certain value. We can expand this set of conditions to include common comparison operators, among other things.

The available comparison operators include:

| Operator | Meaning |
| --- | --- |
| `"<"&A2`| Less than cell `A2` |
| `">"&A2` | Greater than cell `A2` |
| `"<="&A2` | Less than or equal to cell `A2` |
| `">="&A2` | Greater than or equal to cell `A2` |
| `"<>"&A2` | Not equal to cell `A2` |
| `""` | Cell is blank |

!!! note "Quote Syntax"

    To attach a comparison operator, you must enclose that operator in quotation marks and then **concatenate** it, using the `&` function, to the value.

#### Example F: `Stats!B2`

![Aggregation Stats!B2](./images/Aggregation_StatsB2.png){: style="width:300px"}

The formula in `Stats!B2` reads as follows:

```
=COUNTIFS(Students!$L:$L, 1, Students!$D:$D, B1)/COUNTIFS(Students!$D:$D, B1)
```

!!! note "Absolute References"

    Recall that **absolute references** are signified by dollar signs (`$`) in a cell address, and mean that the address won't change if the formula is filled elsewhere. By making these absolute references, we can fill the formula to the right - capturing all grades - without worrying about the formula pulling from the wrong column.

We begin with the first `COUNTIFS()` call, which takes _two pairs_ of arguments:

-   **`Students!$L:$L`** - the first column we are inspecting; in this case, the student's ISA rate.
-   **`1`** - the value which must be present in the column; in this case `100%` or `1`.
-   **`Students!$D:$D`** - the second column we are inspecting; in this case, the student's Grade Level.
-   **`B1`** - the value which must be present in the column; in this case `9`.

In other words, this function will count how many students have an `ISA %` of `100%`, and who _also_ have a `Grade_Level` of `9`.

The second `COUNTIFS()` call is identical to the first, except that it has no clause for ISA rate. That is, it is a straight count of how many students are in the 9th grade, forming the denominator of our fraction. Division then produces the percentage of 9th grade students with perfect attendance.

---

#### Example G: `Stats!B3`

![Aggregation Stats!B3](./images/Aggregation_StatsB3.png){: style="width:300px"}

The formula in `Stats!B3` reads as follows:

```
=COUNTIFS(Students!$L:$L,">="&0.95,Students!$D:$D,B1)/COUNTIFS(Students!$D:$D, B1)
```

We begin with the first `COUNTIFS()` call, which takes _two pairs_ of arguments:

-   **`Students!$L:$L`** - the first column we are inspecting; in this case, the student's ISA rate.
-   **`">="&0.95`** - specifies that we want the ISA rate to be `>=0.95`, or at least 95%.
-   **`Students!$D:$D`** - the second column we are inspecting; in this case, the student's Grade Level.
-   **`B1`** - the value which must be present in the column; in this case `9`.

In other words, this function will count how many students have an `ISA %` of at least `0.95`, and who _also_ have a `Grade_Level` of `9`.

The second `COUNTIFS()` call is identical to the first, except that it has no clause for ISA rate. That is, it is a straight count of how many students are in the 9th grade, forming the denominator of our fraction. Division then produces the percentage of 9th grade students with attendance of at least 95%.

---

#### Example H: `Stats!B4`

![Aggregation Stats!B4](./images/Aggregation_StatsB4.png){: style="width:300px"}

The formula in `Stats!B4` reads as follows:

```
=COUNTIFS(Students!$L:$L,"<"&0.9,Students!$D:$D,B1)/COUNTIFS(Students!$D:$D, B1)
```

We begin with the first `COUNTIFS()` call, which takes _two pairs_ of arguments:

-   **`Students!$L:$L`** - the first column we are inspecting; in this case, the student's ISA rate.
-   **`"<"&0.9`** - specifies that we want the ISA rate to be `<0.9`, or less than 90%.
-   **`Students!$D:$D`** - the second column we are inspecting; in this case, the student's Grade Level.
-   **`B1`** - the value which must be present in the column; in this case `9`.

In other words, this function will count how many students have an `ISA %` of less than 90%, and who _also_ have a `Grade_Level` of `9`.

The second `COUNTIFS()` call is identical to the first, except that it has no clause for ISA rate. That is, it is a straight count of how many students are in the 9th grade, forming the denominator of our fraction. Division then produces the percentage of 9th grade students with attendance of less than 90%.

### `AVERAGEIFS()`

The `AVERAGEIFS()` function operates identically to the `SUMIFS()` function, except that instead of taking a sum of a given column it takes an average. Otherwise, the syntax is identical: the column to be averaged, followed by _pairs_ of arguments representing the conditions.

#### Example I: `Stats!B5`

![Aggregation Stats!B5](./images/Aggregation_StatsB5.png){: style="width:300px"}

The formula in `Stats!B5` reads as follows:

```
=AVERAGEIFS(Students!$G:$G, Students!$D:$D, Stats!B1)
```

We begin with the first `COUNTIFS()` call, which takes _one pair_ of arguments, plus the average column:

-   **`Students!$G:$G`** - the column being averaged; in this case, the GPA.
-   **`Students!$D:$D`** - the column we are inspecting; in this case, the student's Grade Level.
-   **`B1`** - the value which must be present in the column; in this case `9`.

In other words, this function will average the GPA of all students with a `Grade_Level` of `9`.

### `COUNTBLANK()`

`COUNTBLANK()` does exactly what its name suggests: it counts how many cells in a range are **blank**.

!!! warning "Blank vs. Empty Cells"

    A **blank** cell is a cell that contains an blank string, i.e. `""`. By contrast, an **empty** cell is a cell that contains _nothing at all_ - not even a blank string.

    In **Excel**, a blank and an empty cell are _equivalent_ and will be treated the same way by formulas.

    In **Google Sheets**, the two are different, with only a truly empty cell being counted as a blank. Proceed accordingly.


#### Example J: `Assignments!C4`

![Aggregation Assignments!C4](./images/Aggregation_AssignmentsC4.png){: style="width:250px"}

The formula in `Assignments!C4` reads as follows:

```
=COUNTBLANK(E4:CR4)
```

The formula accepts a single argument: `E4:CR4`, or the range of cells in which we are tabulating blanks.

In `E4:CR4` there are six cells with no values, hence the formula returns a `6`.

### `SUMPRODUCT()`

`SUMPRODUCT()` is the most complex aggregation function considered in this course. Broadly, it functions by multiplying two sets of data _pairwise_, and then adding the results. Its behavior is best shown by example.

| Row | (A) Quantity | (B) Price |
| --- | --- | --- |
| 1 | 3 | $2.50 |
| 2 | 1 | $5.60 |
| 3 | 4 | $0.75 |

Suppose we want to calculate the total cost of the items represented in the table. We would perform 2 steps:

1.  Multiply each **quantity** by its corresponding **price**, e.g. `3 * 2.50` to produce the subtotals of `$7.50`, `$5.60`, and `$3.00`.
2.  Add those subtotals to produce the grand total of `$14.10`

`SUMPRODUCT()` accomplishes exactly this process. In the example above, the function call would be

```
=SUMPRODUCT(A1:A3, B1:B3)
```

`SUMPRODUCT()` works on pairs of _columns_ or pairs of _rows_, provided both are the same length.

#### Example K: `Assignments!D4`

![Aggregation Assignments!D4](./images/Aggregation_AssignmentsD4.png){: style="width:400px"}

The formula in `Assignments!D4` reads as follows:

```
=SUMPRODUCT(E4:CR4, $E$2:$CR$2)/(SUM($E$2:$CR$2)-SUMIFS($E$2:$CR$2, E4:CR4, ""))
```

The purpose of this formula is to calculate a student's course grade, _excluding any unsubmitted assignments_.

We begin with the numerator: `SUMPRODUCT(E4:CR4, $E$2:$CR$2)`, which has two arguments:

-   **`E4:CR4`** - the range containing the student's scores on each assignment.
-   **`$E$2:$CR$2`** - the range containing the assignment point values.

!!! hint "Absolute References"

    Note that we are employing **absolute references** in the second argument. As before, this is to keep the point totals steady when filling the formula down the column.

Now, we examine the denominator, which has two parts: a sum and a conditional sum. First, the sum, which accepts a single argument:

-   **`$E$2:$CR$2`** - the point value of each assignment in the gradebook.

This `SUM()` produces the total points available to students. Next, we need to subtract any points corresponding to assignments that a given student did not complete. This is done with the `SUMIFS($E$2:$CR$2, E4:CR4, "")` portion of the denominator, which itself accepts three arguments:

-   **`$E$2:$CR$2`** - the point value of each assignment in the gradebooks.
-   **`E4:CR4`** - the range we are inspecting; in this case, the student's grade on each assignment (or a blank if not submitted).
-   **`""`** - an empty string, indicating that we are adding only when the cell in our target column is **blank**.

In other words, this denominator will count the total points available to a student, sans points corresponding to unsubmitted assignments. The division then produces a percent score corresponding to total points earned divided by total points available.

## Practice

1.  Download the [Session 2 Practice](./assets/Aggregation-Practice.xlsx) workbook, which contains:
    -   A roster of all students at _Cherry Hill Middle School_,
    -   Their Semester 1 grades,
    -   A roster of all students at _Apple Grove High School_ and their Semester 1 grades,
    -   The _Apple Grove Unified School District_ grading scale
2.  In the _Simple GPA_ column of the **MS Students** sheet, calcuate each student's _Simple GPA_. The Simple GPA is just that: an average of each student's grade points (as taken from the **MS Grades** sheet).
    -   If you want to review your lookup functions, delete the contents of the _Grade Points_ column and look them up using the _Grade_ column and the **GradeScale** sheet.
3.  In the _Failing Courses_ column of the **MS Students** sheet, count how many courses each student is failing (i.e. has an `F` as the grade).
4.  In the **MS Statistics** sheet, count the number of students on:
    -   Principal's Honor Roll, i.e. having a GPA ≥ 4.00
    -   Honor Roll, i.e. having a GPA ≥ 3.00 **Note:** you should _exclude_ students who are already on the Principal's Honor Roll, so in effect you're counting students with GPAs _between_ 3.00 and 4.00. (Yes, you can do this with outright subtraction, but that's cheating.)
    -   Academic Probation, i.e. having a GPA ≤ 2.00
5.  In the same sheet, give the _percentage_ of students in each grade who are failing at least one course.
6.  In the _Weighted GradePoints_ column of the **HS Students** sheet, sum the _weighted_ grade points for each student. Weighted grade points are equal to `[Grade Points] x [Credit Hours]`, both of which are in the table at right. (**Hint:** you're using `SUMPRODUCT()` for each student.) Also note that the **HS Grades** table is provided for reference only; you won't need it for this exercise.
7.  In the _Courses_ column of the **HS Students** sheet, count how many courses each student was enrolled in; i.e. how many grade point entries they have. You can do this using the table at right, or the **HS Grades** table - your call.
8.   In the _Weighted GPA_ column of the **HS Students** sheet, calculate each student's weighted GPA by dividing the weighted grade points by the number of courses. If you want to be fancy, you can combine your work from Steps 6-7 into a single mega-formula, rather than referencing the columns directly.

### Solution
When you're ready to check out the solution, it's [here](./assets/Aggregation-Solution.xlsx).

## Official Documentation
### `COUNTIFS()`
Official documentation on `VLOOKUP()` from [Google](https://support.google.com/docs/answer/3256550?hl=en)

### `SUMIFS()`
Official documentation on `HLOOKUP()` from [Google](https://support.google.com/docs/answer/3238496?hl=en&ref_topic=3105474)

### `AVERAGEIFS()`
Official documentation on `INDEX()` from [Google](https://support.google.com/docs/answer/3256534?hl=en)

### `COUNTBLANK()`
Official documentation on `MATCH()` from [Google](https://support.google.com/docs/answer/3093403?hl=en&ref_topic=3105474)

### `SUMPRODUCT()`
Official documentation on `MATCH()` from [Google](https://support.google.com/docs/answer/3094294?hl=en)
