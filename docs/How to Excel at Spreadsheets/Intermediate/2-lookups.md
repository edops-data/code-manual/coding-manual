# Lookup Functions
Lookup functions are the workhorses of Excel. They allow you to join multiple data sets based on values that they have in common, and are essential tools for any specialist.

## Introduction

Lookup functions do more or less what they sound like: they "look up" one record in a data set, and return a corresponding value from that record. Examples include:

-   Returning a student's grade level based on the student's ID number
-   Returning an account's name based on the account's number
-   Returning a performance level based on a scale score

!!! note "Numeric Keys"

      In each of the above examples, the keys being looked up (Student ID, Account Number, Scale Score) were **numeric**. While lookup functions don't require numeric keys, we prefer them because it's unlikely that two different accounts/students will have the same ID number, while it is possible that they will have the same name.

## Vocabulary

Before we get started, some vocabulary is in order:

#### Tables
A **table** is a chunk of data with some number of rows and columns. Examples include

-   Each student's Q1 grade
-   Student enrollment records
-   The general ledger

Lookups permit us, under the right conditions, to pull data from multiple **tables**.

-   The **source table** is the table we are currently in; the table that has some - but not all - of the required data.
-   The **destination table** is the table from which we are going to get our remaining information.

#### Records

A **record** is a row in a table. Examples include

-   Each student in the roster
-   Each grade in the grades table
-   Each transaction in the journal

#### Keys

A **key** is the data point that appears in both tables, allowing us to connect them. Examples include

-   The Student ID, present in both the roster and the table of grades
-   The account number, present in both the ledger and account record
    
The **key** should be _unique_ to each record; i.e. no two students or accounts should share the same ID.

#### Values

A **value** is the data point that is being sought from a different table. Examples include

-   The student's Q1 grade from the grade table)
-   The student's name from the roster table
-   The account name from the accounts table

When performing a lookup, we

1.  Pick a **record** (a student) from one **table** (the roster)
2.  Find its **key** (Student ID) in the other **table** (the stored grades)
3.  Locate the corresponding **record** that belongs to that **key** (the student's grade record)
4.  Retrieve the relevant **value** (the grade) from that **record**

## Syntax
The examples come from the [Lookup](./assets/Lookup.xlsx) Excel workbook. The worked-out version is available [here](./assets/Lookup-Worked.xlsx).

#### File Snapshot
| Students | CC | GradeScale | SpiritWeek |
| --- | --- | --- | --- |
| ![Lookup Students Tab](./images/Lookup_Students.png) | ![Lookup CC Tab](./images/Lookup_CC.png) | ![Lookup GradeScale Tab](./images/Lookup_GradeScale.png) | ![Lookup SpiritWeek Tab](./images/Lookup_SpiritWeek.png) |

### `VLOOKUP()`

`VLOOKUP()` (vertical lookup) is by far the most common lookup function, with most specialists eschewing it for the other two lookup methods less than 1% of the time. The _vertical_ portion of the name comes from the fact that `VLOOKUP()` traverses the data vertically down a single column until it finds the appropriate key.

More exhaustively, `VLOOKUP()` performs the following steps:

1.  Accepts a **key** (e.g. a Student ID) from the **source table**
2.  Scans the **key column** in the **destination table** until it finds the **key** from Step 1
3.  Traverses the same **record** from Step 2 by a given number of columns, landing at the target **value**
4.  Returns that **value**

Conceptually, `VLOOKUP()` is like asking Excel:

> Find me the **record** in [`destination table`] with a **key** of [`key`]. Once there, scoot over [`x`] many columns and return the **value** that you find.

Enough talk. Time for some examples.

#### Example A: `Students!D2`

![Lookup Students!D2](./images/Lookup_StudentsD2.png){: style="width:400px"}

The formula in `Students!D2` reads as follows:

```
=IFERROR(VLOOKUP(A2, CC!B:N, 4, FALSE), "--")
```

For now, ignore the `IFERROR()` clause; we will focus on the `VLOOKUP()` clause. The arguments to the `VLOOKUP()` clause are:

-   **`A2`** - this is the cell containing the **key**, i.e. the Student Number.
-   **`CC!B:N`** - this is the array in which we will be looking up our value. Importantly, the leftmost column (`CC!B:B`) is the **key column**, containing the matching Student Numbers.
-   **`4`** - this argument instructs `VLOOKUP()` to traverse _4 columns to the right_ (i.e. `Column E`) after finding the appropriate **record** based on the **key**.
-   **`FALSE`** - this argument instructs `VLOOKUP()` to only accept an _exact match_ of the Student ID. We will see later what happens when we set this argument to `TRUE`.

!!! note "`IFERROR()` clause"

    The `IFERROR()` clause is used to handle instances where `VLOOKUP()` returns an error. In this instance, `VLOOKUP()` will throw an error if it cannot find the appropriate Student Number in the **key column** of our target array (i.e. meaning that a given student has no Period 1 class). Rather than keeping the ugly error on screen, the `IFERROR()` function lets us mask it with a more visually appealing `--`.

---

#### Example B: `Students!E2`

![Lookup Students!E2](./images/Lookup_StudentsE2.png){: style="width:400px"}

The formula in `Students!E2` reads as follows:

```
=IFERROR(VLOOKUP(A2, CC!B:N, 12, FALSE), "--")
```

As before, we ignore the `IFERROR()` clause and focus on the `VLOOKUP()` clause:

-   **`A2`** - the cell containing the **key**, i.e. the Student Number
-   **`CC!B:N`** - the array in which we will be looking up our value. As before, the **key column** is the leftmost column of the array.
-   **`12`** - the number of columns to the _right_ that the function must traverse after finding the appropriate **record** based on the **key**.
-   **`FALSE`** - accept only exact matches of the **key** in the **key column**.

### `HLOOKUP()`

`HLOOKUP()` (horizontal lookup) acts the same as `VLOOKUP()`, with the exception that, instead of _vertically_ traversing a **key column**, `HLOOKUP()` _horizontally_ traverses a **key row**.

Functionality is best shown by example.

#### Example C: `Students!G2`

![Lookup Students!G2](./images/Lookup_StudentsG2.png){: style="width:400px"}

The formula in `Students!G2` reads as follows:

```
=IFERROR(HLOOKUP(E2, SpiritWeek!$1:$4, 3, FALSE), "--")
```

As before, we ignore the `IFERROR()` clause and focus on the `HLOOKUP()` clause.

-   **`E2`** - the cell containing our **key**, i.e. the Teacher Name.
-   **`SpiritWeek!$1:$4`** - the array in which we will be looking up our value. Much as `VLOOKUP()` requires the **key column** to be the _leftmost_ column, our **key row** must be the _top_ row.
-   **`3`** - the number of _rows_ _down_ that the function must traverse after finding the appropriate **record** based on the **key**.
-   **`FALSE`** - accept only exact matches of the **key** in the **key row**.

!!! note "Absolute Cell References"

    You will notice that the destination table is referenced using dollar signs (`$`). This is because, when filling the formula down the column, these cell references would normally be incremented as well (remove the `$` and try it!). Under normal circumstances, this is a helpful feature. However, in this case, we want the destination table to remain fixed, so we use `$` notation to ensure that `SpiritWeek!$1:$4` _stays_ `SpiritWeek!$1:$4` even when filling down.

    **Why didn't we need this before?** Because we weren't changing _columns_, only changing _rows_. Had you filled the formula to the _right_ instead of filling _down_, however, these same steps would be necessary.

### `INDEX()/MATCH()`

Together, `INDEX()` and `MATCH()` perform the same function as `VLOOKUP()` or `HLOOKUP()`: they take a **key** from a **source table**, find the corresponding **record** in a **destination table**, traverse a certain number of columns or rows through that **record**, and return the corresponding **value**. The beauty of `INDEX()` and `MATCH()` is that they break this process into two parts, providing some added versatility.

#### `MATCH()`

For reference, a lookup function performs four discrete steps:

1.  Pick a **record** from the **source table**
2.  Find its **key** in the **destination table**
3.  Locate the corresponding **record** that belongs to that **key**
4.  Retrieve the relevant **value** from that **record**

The `MATCH()` function accomplishes the first two steps: it takes a **record** from the **source table** and returns the _location_ of the corresponding record in the **destination table**.

Consider the example below:

| Row | (A) Student ID | (B) Name | (C) House |
| --- | --- | --- | --- |
| 1 | 9000141 | Harry Potter | Gryffindor |
| 2 | 9000569 | Cedric Diggory | Hufflepuff |
| 3 | 1427834 | Draco Malfoy | Slytherin |

Suppose we are interested in looking up **Cedric Diggory**'s location in the table. We can run the function

```
=MATCH(9000569, A:A, 0)
```

which will return the value of `2`. In other words, Cedric Diggory's ID number is located in Row `2` of the target column (`A:A`), which a visual inspection confirms to be true. Similarly, looking up the ID `1427834` would return `3`, and looking up `9000141` would return `1`.

!!! note "Third Argument"

    We carefully ignored the third argument, `0`. This simply means to seek an _exact match_, identical to the use of `FALSE` in the `VLOOKUP()` call. We'll look at other options later.

While this information can often be useful on its own, in this case we're interested in how it gets used by `INDEX()`.

#### `INDEX()`

Again, recall the four steps of a lookup function:

1.  Pick a **record** from the **source table**
2.  Find its **key** in the **destination table**
3.  Locate the corresponding **record** that belongs to that **key**
4.  Retrieve the relevant **value** from that **record**

The `INDEX()` function accomplishes the _second_ two steps: it locates a **record** in the **destination table** based on the location provided by `MATCH()`, and retrieves the relevant **value** belonging to that record.

Consider our previous example:

| Row | (A) Student ID | (B) Name | (C) House |
| --- | --- | --- | --- |
| 1 | 9000141 | Harry Potter | Gryffindor |
| 2 | 9000569 | Cedric Diggory | Hufflepuff |
| 3 | 1427834 | Draco Malfoy | Slytherin |

Earlier, we looked up **Cedric Diggory** using his Student ID:

```
=MATCH(9000569, A:A, 0)
```

which returned `2`, indicating that Cedric's record is in the 2nd row of the table. Suppose we're interested in retrieving his House, which is _2 columns to the right_ of his Student ID (i.e. Column `3` overall). We could construct the function call:

```
=INDEX(A1:C3, 2, 3)
```

which tells us that our record is in row number `2`, and we should navigate `3` columns over. Sure enough, this would produce the value `Hufflepuff`. Keeping the `3` the same (because the House column remains Column `3`) and changing the row would allow us to look up the houses of the other characters as well:

| Function | Output |
| --- | --- |
| `INDEX(A1:C3, 3, 3)` | `Slytherin` |
| `INDEX(A1:C3, 1, 3)` | `Gryffindor` |

!!! note "Two Dimensional Lookup"

    `INDEX()` allows us to traverse an array in two dimensions at once: _horizontally_ (across _columns_) and _vertically_ (across _rows_). Under normal circumstances, however, we only use the _vertical_ navigation and simply constrain our array to a single column, as you will see in the next section.

#### Combining `INDEX()` and `MATCH()`

This spreadsheet contains two examples of how to combine `INDEX()` and `MATCH()`. The first simply recreates a basic `VLOOKUP()`; the second uses the 2-dimensional nature of `INDEX()` to perform a more complex lookup.

#### Example D: `Students!F2`

![Lookup Students!F2](./images/Lookup_StudentsF2.png){: style="width:400px"}

The formula in `Students!F2` reads as follows:

```
=IFERROR(INDEX(CC!O:O, MATCH(Students!A2, CC!B:B, 0)), "--")
```

As before, we ignore the `IFERROR()` clause and focus on the interior function calls. We start with `MATCH()`, which has 3 arguments:

-   **`Students!A2`** - the cell containing our **key**, in this case the Student Number
-   **`CC!B:B`** - the column in the **destination table** containing our **key**. Note that we supply only the **key column** rather than a full array, since `INDEX()` will handle the horizontal navigation
-   **`0`** - specifies that we want an _exact_ match akin to the `FALSE` argument in `VLOOKUP()`

Taken on its own,

```
MATCH(Students!A2, CC!B:B, 0)
```

returns `45`. A quick visual inspection shows that row `45` of the `CC` table belongs to Student Number `14`, or Cody Anderson.

Next, we look at the `INDEX()` portion of the function, which as two arguments:

-   **`CC!O:O`** - the **value column** in the destination table, containing the grades that we wish to pull
-   **`MATCH(...)`** - the distance down that column to navigate, as returned by the `MATCH()` function (in this case, `45` as described above)

!!! note "Single Dimension"

    Since we are navigating a single column, there is no need for a third argument. If the array had multiple columns, we would need to specify a horizontal dimension as well.

#### Example E: `Students!H2`

![Lookup Students!H2](./images/Lookup_StudentsH2.png){: style="width:400px"}

The formula in `Students!H2` reads as follows:

```
=IFERROR(INDEX(SpiritWeek!$1:$6, MATCH(Students!H$1, SpiritWeek!$A:$A, 0), MATCH(Students!E2, SpiritWeek!$1:$1, 0)), "--")
```

As before, we ignore the `IFERROR()` clause. Unlike before, we will _not_ start with the innermost function, preferring to start with the `INDEX()` clause.

In this instance we must use all 3 arguments of `INDEX()`:

```
INDEX([array], [vertical offset], [horizontal offset])
```

-   The `[array]` is `SpiritWeek$1:$6`, the range we are going to scan.
-   The `[vertical offset]` is how far _down_ we must navigate in the array. In this example that corresponds to identifying the appropriate **Date** using `MATCH(Students!H$1, SpiritWeek!$A:$A, 0)`
-   The `[horizontal offset]` is how far to the _right_ we must navigate in the array. In this example that corresponds to identifying the appropriate **Teacher** using `MATCH(Students!E2, SpiritWeek!$1:$1, 0))`.

Given that cell `H2` corresponds to a teacher of `Adams, Mark B` and a date of `Wednesday, February 3, 2021`, this reduces to

```
INDEX(SpiritWeek$1:$6, 4, 13)
```

Now, we examine the two `MATCH()` clauses in turn, to confirm that they return the expected values of `4` and `13`, respectively.

```
MATCH(Students!H$1, SpiritWeek!$A:$A, 0)
```

This instance of `MATCH()` is used to determine how far _down_ the `INDEX()` function must navigate. It takes 3 arguments:

-   **`Students!H$1`** - the cell containing our **key** (in this case, the date)
-   **`SpiritWeek!$A:$A`** - the **key column** in the **destination table** (in this case, the column containing dates)
-   **`0`** - specifies that we are seeking an exact match

In practice, this function tells `INDEX()` to navigate _down_ by `4` rows.

Next, the second `MATCH()` clause.

```
MATCH(Students!E2, SpiritWeek!$1:$1, 0)
```

This instance of `MATCH()` is used to determine how far to the _right_ the `INDEX()` function must navigate. It takes 3 arguments:

-   **`Students!E2`** - the cell containing our **key** (in this case, the teacher)
-   **`SpiritWeek!$1:$1`** - the **key row** in the **destination table** (in this case, the column containing teachers)
-   **`0`** - specifies that we are seeking an exact match

In practice, this function tells `INDEX()` to navigate _right_ by `13` columns.

### Inexact Matches

All of the matches so far - be they in `VLOOKUP()`, `HLOOKUP()` or `MATCH()` - have been **exact matches**.  That is, if the exact key value cannot be located in the key column/row, then the lookup function will return an error. 99% of the time you will use exact matches.

In some instances, though, we simply want the _nearest_ value to our key. In such instances, we are seeking an **inexact match**, which comes with its own syntax and considerations.

In `VLOOKUP()` and `HLOOKUP()` changing the fourth argument from `FALSE` to `TRUE` will result in an **inexact match**, which is defined as _the largest value less than or equal to the key_. In `MATCH()` this is the same as setting the third argument to `1`.

That is, in the set

| n |
| --- |
| 1 |
| 3 |
| 4 |
| 7 |
| 9 |
| 14 |
| 15 |

searching for `11` would match with `9`, the largest number smaller than the key.

!!! warning "Sorting"

    In order to use an inexact match, the destination table must be sorted in **ascending order**. Such sorting is not necessary for exact matches.

!!! hint "Data Types"

    Inexact matches perform strangely with text, and as such should only be used with **numeric** keys.

#### Example F: `CC!O2`

![Lookup CC!O2](./images/Lookup_CCO2.png){: style="width:300px"}

The formula in `CC!O2` reads as follows:

```
=VLOOKUP(N2, GradeScale!A:C, 2, TRUE)
```

The three arguments are:

-   **`N2`** - the key being searched for
-   **`GradeScale!A:C`** - the destination table, such that the key column is in the leftmost position
-   **`2`** - the number of columns to traverse once the record has been located
-   **`TRUE`** - specifies that we are interested in the nearest number that is less than our key

In practice, since `N2` is equal to `90.24`, the nearest value in `GradeScale!A:C` is `90`, resulting in a returned value of `A-`.

#### Example G: `CC!P2`

![Lookup CC!O2](./images/Lookup_CCP2.png){: style="width:300px"}

The formula in `CC!P2` reads as follows:

```
=VLOOKUP(N2, GradeScale!A:C, 3, TRUE)
```

The three arguments are:

-   **`N2`** - the key being searched for
-   **`GradeScale!A:C`** - the destination table, such that the key column is in the leftmost position
-   **`3`** - the number of columns to traverse once the record has been located
-   **`TRUE`** - specifies that we are interested in the nearest number that is less than our key

In practice, since `N2` is equal to `90.24`, the nearest value in `GradeScale!A:C` is `90`, resulting in a returned value of `3.67`.

## Practice

1.  Download the [Session 2 Practice](./assets/Lookup-Practice.xlsx) workbook, which contains:
    -   A roster of all students at _Cherry Hill Middle School_,
    -   Their Homeroom teachers,
    -   Their Winter MAP scores in Math,
    -   The concordance table that links MAP RIT scores to PARCC performance levels.
2.  In the _Homeroom Teacher_ column, use `VLOOKUP()` to associate each student in the **Students** table with their homeroom teacher in the **Homerooms** table. (**Hint:** you will want to use the `Student_Number` field as your key.)
    -   Not every student has a homeroom. For these students, `VLOOKUP()` will throw an error. If you want to be fancy, you can use the `IFERROR()` function to suppress these errors with a tidier-looking `--`.
3.  In the _Winter Math RIT_ column, use `INDEX()` and `MATCH()` to retrieve each student's RIT score from the ** MAP_Math** table. (**Hint:** you will want to use `StudentID` as your key, and your value column is `StartRIT`.)
4.  In the _Winter PARCC Level_ column, use `HLOOKUP()` to match each student's RIT score with their projected PARCC level using the **MAP-PARCC** table. _Do this only for 6th grade._ If you want to match all students, you will need to use `INDEX()`/`MATCH()`. (**Hint:** this is an approximate match).

### Solution
When you're ready to check out the solution, it's [here](./assets/Lookup-Solution.xlsx).

## Official Documentation
### `VLOOKUP()`
Official documentation on `VLOOKUP()` from [Google](https://support.google.com/docs/answer/3093318?hl=en)

### `HLOOKUP()`
Official documentation on `HLOOKUP()` from [Google](https://support.google.com/docs/answer/3093375?hl=en)

### `INDEX()`
Official documentation on `INDEX()` from [Google](https://support.google.com/docs/answer/3098242?hl=en)

### `MATCH()`
Official documentation on `MATCH()` from [Google](https://support.google.com/docs/answer/3093378?hl=en&ref_topic=3105472)
