# Text Functions
When people think of spreadsheets, they generally think of numbers. But as data specialists, we often have to spend inordinate amounts of time parsing text - notably names. For this, we have a useful collection of text functions.

## Introduction

Our text functions come in 2 main flavors:

-   Functions that **split** or **combine** strings to form new strings
-   Functions that **format** text to make it more user-friendly

We will also use the `IFERROR()` function in this module, so we should probably learn about it now.

### `IFERROR()`

`IFERROR()` takes a formula and does either of two things, depending on whether that formula throws an error:

-   If the formula does **not** throw an error, `IFERROR()` just passes the output of the formula.
-   If the formula **does** throw an error, `IFERROR()` does whatever is in its second argument. This could be as simple as suppressing the error message with an empty cell, or as complex as running a different formula entirely.

#### Example A: `IFERROR()`

Consider the table below:

| Row | A | B |
| --- | --- | --- |
| 1 | 2 | 2 |
| 2 | 2 | 1 |
| 3 | 2 | 0 |

Let's now create a Column `C` which divides Column `A` by Column `B`. We use the formula

```
=B1/A1
```

and get

| Row | A | B | C |
| --- | --- | --- | --- |
| 1 | `2` | `2` | `1` |
| 2 | `2` | `1` | `2` |
| 3 | `2` | `0` | `#DIV/0` |

Cell `C3` throws an error because division by zero is forbidden. Errors are ugly, though, so let's create a Column `D` that conceals these errors with a more pleasant `--`, using the formula:

```
=IFERROR(B1/A1, "--")
```

Now, we get

| Row | A | B | C | D |
| --- | --- | --- | --- | --- |
| 1 | `2` | `2` | `1` | `1` |
| 2 | `2` | `1` | `2` | `2` |
| 3 | `2` | `0` | `#DIV/0` | `--` |

Where the division is successful, the `IFERROR()` clause does nothing, but when there is an error, it gets suppressed. As stated before, we can refer to entire functions when the first function throws an error, and we will do exactly that later on.

Now, back to our regularly scheduled program.

## Syntax
The examples come from the [Text](./assets/Text.xlsx) Excel workbook. The worked-out version is available [here](./assets/Text-Worked.xlsx).

#### File Snapshot
| 10th Grade | 12th Grade | 11th Grade | Classes |
| --- | --- | --- | --- |
| ![Text 10th Grade Tab](./images/Text_10thGrade.png) | ![Text 12th Grade Tab](./images/Text_12thGrade.png) | ![Text 11th Grade Tab](./images/Text_11thGrade.png) | ![Text Classes Tab](./images/Text_Classes.png) |

### `TEXT()`

The `TEXT()` function takes values that are stored as numbers (including dates and times!) and renders them as text in a format dictated by the user. Functionality is best shown by example:

| `x` | Function | Result |
| --- | --- | --- |
| October 4, 2021 | `TEXT(x, "M")` | `10` |
| October 4, 2021 | `TEXT(x, "DD")` | `04` |
| October 4, 2021 | `TEXT(x, "MMM D")` | `Oct 4` |
| 6:05 PM | `TEXT(x, HH:MM)` | `18:05` |
| 1278 | `TEXT(x, 00000.0)` | `01278.0` |
| 1278 | `TEXT(x, "#,##0")` | `1,278` |

While `TEXT()` can operate on any numeric value, we will focus only on dates here.

#### Common Date Masks

!!! note Terminology

    **Mask** is the technical term for the second parameter of the `TEXT()` function - i.e. the text that determines the format of the output.

Consider the input `March 4, 2021`:

| Mask | Output |
| --- | --- |
| `M` | `3` |
| `MM` | `03` |
| `MMM` | `Mar` |
| `MMMM` | `March` |
| `D` | `4` |
| `DD` | `04` |
| `DDD` | `Thu` |
| `DDDD` | `Thursday` |
| `Y` / `YY` | `21` |
| `YYYY` | `2021` |

#### Example A: `'10th Grade'!I2`

![Text '10th Grade'!I2](./images/Text_10thGradeI2.png){: style="width:300px"}

The formula in `'10th Grade'!I2` reads as follows:

```
=TEXT(F2, "MMMM D")
```

The function accepts two arguments:

-   **`F2`** - the date object being rendered; in this case, the student's birthdate.
-   **`"MMMM D"`** - the format mask; in this case, the full name of the month, plus the date without leading zeroes.

As expected, the formula returns the date in the format `MMMM D`: `October 1`.

### `LEFT()` and `RIGHT()`

The `LEFT()` and `RIGHT()` functions extract the left/rightmost `n` characters from a string. For example:

| String | `n` | `LEFT(String, n)` | `RIGHT(String, n)` |
| --- | --- | --- | --- |
| Hello, World! | 1 | `H` | `!` |
| Hello, World! | 5 | `Hello` | `orld!` |

### Ampersand (`&`) Operator

The ampersand (`&`) operator accomplishes the same function as the `CONCAT()` function, in that it concatenates (i.e. "glues together") multiple strings. For example:

| A | B | `A&B` |
| --- | --- | --- |
| Hello | World! | HelloWorld! |

Note that `&` doesn't insert spaces, so if you want any manner of delimiter it's on you. In the previous example, we would instead write:

```
A&" "&B
```

which produces

```
Hello World!
```

### `TRIM()`

`TRIM()` removes leading or trailing whitespace from a string. Its operation is best shown by example:

!!! note Underscores

    The script that renders these files is trying to be helpful by auto-trimming the text, ruining my examples. Therefore, I am substituting underscores for spaces.

| String | `TRIM(String)` | Explanation |
| --- | --- | --- |
| `_Hello World!` | `Hello World!` | The space prior to _Hello_ is removed. |
| `Hello World!__` | `Hello World!` | The two spaces following _World!_ are removed. |
| `Hello World!` | `Hello World!`| The only space is in the middle of the string, so it is untouched. |

#### Example B: `'10th Grade'!H2`

![Text '10th Grade'!H2](./images/Text_10thGradeH2.png){: style="width:300px"}

The formula in `'10th Grade'!H2` reads as follows:

```
=TRIM(C2&", "&D2&" "&LEFT(E2, 1))
```

We begin examining this formula with the `LEFT()` clause. The `LEFT()` function takes two arguments:

-   **`E2`** - the cell we are extracting text from; in this case, the student's middle name.
-   **`1`** - the number of characters we are extracting; in this case, just the first.

!!! note "Empty Strings"

    Running `LEFT()` or `RIGHT()` on an empty string will simply return nothing without throwing an error. Convenient!

Next, we examine the contents of the `TRIM()` function: `C2&", "&D2&", "&LEFT(E2, 1)`

-   **`C2`** - the entirety of the student's last name
-   **`", "`** - a comma and a space to delimit the last/first names
-   **`D2`** - the entirety of the student's first name
-   **`" "`** - a space to delimit the first name/middle initial. If there is no middle initial, this space will be trimmed off.
-   **`LEFT(E2, 1)`** - the first character, if applicable, of the student's middle name

The result of this formula is `Adams, Corby_` (note the trailing space - represented by an underscore - since C. Adams has no middle name).

Finally, we wrap the above in a `TRIM()` function call to remove the trailing space, resulting in `Adams, Corby`.

### `SEARCH()`

`SEARCH()` searches for a substring inside a larger string and returns the _starting_ position of the substring. For example:

`SEARCH("book", "textbook") = 5` because _book_ appears starting at the 5th character in _textbook_.

!!! note "Errors"

    If the substring is not found in the main string, then `SEARCH()` throws an error. This is unlike `LEFT()` and `RIGHT()`, which just return a blank cell.

In this module, we will primarily use `SEARCH()` to locate spaces or commas in names. Since `SEARCH()` only appears inside larger formulas, a few isolated examples are in order:

| String | Substring | `SEARCH([substring], [string])` | Notes |
| --- | --- | --- | --- |
| `Hopkins, Anthony` | `", "` | `8` | The comma is the 8th character in the string |
| `Hopkins, Anthony` | `" "` | `9` | The space is the 9th character in the string |

!!! note "Start Point"

    By default, `SEARCH()` starts at the left of the string and moves to the right. If necessary, an alternate start point can be specified. For example:

    `SEARCH("o", "Anthony Hopkins") = 5` because "o" is the 5th character. However, there is a second "o" in _Hopkins_. To find it, we specify a starting position after the first "o": `SEARCH("o", "Anthony Hopkins", 6) = 10`.

    Any starting point will do, as long as it's after the first "o" and before the second.

### `LEN()`

`LEN()` returns the number of characters - i.e. the length - of a string. In this module, we will use it when we need to extract "all but `x` many characters" from a string. By example:

| `x` | `LEN(x)` | `SEARCH(" ", x)` | `LEN(x) - SEARCH(" ", x)` | `RIGHT(x, LEN(x) - SEARCH(" ", x))` |
| --- | --- | --- | --- | --- |
| `Hello, World!` | `13` | `7` | `6` | `World!` |

#### Example C: `'12th Grade'!F2`

![Text '12th Grade'!F2](./images/Text_12thGradeF2.png){: style="width:300px"}

The formula in `'12th Grade'!F2` reads as follows:

```
=IFERROR(LEFT(E2, SEARCH(", ", E2)-1), RIGHT(E2, LEN(E2)-SEARCH(" ", E2)))
```

The `IFERROR()` clause exists because the names in Column `E` are sometimes in _Last, First_ order (e.g. `Adair, Brandon`) and sometimes in _First Last_ order (e.g. `Barry Anderson`). To handle these two cases, we generate a formula which works in _Last, First_ order but that throws an error when it encounters _First Last_ order. In such a case, `IFERROR()` will trigger the second formula, which is designed to work in _First Last_ order.

> I promise, this will make sense.

We begin by analyzing the first formula inside the `IFERROR()` function:

```
LEFT(E2, SEARCH(", ", E2)-1)
```

This function accepts two arguments. The second argument has been separated into its component parts for readability.

-   **`E2`** - the student's name, assumed to be in _Last, First_ order
-   **`SEARCH(", ", E2)`** - the location of the comma in the student's name. If there is no comma (i.e. the name is in _First Last_ order) then this function will throw an error - which, in this case, is desired behavior!
-   **`-1`** - since the `SEARCH()` function returns the location of the comma, then backs up by one character to capture only the last name

In practice, when fed a name in _Last, First_ order (e.g. `Adair, Brandon`) this function will return everything prior to the comma (e.g. `Adair`). When fed a name in _First Last_ order, this function will return an error.

Next, we analyze the second formula inside the `IFERROR()` function:

```
RIGHT(E2, LEN(E2)-SEARCH(" ", E2))
```

This function accepts two arguments. The second argument has been separated into its component parts for readability:

-   **`E2`** - the student's name, assumed to be in _First Last_ order
-   **`LEN(E2)`** - the length of the entire name, from which we will back off a number of characters
-   **`-SEARCH(" ", E2)`** - the location of the space in the name, which is subtracted off in order to produce the length of the last name

In practice, when fed a name in _First Last_ order (e.g. `Barry Anderson`) this function will return everything after the space (e.g. `Anderson`). When fed a name in _Last, First_ order this function will return garbage. That's okay, though, because it will only be called if the name is confirmed to be in _First Last_ order as a result of the first function returning an error.

#### Example D: `'12th Grade'!G2`

![Text '12th Grade'!G2](./images/Text_12thGradeG2.png){: style="width:300px"}

The formula in `'12th Grade'!G2` reads as follows:

```
=IFERROR(RIGHT(E2, LEN(E2)-SEARCH(", ", E2)-1), LEFT(E2, SEARCH(" ", E2)-1))
```

As before, the `IFERROR()` clause exists because the names in Column `E` are sometimes in _Last, First_ order (e.g. `Adair, Brandon`) and sometimes in _First Last_ order (e.g. `Barry Anderson`). As such, we create two formulas inside the `IFERROR()` function. The first functions when it detects _Last, First_ order and throws an error otherwise. If this function does throw an error, the name is assumed to be in _First Last_ order, which is handled by the second function.

We begin with the first argument to `IFERROR()`:

```
RIGHT(E2, LEN(E2)-SEARCH(", ", E2)-1)
```

This function accepts two arguments. The second argument, which contains three parts, has been separated for readability:

-   **`E2`** - the student's name, assumed to be in _Last, First_ order
-   **`LEN(E2)`** - the length of the entire name, from which we will back off a number of characters in order to retrieve just the first name
-   **`-SEARCH(", ", E2)`** - the location of the comma in the name, which is subtracted from the length of the entire name to return just the first name
-   **`-1`** - absent this subtraction, the first name would always carry a leading space. This could be handled with `TRIM()` as well.

In practice, when fed a name in _Last, First_ order (e.g. `Adair, Brandon`) this function will return everything after the space (e.g. `Brandon`). When fed a name in _First Last_ order, this function will fail to find a comma and will throw an error. This error then directs the `IFERROR()` function to defer to the second argument, which we will now analyze.

The second argument to `IFERROR()` is the following:

```
LEFT(E2, SEARCH(" ", E2)-1)
```

This function accepts two arguments. The second argument, which contains two parts, has been separated for readability:

-   **`E2`** - the student's name, assumed now to be in _First Last_ order
-   **`SEARCH(" ", E2)`** - the location of the space between the student's first and last names
-   **`-1`** - subtraction to remove the leading space from the first name. This could be handled with `TRIM()` as well.

In practice, when fed a name in _First Last_ order (e.g. `Barry Anderson`) this function will return everything prior to the space (e.g. `Barry`). When fed a name in _Last, First_ order, this function will return the last name; however, this is immaterial because this function is only invoked when the name has been confirmed to be in _First Last_ order.

### A much more complicated scenario

In the next two examples, our work has been substantially complicated by the occasional presence of a middle initial. Our logic for extracting the last name remains the same. Extracting the first name, however, requires additional logic to detect whether there is a middle initial following or not.

#### Example E: `'11th Grade'!F2`

![Text '11th Grade'!F2](./images/Text_11thGradeF2.png){: style="width:300px"}

The formula in `'11th Grade'!F2` reads as follows:

```
=LEFT(C2, SEARCH(", ", C2)-1)
```

Since all of the names in the **11th Grade** sheet are in _Last, First_ order, we can use this simple formula to extract the last name. The function accepts two parameters, the second of which has been split into parts for readability:

-   **`C2`** - the student's name, in _Last, First_ order
-   **`SEARCH(", ", C2)`** - the location of the comma between the last and first names
-   **`-1`** - subtraction to remove the comma from the last name

#### Example F: `'11th Grade'!G2`

![Text '11th Grade'!G2](./images/Text_11thGradeG2.png){: style="width:300px"}

The formula in `'11th Grade'!G2` reads as follows:

```
=TRIM(MID(C2, SEARCH(", ", C2)+2, IFERROR(SEARCH(" ", C2, SEARCH(", ", C2)+2), LEN(C2))-SEARCH(", ", C2)-1))
```

The `TRIM()` clause simply removes any residual whitespace, which in this case is caused by removing a middle initial - but not its prior space - when generating the first name. With that functionality established, we examine the `MID()` function in all its glory.

As mentioned earlier, the `MID()` function accepts 3 arguments:

1.  The string to be parsed: `C2`
2.  The starting position: `SEARCH(", ", C2)+2`
3.  The number of characters to capture: `IFERROR(SEARCH(" ", C2, SEARCH(", ", C2)+2), LEN(C2))-SEARCH(", ", C2)-1)`

Argument #1 is obvious enough, so we move on to the second argument:

```
SEARCH(", ", C2)+2
```

This argument returns the position of the first character of the first name. That is, it searches for the comma that separates last and first names, and then scans two characters ahead (i.e. the comma and the space) to find the position of the beginning of the first name.

In practice, when fed `Alfonso, Scott` this function returns `10`, since the comma is in the 8th position. That is, we will start capturing the first name at position `10`.

Argument #3 itself has three parts, the first two of which are wrapped in an `IFERROR()` function. Recall that `IFERROR()` returns the second argument only if the first argument throws an error; otherwise it returns the first argument. In this instance, our first argument functions only if the student has a middle initial, failing if it finds none. This way we can extract the first name in either _Last, First M_ or _Last, First_ format. We begin with the first argument to `IFERROR()`:

```
SEARCH(" ", C2, SEARCH(", ", C2)+2)
```

This function searches for a space in `C2`, much as we have done in prior examples. However, there is a third parameter: `SEARCH(", ", C2)+2`.

Recall that `SEARCH()` has an optional third parameter that specifies where to begin the search. Since we are trying to detect a middle initial, we are actually searching for the _second_ space in the name. As such, we must specify a start location that is _after the first space_. We do this by searching for a comma-space combination, and then add two characters so that the search starts after that position.

In practice, given a name in _Last, First M_ format (e.g. `Allen, Victor C`), this function will return the location of the second space (`14`). Given a name in _Last, First_ format (e.g. `Alfonso, Scott`), this function will fail to find a second space and will return an error. This error signifies to `IFERROR()` that there is no middle initial, and as such it should proceed to the second argument:

```
LEN(C2)
```

If there is no middle initial, then we wish to capture the _entire_ rest of the name; hence, we return the entire length of the string.

**To recap**, the `IFERROR()` function has told us how far to capture, depending on whether or not the student has a middle initial - up to the initial if so, through the entire string if not. However, we don't want the _location_ of the end of our search, we want the _distance from the first space_. Therefore, we do some subtraction.

```
-SEARCH(", ", C2)-1
```

This clause finds the location of the comma and subtracts it off (plus one more for the space prior to the first name), transforming the _position_ of the end of our string into a _distance_.

In practice, given a name in _Last, First M_ format, the function will:

-   Find the location of the comma (`SEARCH(", ", C2)+2`)
-   From there, find the location of the second space (`SEARCH(" ", C2, SEARCH(", ", C2)+2)`)
-   Subtract the location of the comma (`-SEARCH(", ", C2)-1`)
-   Pull that many characters, starting at the comma (`MID(...)`)
-   Trim any remaining spaces (`TRIM(...)`)

Given a name in _Last, First_ format, the function will:

-   Find the location of the comma (`SEARCH(", ", C2)+2`)
-   Attempt to find the location of the second space (`SEARCH(" ", C2, SEARCH(", ", C2)+2)`)
-   Fail at doing so, and instead return the length of the entire string (`LEN(C2)`)
-   Subtract the location of the comma (`-SEARCH(", ", C2)-1`)
-   Pull that many characters, starting at the comma (`MID(...)`)
-   Trim any remaining spaces (`TRIM(...)`)

## Practice

1.  Download the [Session 4 Practice](./assets/Text-Practice.xlsx) workbook, which contains:
    -   A roster of all students at _Cherry Hill Middle School_, and
    -   Their class schedules
2.   In the _Full Name_ column of the **Students** sheet, construct each student's full name in the format `Last, First M` where `M` is the middle initial where applicable. Make sure that names with no middle initial do not have a trailing space.
3.  In the _Birth Month_ and _Birth Day_ columns of the **Students** sheet, extract the student's birth month and day, respectively, from the _Birthday_ column.
4.  In the _Section_ column of the **Classes** sheet, construct the full course identifier for each row, in the format `Course_Number.Section_Number` (e.g. `ENG300.5`).
5.  In the _Schedule Entry_ column of the **Classes** sheet, construct the entry as it will appear in the student's schedule, in the format `Course_Name (Teacher) - Period` where `Teacher` is the teacher's name in the format `Last, F.` (`F.` being the first initial) and `Period` is the `External_Expression` with teh cycle day (the stuff in parentheses) removed. For example, Row `2` would render as `PE 8 (WIllard, H.) - P2`. It may help to create additional "helper" columns to render the teacher's name and period separately prior to combining them. Or use a single mega-formula; it's up to you.
6.  **Bonus**: In the _Department_ column of the **Classes** sheet, extract the department from each `Course_Number` entry. The department is the alphabetic portion of the `Course_Number`, and varies in length by department.

### Solution
When you're ready to check out the solution, it's [here](./assets/Text-Solution.xlsx).

## Official Documentation
### `LEFT()`
Official documentation on `LEFT()` from [Google](https://support.google.com/docs/answer/3094079?hl=en)

### `RIGHT()`
Official documentation on `RIGHT()` from [Google](https://support.google.com/docs/answer/3094087?hl=en&ref_topic=3105625)

### `MID()`
Official documentation on `MID()` from [Google](https://support.google.com/docs/answer/3094129?hl=en&ref_topic=3105625)

### `LEN()`
Official documentation on `LEN()` from [Google](https://support.google.com/docs/answer/3094081?hl=en&ref_topic=3105625)

### `IFERROR()`
Official documentation on `IFERROR()` from [Google](https://support.google.com/docs/answer/3093304?hl=en)

### `&`
Official documentation on `&` from [Google](https://support.google.com/docs/answer/3093592?hl=en)
