# Spreadsheet Function Library

All EdOps Data Specialists should be fluent with the functions below.  This list is far from comprehensive and does not include most basic functions (e.g. `SUM()`, `AVERAGE()`, `IF()`).

Functions are ordered by frequency of use, _not alphabetically_.

## Lookup Functions
Lookup functions are used to identify one value in an array, and then return a corresponding value - for example, looking up a student by ID and returning a name.

### `HLookup` / `VLookup`

`VLOOKUP()` and `HLOOKUP()` are used to match one value from an array, and then pull a corresponding value from a nearby column or row, respectively. They are likely the most heavily used functions in the EdOps arsenal.

Under normal circumstances, the _key_ (the value being matched) must be in the leftmost column or topmost row (depending on the function), which can be an inconvenience. There are ways to rework the function to get around this limitation, or in certain cases `INDEX()` and `MATCH()` can be used instead.

-   Documentation on `HLOOKUP()` [from Google](https://support.google.com/docs/answer/3093375)
-   Documentation on `VLOOKUP()` [from Google](https://support.google.com/docs/answer/3093318)

### `Index` / `Match`

`INDEX()` and `MATCH()` are a pair of functions which, together, accomplish the same function as `VLOOKUP()` or `HLOOKUP()`.  Being able to separate the functionality has advantages, though, and even in combination they can often perform lookups that are too complicated for `VLOOKUP()` and `HLOOKUP()` - in particular, they are useful when the _key_ (the value being matched) is not in the leftmost column or topmost row, respectively.

#### `MATCH()`
`MATCH()` returns the location of a value in a given column or row. This is similar to the first step in an `HLOOKUP()` or `VLOOKUP()`.  Often, the location is passed to an `INDEX()` function in order to return a corresponding value.

-   Documentation on `MATCH()` [from Google](https://support.google.com/docs/answer/3093378)

#### `INDEX()`
`INDEX()` returns a specific item from an array when provided with the number of rows and columns to be traversed in the array.  This is similar to the second step an `HLOOKUP()` or `VLOOKUP()`.  Often, the offset is provided by a `MATCH()` function.

-   Documentation on `INDEX()` [from Google](https://support.google.com/docs/answer/3098242)

Typically, `INDEX()` and `MATCH()` are used together as a more versatile version of `VLOOKUP()`.  For details, see this documentation [from Microsoft](https://support.microsoft.com/en-us/office/look-up-values-with-vlookup-index-or-match-68297403-7c3c-4150-9e3c-4d348188976b).

### `INDIRECT()`
`INDIRECT()` returns a cell reference as specified by a string. This can be useful when the cell being referenced is expected to change or needs to be generated formulaically.

-   Documentation on `INDIRECT()` [from Google](https://support.google.com/docs/answer/3093377)

### `OFFSET()`
`OFFSET()` functions similarly to `INDEX()` in that it traverses an array to return a value.  However, like with `INDIRECT()`, the array's location and dimensions can be generated formulaically.

-   Documentation on `OFFSET()` [from Google](https://support.google.com/docs/answer/3093379)

## Conditional Aggregation Functions
Conditional Aggregation Functions count, add, or otherwise operate on _portions_ of an array, as defined by various parameters.

### `CountIfs` / `SumIfs` / `AverageIfs`

The trio of `COUNTIFS()`, `SUMIFS()`, and `AVERAGEIFS()` are used to perform basic `COUNT()`, `SUM()`, and `AVERAGE()` functions on only a _portion_ of a data set, as determined by various sets of criteria.

#### `COUNTIFS()`
`COUNTIFS()` counts the number of cells in a range that meet various criteria.

-   Documentation on `COUNTIFS()` [from Google](https://support.google.com/docs/answer/3256550)

#### `SUMIFS()`
`SUMIFS()` adds the cells within a given range; however, cells are only included in the summation if various criteria are met.

-   Documentation on `SUMIFS()` [from Google](https://support.google.com/docs/answer/3238496)

!!! note "`COUNTIF()` and `SUMIF()`"

    `COUNTIF()` and `SUMIF()` are discouraged in favor of `COUNTIFS()/SUMIFS()` for consistency and scalability.

#### `AVERAGEIFS()`
`AVERAGEIFS()` takes the arithmetic mean of the cells within a given range; however, cells are only included in the average if various criteria are met.

-   Documentation on `AVERAGEIFS()` [from Google](https://support.google.com/docs/answer/3256534)

### `COUNTBLANK()`
`COUNTBLANK()` returns the number of empty cells in a range.

!!! note "Blank vs. Empty Cells"

    Importantly, a cell that is set to an empty string (`""`) is _not blank_ and will not be included in the count.

-   Documentation on `COUNTBLANK()` [from Google](https://support.google.com/docs/answer/3093403)

### `ROWS()`
`ROWS()` counts the number of rows in a range, essentially returning the height of an array.

!!! note "Use Case"

    `ROWS()` is often used in conjunction with a `FILTER()` statement when `COUNTIFS()` is impractical.

-   Documentation on `ROWS()` [from Google](https://support.google.com/docs/answer/3093382)

### `SUMPRODUCT()`
`SUMPRODUCT()` performs item-wise multiplication on two columns and then adds the products; for example, multiplying the price of each item by the corresponding quantity and then totaling up the amounts.

-   Documentation on `SUMPRODUCT()` [from Google](https://support.google.com/docs/answer/3094294)

## Array Functions
Array functions, as their name suggests, return entire arrays of values, rather than the single values returned by most other functions.

### `UNIQUE()`
`UNIQUE()` returns the unique rows from an array. Two rows are considered identical if and only if _every value_ is a match between the rows.

-   Documentation on `UNIQUE()` [from Google](https://support.google.com/docs/answer/3093198)

### `SORT()`
`SORT()` returns an array, with the rows sorted according to various parameters.  The array can be sorted by multiple columns, in order, each either ascending or descending.

-   Documentation on `SORT()` [from Google](https://support.google.com/docs/answer/3093150)

### `FILTER()`
`FILTER()` returns only the rows or columns from an array that meet various criteria.

!!! note "Use Case"

    Often, `FILTER()` is paired with other functions in order to perform conditional operations; for example, wrapping a `FILTER()` in a `COUNT()` returns the same result as a `COUNTIFS()` statement.

-   Documentation on `FILTER()` [from Google](https://support.google.com/docs/answer/3093197)

### `ARRAYFORMULA()` (GSheets Only)
`ARRAYFORMULA()` does not accomplish anything on its own; instead, it allows other formulas to be performed on an entire array of cells without the need to fill that formula throughout the entire range. For example, the `UPPER()` function can be applied to make all names in a column uppercase with a single formula call.

!!! note "Compatibility"

    While Excel has some array formula functionality, it is limited and clunky.  When such functionality is needed, you're best off transitioning to GSheets.

-   Documentation on `ARRAYFORMULA()` [from Google](https://support.google.com/docs/answer/3093275)

## Text Functions
Most spreadsheet functions operate on numbers. Text functions, by contrast, manipulate strings of text.

### `Upper` / `Lower` / `Proper`
The trio of `UPPER()`, `LOWER()`, and `PROPER()` are used to change whether text is represented in UPPERCASE, lowercase, or Proper Case.

#### `UPPER()`
`UPPER()` turns all characters of a string into their _UPPERCASE_ variants.

-   Documentation on `UPPER()` [from Google](https://support.google.com/docs/answer/3094219)

#### `LOWER()`
`LOWER()` turns all characters of a string into their _lowercase_ variants.

-   Documentation on `LOWER()` [from Google](https://support.google.com/docs/answer/3094083)

#### `PROPER()`
`PROPER()` capitalized the first letter of each word in a string; all subsequent letters are lowercase.

-   Documentation on `PROPER()` [from Google](https://support.google.com/docs/answer/3094133)

### `TRIM()`
`TRIM()` removes any spaces from the beginning and end of a string.

-   Documentation on `TRIM()` [from Google](https://support.google.com/docs/answer/3094140)

### `Left` / `Right` / `Mid`
The trio of `LEFT()`, `RIGHT()`, and `MID()` are used to extract portions of a string.

!!! note "Usage Note"

    If the value being extracted is a number, it will remain a string unless it is multiplied by 1 by adding `*1` to the end of the function call.

#### `LEFT()`
`LEFT()` returns the first _n_ characters of a string (by default, it returns only the first character).

-   Documentation on `LEFT()` [from Google](https://support.google.com/docs/answer/3094079)

#### `RIGHT()`
`RIGHT()` returns the last _n_ characters of a string (by default, it returns only the last character).

-   Documentation on `RIGHT()` [from Google](https://support.google.com/docs/answer/3094087)

#### `MID()`
`MID()` returns the _n_ characters of a string, starting at a designated point (e.g. the 3 characters starting from the 4th character).

-   Documentation on `MID()` [from Google](https://support.google.com/docs/answer/3094129)

### `LEN()`
`LEN()` returns the number of characters in a string.

-   Documentation on `LEN()` [from Google](https://support.google.com/docs/answer/3094081)

### `SEARCH()`
`SEARCH()` returns the starting location of a string inside another string; for example, indicating that the word _flower_ appears starting `4` characters into the word _sunflower_.

-   Documentation on `SEARCH()` [from Google](https://support.google.com/docs/answer/3094154)

### `SUBSTITUTE()`
`SUBSTITUTE()` functions like `SEARCH()`, except that once the string is located, it gets replaced by a different string.  For example, the _flower_ portion of _sunflower_ can be replaced with _shine_ to form _sunshine_.

-   Documentation on `SUBSTITUTE()` [from Google](https://support.google.com/docs/answer/3094215)

### `Split` / `TextJoin`
`SPLIT()` and `TEXTJOIN()` can be used to either combine or break up delimited strings of text.

#### `TEXTJOIN()`
`TEXTJOIN()` combines strings of text with a specified delimiter between them; for example, combining _John_ and _Smith_ to form _Smith, John_.

-   Documentation on `TEXTJOIN()` [from Google](https://support.google.com/docs/answer/7013992)

!!! note "Compatibility"

    For compatibility, use `TEXTJOIN()` instead of `JOIN()` since the former is supported in both Excel and GSheets, while the latter exists only in GSheets.

#### `SPLIT()`
`SPLIT()` does the opposite of `JOIN()`, breaking a string of text at a specified delimiter. Each resultant piece of text appears in its own column.

-   Documentation on `SPLIT()` [from Google](https://support.google.com/docs/answer/3094136)

### `CONCAT()` / `&`
`CONCAT()` and the ampersand (`&`) operator both concatenate (i.e. combine) strings end-to-end.

#### `CONCAT()`
`CONCAT()` combines strings end-to-end, though without a delimiter (separator) character. For delimited concatenation see `TEXTJOIN()`.

-   Documentation on `CONCAT()` [from Google](https://support.google.com/docs/answer/3094123?hl=en)

#### `&`
`&` also concatenates strings and takes up less space than a complete call to `CONCAT()`.

-  Documentation on `&` [from Google](https://support.google.com/docs/answer/3094123?hl=en)

## Date and Time Functions

### `TODAY()`
`TODAY()` returns the current date, encoded as a date object.

-   Documentation on `TODAY()` [from Google](https://support.google.com/docs/answer/3092984)

### `Day` / `Month` / `Year`
The trio of `DAY()`, `MONTH()`, and `YEAR()` are used to extract portions of a date object and return the results as integers.

#### `DAY()`
`DAY()` extracts the day (1-31) from a date object and returns the value as an integer.

-   Documentation on `DAY()` [from Google](https://support.google.com/docs/answer/3093040)

#### `MONTH()`
`MONTH()` extracts the month (1-12) from a date object and returns the value as an integer.

-   Documentation on `MONTH()` [from Google](https://support.google.com/docs/answer/3093052)

#### `YEAR()`
`YEAR()` extracts the Gregorian year from a date object and returns the value as an integer.

-   Documentation on `YEAR()` [from Google](https://support.google.com/docs/answer/3093061)

### `DAYS()`
`DAYS()` returns the number of days that have passed between two dates.

-   Documentation on `DAYS()` [from Google](https://support.google.com/docs/answer/9061296)

### `DATEDIF()` (GSheets Only)
`DATEDIF()` is a more sophisticated version of `DAYS()`, able to return the number of days/months/years and fractions of same that have elapsed between two dates.

-   Documentation on `DATEDIF()` [from Google](https://support.google.com/docs/answer/6055612)

### `NETWORKDAYS()`
`NETWORKDAYS()` is a more sophisticated version of `DAYS()`, able to return the number of _weekdays_ that have elapsed between two dates. `NETWORKDAYS()` can also exclude dates that are designated as holidays from the count.

-   Documentation on `NETWORKDAYS()` [from Google](https://support.google.com/docs/answer/3092979?hl=en)

### `NOW()`
`NOW()` returns the current time, encoded as a datetime object.

-   Documentation on `NOW()` [from Google](https://support.google.com/docs/answer/3092981?hl=en)

### `Hour` / `Minute` / `Second`
The trio of `HOUR()`, `MINUTE()`, and `SECOND()` are used to extract portions of a datetime object and return the results as integers.

#### `HOUR()`
`HOUR()` extracts the hour (0-23) from a datetime object and returns the value as an integer.

-   Documentation on `HOUR()` [from Google](https://support.google.com/docs/answer/3093045?hl=en)

#### `MINUTE()`
`MINUTE()` extracts the minute (0-59) from a datetime object and returns the value as an integer.

-   Documentation on `MINUTE()` [from Google](https://support.google.com/docs/answer/3093048)

#### `SECOND()`
`SECOND()` extracts the second (0-59) from a datetime object and returns the value as an integer.

-   Documentation on `SECOND()` [from Google](https://support.google.com/docs/answer/3093054)

## Error Handling Functions
Error Handling functions are used to detect when an expression throws an error (intended or unintended), or to change the behavior of an expression when an error is detected.

### `IFERROR()`
`IFERROR()` returns the value from an expression, unless that expression throws an error.  If the expression throws an error, than a second expression is returned instead.  Absent a second expression, the GSheets version of `IFERROR()` will return an empty cell (see warning below).

!!! warning "Usage Warning"

    Excel requires a second parameter, while GSheets does not.  This has the result that an Excel cell with an `IFERROR()` formula _will never be blank_, while in GSheets it's possible to return a blank cell.

-   Documentation on `IFERROR()` [from Google](https://support.google.com/docs/answer/3093304)

### `ISERROR()`
`ISERROR()` returns a value of _TRUE_ if the contained expression throws an error; otherwise it returns a value of _FALSE_.

-   Documentation on `ISERROR()` [from Google](https://support.google.com/docs/answer/3093349)

## Format Detection Functions
Format Detection functions return information based on the _data type_ of a cell, rather than its specific contents.

### `ISNUMBER()`
`ISNUMBER()` returns a value of _TRUE_ if the cell being referenced is formatted as a number, or _FALSE_ if the cell being referenced is not formatted as a number.

!!! note "Usage Note"

    Cells that contain numbers formatted as text will return _FALSE_.  Consequently, `ISNUMBER()` is often used in preprocessing tasks to ensure that numbers have been properly rendered.

### `N()`
`N()` returns a number, regardless of its input.  In particular:
-   If `N()` is passed a number formatted as text, it will return _0_
-   If `N()` is passed a _TRUE_ or _FALSE_, it will return _1_ or _0_ respectively
-   If `N()` is passed a date, it will return an integer.
-   If `N()` is passed text, it will return _0_

!!! note "Usage Note"

    `N()` is useful for converting booleans (_TRUE_ and _FALSE_) into numbers that can be added/multiplied, thus allowing multiple logical expressions to be combined.

-   Documentation on `N()` [from Google](https://support.google.com/docs/answer/3093357)

### `T()`
`T()` returns a text string, regardless of its input, while non-text strings are turned into blank cells.  In particular:
-   If `T()` is passed a number formatted as a number, it will return a blank cell.
-   If `T()` is passed a _TRUE_ or _FALSE_, it will return a blank cell.
-   If `T()` is passed a date, it will return a blank cell.
-   If `T()` is passed text, it will return the input string.

-   Documentation on `T()` [from Google](https://support.google.com/docs/answer/3094138)
