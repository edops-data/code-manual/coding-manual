# EdOps Development Stack

These are the software tools that you should have installed in order to

-   Run SIS queries
-   Generate dashboards
-   Contribute to the EdOps codebase

Not all tools are required; pick and choose as necessary depending on your needs.

## Querying a Student Information System
### DBeaver
DBeaver is our omni-purpose SQL editor of choice. While you're welcome to download any of the architecture-specific clients listed below, DBeaver is perfectly suited to supplant all of them.

[Detailed installation and configuration instructions](./Installation%20Guides/DBeaver.md) are available.

Apart from DBeaver, you may require additional utilities - particularly VPN clients - so read on.

### PowerSchool (and Oracle-based systems)
#### VPN
PowerSchool is hosted on the Microsoft Azure cloud, and a VPN is needed in order to access your instance. PowerSchool uses the F5 Access Client (a.k.a. F5 Edge or F5 Big IP).

[Detailed installation and configuration instructions](./Installation%20Guides/F5.md) are available.

#### Oracle SQLDeveloper
**Oracle SQLDeveloper** is a GUI frontend to Oracle databases, of which PowerSchool is one. SQLDeveloper can be downloaded from [Oracle](https://www.oracle.com/tools/downloads/sqldev-downloads.html) (a free account is required).

You will want to download the **Windows 64-bit with JDK Included** version. Unzip the downloaded file to the folder of your choice and run `SQLDeveloper.exe`.

[Detailed installation and configuration instructions](./Installation%20Guides/SQLDeveloper.md) are also available.

#### APEX
**APEX** is a SQL frontend to PowerSchool. To access, navigate to https://[LEA].powerschool.com:8443/ords. You will need to grant yourself APEX access in your user-level security settings as well.

[Detailed configuration instructions](./Installation%20Guides/APEX.md) are available.

#### Excel
Powerschool loves to export its files as tab-delimited (fine) files with an unrecognizable `.text` extension (not fine). To get these files to open in Excel by default, follow [these instructions](./Installation%20Guides/ExcelPowerSchool.md).

### Illuminate (and PostgreSQL-based systems)
#### OpenVPN
Only the EdOps home office in DC is whitelisted to access Illuminate. Therefore, if you are accessing Illuminate (versus other PostgreSQL systems) you will need to VPN into the EdOps office first. This requires OpenVPN.

[Detailed installation and configuration instructions](./Installation%20Guides/OpenVPN.md) for installing the OpenVPN client can be found here.

#### PGAdmin
**PGAdmin** is the official interface for PostgreSQL databases, of which Illuminate is one. PGAdmin can be downloaded from [pgadmin.org](https://www.pgadmin.org/).

Make sure to download the latest Windows (x64) version.

[Detailed installation and configuration instructions](./Installation%20Guides/PGAdmin.md) are also available.

### Cortex/SchoolZilla (and MS SQL-based systems)
#### OpenVPN
Only the EdOps home office in DC is whitelisted to access Illuminate. Therefore, if you are accessing Illuminate (versus other PostgreSQL systems) you will need to VPN into the EdOps office first. This requires OpenVPN.

[Detailed installation and configuration instructions](./Installation%20Guides/OpenVPN.md) for installing the OpenVPN client can be found here.

#### Microsoft SQL Server Management Studio (SSMS)
**SQL Server Management Studio** (SSMS) is the official interface for Microsoft SQL databases, of which Cortex and SchoolZilla are two. SSMS can be downloaded from [Microsoft](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15).

[Detailed installation and configuration instructions](./Installation%20Guides/SSMS.md) are also available.

## Generating Dashboards

You will need **Python** and **Git/Bash**, in addition to the appropriate VPN (if applicable) for your SIS.

Most dashboards come with a **3-click installer** that will install Python, Bash, and any dependencies, and can be found on the documentation page for that dashboard.

Detailed installation instructions, independent of product, are also available below.

### Python (Anaconda)
**Anaconda** is a popular Python package for data science, and it is the easiest way to get the necessary software to develop in Python.

While Python 2.7 can be downloaded for legacy purposes, all EdOps development takes place using **Python 3**.

[Detailed installation and configuration instructions](./Installation%20Guides/Anaconda.md) for installing Anaconda can be found here.

### Git/Bash
**Git** is the version control software used by EdOps and much of the programming world. It behaves best in a Unix/Linux environment, so it is typically installed alongside **Bash**, a popular Linux command line.

[Detailed installation and configuration instructions](./Installation%20Guides/Gitbash.md) for installing Git/Bash can be found here.

## Contributing to the EdOps Codebase

So, you want to add to our existing codebase. Great! Before adding to any of our existing projects, though, it's probably worth consulting with the Product & Tech Team so that you can get brought up to date on the status of our projects and get indoctrinated in our project management methodologies.

Meanwhile, you'll also want to install **VSCodium** for code editing and **Git/Bash** for source code management.

### VSCodium

While not required, **VSCodium** is our preferred code editor. (This position was formerly held by Atom, which is no longer under development.)

[Detailed installation and configuration instructions](./Installation%20Guides/VSCodium.md) for installing VSCodium can be found here.

### Git/Bash
**Git** is the version control software used by EdOps and much of the programming world. It behaves best in a Unix/Linux environment, so it is typically installed alongside **Bash**, a popular Linux command line.

[Detailed installation and configuration instructions](./Installation%20Guides/Gitbash.md) for installing Git/Bash can be found here.

### GitLab
**GitLab** is where EdOps stores source code. To receive a login, contact Adam.

GitLab supports multi-factor authentication (MFA). [Instructions for enabling MFA on GitLab](./Installation%20Guides/GitLabMFA.md) are available here.
