# Spreadsheet Function Guide

## Looking Up Data

### [Basic use of `VLOOKUP()` and `HLOOKUP()`](./Spreadsheet Function Guide/basiclookup.md)
This is a primer for specialists who are learning the ins and outs of these fundamental lookup functions.

### [Lookups when the key is to the right of the target data](./Spreadsheet Function Guide/leftlookup.md)
i.e. when regular `VLOOKUP()` syntax fails. Methods include:

-   Array Literals (GSheets)
-   Index/Match

### [Lookups on multiple keys](./Spreadsheet Function Guide/lookupmultiplekeys.md)
e.g. _Student ID_ **and** _Course Number_

### [Lookups that return multiple values](./Spreadsheet Function Guide/lookupreturnmultiple.md)
e.g. returning _Grade_ **and** _Percent_ for a student

## Generating Series of Numbers

### [Single list of consecutive numbers](./Spreadsheet Function Guide/consecutivenumbers.md)
i.e. _1, 2, 3, ..., n_

### [Repeating list of numbers](./Spreadsheet Function Guide/repeatinglist.md)
i.e. _1, 2, ..., n, 1, 2, ..., n..._

### [List of repeating numbers](./Spreadsheet Function Guide/listrepeating.md)
i.e. _1, 1, 2, 2, 3, 3..._

## Filters and Array Formulas

### [Basic use of Array Literals](./Spreadsheet Function Guide/arrayliteralbasics.md)
Basic use of arrays to stack sets of data on top of or next to one another.

### [Basic use of `UNIQUE()` and `SORT()`](./Spreadsheet Function Guide/basicuniquesort.md)
This is a primer for specialists who are learning the ins and outs of these fundamental array functions.

### [Basic use of `FILTER()`](./Spreadsheet Function Guide/basicfilter.md)
This is a primer for specialists who are new to the `FILTER()` function.

### [Intermediate use of `FILTER()`](./Spreadsheet Function Guide/intermediatefilter.md)
Includes array literals, nested filters, and the use of `VLOOKUP()` to identify whether values exist in a set.

### [Using `MOD()` to filter courses by term length](./Spreadsheet Function Guide/courselengthusingmod.md)
Applies to PowerSchool, where yearlong terms end in `00` and semesters end in `01`, `02`, etc.

## Tabulating Data

### [Basic use of `COUNTIFS()` and `SUMIFS()`](./Spreadsheet Function Guide/basiccountifs.md)
This is a primer for specialists who are learning the ins and outs of these fundamental aggregate functions.

### Tabulation with the `FILTER()` function
The `FILTER()` function permits far more advanced tabulation than `COUNTIFS()` and `SUMIFS()` alone. For users who have already mastered the aforementioned functions.
