# Git/Bash
Installation and Configuration

## Download Git/Bash

Navigate to [https://git-scm.com/downloads](https://git-scm.com/downloads) and download whatever version is inside the picture of a monitor.

![Git Downlaod](../images/gitdownload.png)

## Install Git/Bash

Open the file that you downloaded.

Permit the installer to make changes to your system (if you don't have admin permissions on your laptop, contact Josh or Xavier).

![Git UAC Permissions](../images/gituac.png)

Install to the default folder.

![Git Install Folder](../images/gitinstallfolder.png)

Use the default installation options:
-   [ ] Additional Icons
    -   [ ] On the Desktop
-   [x] Windows Explorer Integration
    -   [x] Git Bash Here
    -   [x] Git GUI Here
-   [x] Git LFS
-   [x] Associate `.git*` files with the default text editor
-   [x] Associate `.sh` files to be run with Bash
-   [ ] Use TrueType fonts in all console windows
-   [ ] Check daily for Git for Windows updates

![Git Install Options](../images/gitinstalloptions.png)

Set the default text editor to **Nano**.

![Git Nano](../images/gitinstallernano.png)

Permit Git to run from the **command line and 3rd-party software**.

![Git PATH Environment](../images/gitinstallerpath.png)

Use OpenSSL for HTTPS.

![Git HTTPS OpenSSL](../images/gitinstallerhttps.png)

Set to checkout Windows-style and commit Unix-style line endings.

![Git Line Endings](../images/gitinstallercheckout.png)

Use **MinTTY** as the console.

![Git Console](../images/gitinstallermintty.png)

Set the `git pull` behavior to **Default**.

![Git Install Git Pull](../images/gitinstallergitpull.png)

Set the extra options as follows:
-   [x] Enable file system caching
-   [x] Enable Git Credential Manager
-   [ ] Enable symbolic links

![Git Install Extras](../images/gitinstallerextras.png)

Leave the _Experimental Support for Pseudo Consoles_ unchecked.

![Git Install Experimental Support](../images/gitinstallerexperimental.png)
