# OpenVPN Client
Installation and configuration

## Download OpenVPN

Navigate to [https://openvpn.net/community-downloads/](https://openvpn.net/community-downloads/).

Click the **Download** icon for the **64-bit MSI Installer**.

![OpenVPN 64-bit Download](../images/openvpndownload.png)

!!! note DNS Firewall

    If the DNS firewall prevents your accessing the OpenVPN site, download the installer from [this link](../assets/OpenVPN-2.6.12-I001-amd64.msi).

## Install OpenVPN

Open the file that you just downloaded.

Allow the installer to make changes to the computer.

![OpenVPN UAC](../images/openvpnuac.png)

## Download the VPN Config File

Download [edops.ovpn](../assets/edops.ovpn) from this site.

## Configure OpenVPN

Open the OpenVPN client from the Start Menu, or from the Desktop icon.

![OpenVPN Start Menu Entry](../images/openvpnstartmenu.png)

### If no window appears

If no window appears, right-click on the OpenVPN icon in the system tray (that thing in the bottom right with the clock) and choose **Import File**. The icon resembles a computer with a padlock.

![OpenVPN Tray Icon](../images/openvpntrayicon.png)

Select the `edops.ovpn` file that you downloaded earlier.

![OpenVPN Import](../images/openvpnimport.png)

## Connect to the VPN

Right-click the OpenVPN icon in the system tray and choose **Connect**.

![OpenVPN Connect](../images/openvpnconnect.png)

It will ask for a password, which you can obtain by either a FreshDesk ticket or asking on Slack.

Watch in amazement as the connection information flows, and celebrate your successful connection.

![OpenVPN Status](../images/openvpnstatus.png)
