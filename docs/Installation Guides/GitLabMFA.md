# GitLab Multi-Factor Authentication
Configuration

## Sign in to GitLab

If you don't have a login, contact Adam.

![GitLab Login](../images/gitlablogin.png)

## Open User Account Settings

Click on your avatar and choose **Settings**.

![GitLab User Settings](../images/gitlabusersettings.png)

In the left-hand toolbar, open **Account**.

![GitLab Account Settings](../images/gitlabaccountsettings.png)

## Enable Two-Factor Authentication

Click on **Enable Two-Factor Authentication**.

![GitLab Enable MFA](../images/gitlabenablemfa.png)

Open Google Authenticator and select **Add Account**, followed by **Scan QR Code**.

![Google Authenticator Add Account](../images/gitlabauthaddaccount.png)

Scan the QR code, and then enter the 6-digit one-time-password into GitLab.
Click **Register with two-factor app**.

![Register MFA App](../images/gitlabregistermfa.png)

Download your recovery codes and put them somewhere safe.

![GitLab Recovery Codes](../images/gitlabrecoverycodes.png)

## Get an Access Token

In the left-hand toolbar, open **Access Tokens**.

![GitLab Access Token Settings](../images/gitlabaccesstokenlink.png)

Name your code `Git/Bash` and give it the following privileges:

-   `Read_Repository`
-   `Write_Repository`

Leave the _expiration date_ blank.

Click **Create Personal Access Token**.

![GitLab Git/Bash Token](../images/gitlabgitbashtoken.png)

Copy-paste the token and store it somewhere safe.

![GitLab Personal Access Token](../images/gitlabpersonalaccesstoken.png)

## Change your GitLab credentials

!!! note "Personal Access Token"

    Going forward, this personal access token is how you will authenticate with GitLab when using Git/Bash.

Click on the Start Menu and search for _Credential Manager_.

![Windows Credential Manager](../images/gitlabcredentialmanager.png)

Click on **Windows Credentials**.

![Windows Credentials](../images/gitlabwindowscredentials.png)

Under _Generic Credentials_ find your GitLab credentials and _remove_ them.

![Git Credentials](../images/gitlabgitcredentials.png)

Close Credential Manager.

On your Desktop, open a **Git/Bash** window.

Enter `git clone https://gitlab.com/edops-data/code-manual/hello-world` and press **Enter**.

Enter your credentials:

-   _Username_ is your GitLab username.
-   _Password_ is your **Personal Access Token**.

![Git Credential Manager](../images/gitlabcredentialmanager.png)

Enter `git config --global credential.helper store` and press **Enter**.

Type `exit` and press **Enter**.

![Git Bash Window](../images/gitlabgitbash.png)
