# Microsoft SQL Server Management Studio
Installation and Configuration

## SSMS Download

Navigate to [https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms)

Under the **Download SSMS** heading, click the download link.

![SSMS Download](../images/ssmsdownload.png)

## SSMS Installation

Open the file that you have downloaded.

Use the default installation folder.

![SSMS Installation Folder](../images/ssmsinstallationfolder.png)

## Configure an SSMS Connection

Open SQL Server Management Studio.

![SSMS Start Menu Entry](../images/ssmsstartmenu.png)

If a connection window doesn't load automatically, click on **Connect** | **Database Engine**.

![SSMS Connect Database Engine](../images/ssmsconnectdatabase.png)

Set the following parameters:

-   **Server Name**: as found in the _SIS IP_ column on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)
-   **Authentication**: `SQL Server Authentication`
-   **Login**: as found in the _SIS Server Username_ column on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)
-   **Password**: as found in the _SIS Server Password_ column on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107) 
