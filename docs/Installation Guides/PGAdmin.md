# PGAdmin
Installation and Configuration

## Download PGAdmin

Navigate to [https://www.pgadmin.org/download/pgadmin-4-windows/](https://www.pgadmin.org/download/pgadmin-4-windows/) and click on the latest version number.

![PGAdmin Latest Version](../images/pgadminlatestversion.png)

Download the **x64** version of PGAdmin.

![PGAdmin x64 version](../images/pgadminx64.png)

## Install PGAdmin

Open the file that you have downloaded.

Grant the installer permissions.

![PGAdmin UAC Prompt](../images/pgadminuac.png)

Use the default installation folder.

![PGAdmin Installation Folder](../images/pgadmininstallationfolder.png)

Use the default Start Menu folder.

![PGAdmin Start Menu Folder](../images/pgadminstartmenu.png)

Launch PGAdmin, either from the installer or from the Start Menu.

![PGAdmin Start Menu Launcher](../images/pgadminstartmenuentry.png)

You will be required to set a master password to access PGAdmin. (You'll probably want to save this to LastPass since if a month goes by and you forget it, you're out of luck.)

![PGAdmin Master Password](../images/pgadminmasterpassword.png)

## Configure a PGAdmin Connection

Launch PGAdmin and enter your master password.

![PGAdmin Master Password Entry](../images/pgadminmasterpasswordentry.png)

Navigate to **Object** | **Create** | **Server**

![PGAdmin Create Server](../images/pgadminobjectcreateserver.png)

Name your connection.

![PGAdmin Name Connection](../images/pgadminnameconnection.png)

Set the following parameters:

-   **Host Name/Address**: as found in the _SIS IP_ column on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)
-   **Port**: `5432`
-   **Maintenance Database**: as found in the _SID_ column on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)
-   **Username**: as found in the _SIS Server Username_ column on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)
-   **Password**: as found in the _SIS Server Password_ column on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)

![PGAdmin Server Settings](../images/pgadminserversettings.png)

Click **Save** to test and save the connection.
