# Python with Anaconda
Installation and Configuration

## Download Anaconda

Navigate to [https://www.anaconda.com/products/individual](https://www.anaconda.com/products/individual) and click on **Download**, which will scroll down to the _Downloads_ section.

Under **Windows**, download the **64-bit Graphical Installer** for **Python 3.x**.

![Anaconda Download](../images/anacondadownload.png)

There will be a pop-up about creating an account. **Don't**.  Spam city.

## Install Anaconda

Open the file that you downloaded.

When prompted, install for _Just Me_

![Anaconda Install for All Users](../images/anacondainstalljustme.png)

Keep the default destination.

![Anaconda Install Default Folder](../images/anacondadefaultfolder.png)

Add Anaconda to the `PATH`, and register it as the default Python interpreter.

![Anaconda Install Path](../images/anacondapathyes.png)
