# VSCodium
Installation and Configuration

## Download VSCodium

1.  Navigate to [https://github.com/VSCodium/vscodium/releases](https://github.com/VSCodium/vscodium/releases).
2.  Find (`ctrl-F` may assist) the line starting with **VSCodium-x64-** and ending in _just_ **.msi** (i.e. _not_ `.msi.sha1` or any other faff) and download.

## Install VSCodium

Open the file that you downloaded. Windows won't trust the file; tell it to do so anyway.
