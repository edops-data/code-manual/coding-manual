# PowerSchool APEX
Configuration

## Grant Yourself Access
From the PowerSchool Start Screen, select **Staff** and search for your name.

![Staff Search](../images/APEX_StaffSearch.png)

Open your **Security Settings**.

Navigate to the **Applications** tab and check the box for **APEX**. Create a new password and click **Submit**.

![APEX Settings](../images/APEX_SecuritySettings.png)

## Log into APEX
Navigate to https://[LEA].powerschool.com:8443/ords

Set the **Workspace** to `PS`

Log in using the credentials you created.

![APEX Login](../images/APEX_Signin.png)

Click on **SQL Workshop**, followed by **SQL Commands**.

Now you can create and test database queries.

![APEX Query](../images/APEX_Query.png)
