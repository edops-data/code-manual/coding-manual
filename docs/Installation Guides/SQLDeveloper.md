# SQLDeveloper (Oracle)
Installation and Configuration

## Download SQLDeveloper

Navigate to [https://www.oracle.com/tools/downloads/sqldev-downloads.html](https://www.oracle.com/tools/downloads/sqldev-downloads.html)

Under **Platform** find `Windows 64-bit with JDK 8 included` and click the corresponding **Download** link.

![SQLDeveloper Download Link](../images/sqldeveloperdownload.png)

Accept the terms and conditions.

![SQLDeveloper Terms and Conditions](../images/sqldevelopereula.png)

Log in to activate the download.  If you don't have a login, click on **Create Account**.  It's free, and Oracle is good at not spamming your inbox.

![Oracle Account Login](../images/oracleaccount.png)

## Install SQLDeveloper

Once the download completes, open the resultant `.zip` file.

![SQL Developer Zip File](../images/sqldeveloperzip.png)

Right click on **File Manager** and open a new window.

![Open File Explorer](../images/fileexplorer.png)

Navigate to `C:\Users\[Your Username]`

![User's Home Folder](../images/homefolder.png)

Drag the `sqldeveloper` folder from the compressed folder to your home folder.

![Drag and Drop Extraction](../images/sqldeveloperdragdrop.png)

Open the folder and right-click on `sqldeveloper.exe`.  Choose **Create Shortcut**.

![Create Shortcut](../images/createshortcut.png)

Drag the newly-created shortcut to your Desktop.

![Drag and Drop Shortcut](../images/shortcutdragdrop.png)

## Configure a SQLDeveloper Connection

This section assumes that you are connecting to a PowerSchool instance and that you have the VPN connected.

Open SQLDeveloper.

![SQLDeveloper Loading](../images/sqldeveloperloading.png)

In the **Connections** pane, click on the green plus sign.

![New Oracle Connection](../images/neworacleconnection.png)

Name your connection appropriately and set the following parameters:
-   **Username**: `PSNavigator`
-   **Password**: as found on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)
-   **Hostname**: IP address from the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)
-   **Port**: `1521`
-   **SID**: as found on the [Master VPN spreadsheet](https://docs.google.com/spreadsheets/d/1UCK47u4DUyMh1Ci03IMce4sFMKSAodw6_6C1J6UFQBs/edit#gid=54203107)

![Sample DB Connection](../images/sampledbconnection.png)

Click **Test** to confirm the settings, and then **Save**.
