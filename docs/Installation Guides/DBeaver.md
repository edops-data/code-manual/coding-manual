# DBeaver
Installation and Configuration

## Download DBeaver

Navigate to [The Microsoft Store](https://apps.microsoft.com/detail/9pnkdr50694p?rtc=1&hl=en-us&gl=US) and click on **Download**. 

## Install DBeaver

Open the file that you downloaded and follow the instructions, if any.
