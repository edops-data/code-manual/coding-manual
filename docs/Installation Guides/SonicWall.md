# SonicWall Global VPN
Installation and configuration

## Deprecation
!!! warn "Deprecated"

    We **no longer use** the SonicWall VPN. Instead, use the **OpenVPN** client, which can be accessed [here](./OpenVPN.md).  This page is left for reference purposes only.
    

## Download SonicWall

Navigate to [https://mysonicwall.com](https://mysonicwall.com) and log in. You will need to create a free account (fortunately they're good about spam).

In the left-hand Navigation Bar, scroll to **Resources and Support** and click **Download Center**.

![Sonic Wall Download Center Link](../images/sonicwalldownloadcenter.png)

Select **Global VPN Client (32-bit)** from the drop-down.

![Sonic Wall Product Drop-Down](../images/sonicwallproductdropdown.png)

Expand the entry for **Global VPN Client**.

![Sonic Wall Product Drop-Down Expanded](../images/sonicwalldropdownexpand.png)

Click the **Download** icon for the **64-bit version**.

![Sonic Wall 64-bit Download](../images/sonicwall64bitdownload.png)

## Install SonicWall

Open the file that you just downloaded.

Allow the installer to make changes to the computer.

![Sonic Wall UAC](../images/sonicwalluac.png)

Keep the installation directory unchanged. Set the installation to be for _Just Me_.

![Sonic Wall Installation Folder](../images/sonicwalljustme.png)

Right-click on the **Network** icon in the bottom-right of your screen and choose **Open Network and Internet Settings**.

![Windows Network Settings](../images/sonicwallnetworksettings.png)

Click on **Ethernet** followed by **Change Adapter Options**.

Right click on the **SonicWall VPN Connection** and choose **Enable**.

![Enable VPN Connection](../images/sonicwallenable.png)

## Configure SonicWall

Open the SonicWall VPN client from the Start Menu.

![Sonic Wall Start Menu Entry](../images/sonicwallstartmenu.png)

Click the **Plus** to add a new connection.

![Sonic Wall New Connection](../images/sonicwallnewconnection.png)

Enter the following information:
-   **IP Address or Domain Name**: `mail.goldstargroup.com`
-   **Connection Name**: `EdOps`

![Sonic Wall Settings](../images/sonicwallsettings.png)

Click **Enable** to start the connection, using the same credentials you use for your laptop.

![Sonic Wall Authentication](../images/sonicwallconnect.png)

Once the status displays **Connected** you're set to go.

![Sonic Wall Successful Connection](../images/sonicwallsuccess.png)
