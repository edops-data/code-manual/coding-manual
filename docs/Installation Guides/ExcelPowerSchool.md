# Configuring Excel to Work With PowerSchool
PowerSchool loves to export its tab-delimited data in `.text` files - an extension which is not normally recognized by Excel. The steps below will set Excel as the default application to open `.text` files, saving time, heartache, and copy-paste actions.

## Find the Excel Shortcut in the Start Menu
Click on the **Start** Menu and search for _Excel_. Then right-click on the shortcut and select **Open File Location**.

![Excel Start Menu Shortcut](../images/excelshortcut1.png)

## Find the Excel Shortcut in the File System
Repeat the process. Right-click on the _Excel_ shortcut and select **Open File Location**.

![Excel File System Shortcut](../images/excelshortcut2.png)

## Copy the Directory Path
Select the directory path (basically the address of the file) and copy it to the clipboard.

![Excel Directory Path](../images/excelpath.png)

## Open a `.text` file with _Open With_
Locate a `.text` file (probaly in your _Downloads_ folder), right-click, and select **Open With**. Choose **Choose Another App**.

![Open .text File With Another App](../images/excelchooseanotherapp.png)

## Look For Another App
Scroll through the list. If Excel is there, you win! Select it. If not (more likely), then scroll to the end and choose **Look for Another App on this PC**.

![Look For Another App](../images/excellookforanotherapp.png)

## Select `Excel.exe`
Paste the path that you copied into the **File name** box and press **Enter**, which will teleport you to the folder you found earlier. Select **`Excel.exe`** and click **Open**.

![Excel.exe](../images/excelexe.png)

## Success!
Excel is now the default app (or at the very least an option) for handling `.text` files. Celebrate your success with a lovely beverage.