# Kingdom of the Crystal SQL

This course consists of five _modules_, which are expected to be taken consecutively.

To earn the coveted **Certificate of Data-Basics**, you must complete a _performance task_, which can be requested from Adam.

## Course Materials
All Advanced materials take place in Google Sheets, owing to the use of array formulas.

!!! note "Prerequsite"

    Successful completion of the Intermediate course, or its performance task, should be considered a prerequisite.

### [The Basics](./Kingdom of the Crystal SQL/1-basics.md)
The fundamentals of a relational database; how to use the `SELECT` and `JOIN` keywords.

### [Aggregation, Grouping, and Ordering](./Kingdom of the Crystal SQL/2-aggregation.md)
How to `SUM()` and `COUNT()` records and the intricacies of aggregate functions; how to sort result sets.

### [Aliases, Cases, and the Joys of `NULL`s](./Kingdom of the Crystal SQL/3-aliases-cases-nulls.md)
As data sets get more complicated, some transformations are in order.

### [Unions and Data Types](./Kingdom of the Crystal SQL/4-unions-datatypes.md)
Combining sets of data vertically; common database data types.

### [Subqueries and Common Table Expressions](./Kingdom of the Crystal SQL/5-subqueries.md)
Combining queries to create grandeur.
