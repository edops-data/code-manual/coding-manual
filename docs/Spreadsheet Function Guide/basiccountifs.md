# Basic use of `COUNTIFS()` and `SUMIFS()`
This is a primer for specialists who are learning the ins and outs of these fundamental aggregate functions.

`COUNTIFS()` and `SUMIFS()` are extensions of the familiar `COUNT()` and `SUM()` functions that permit the counting or summation of values based on one or more _conditions_

## Example

In [BasicCountifs.xlsx](./assets/BasicCountifs.xlsx), the **Attendance** sheet contains all of the daily attendance records at _Apple Grove High School_ for _August-December 2019_. Our task is to calculate the _In-Seat Attendance (ISA)_ rate for each student on the **Students** sheet, and for each homeroom on the **Homerooms** sheet. Then, we will display some summary statistics on the **Stats** sheet.

!!! note "In-Seat Attendance"

    **In-Seat Attendance (ISA)** is defined as:
    '[Student-Days Present] / [Student-Days Enrolled]`
    where _student-days_ refers to the presence of one record, per student, per instructional day.

#### File Snapshot
| **Students** | **Homerooms** | **Stats** | **Attendance** |
| --- | --- | --- | --- |
| ![Basic Countifs Students](./images/BasicCountifs_Students.png) | ![Basic Countifs Homerooms](./images/BasicCountifs_Homerooms.png) | ![Basic Countifs Stats](./images/BasicCountifs_Stats.png) | ![Basic Countifs Attendance](./images/BasicCountifs_Attendance.png) |

### Using `COUNTIFS()` to Conditionally Count Records

#### Syntax

The `COUNTIFS()` function takes its arguments in **pairs**, where the first element of each pair is the _target column_, and the second pair is the _condition_.

```
COUNTIFS([column1], [condition1], [column2], [condition2]...)
```

#### Example A: Cell `Students!G2`

![Basic COUNTIFS Students!G2](./images/BasicCountifs_Students_G2.png)

The formula in cell `G2` reads as follows:

```
=COUNTIFS(Attendance!A:A, Students!A2)
```

The call to `COUNTIFS()` takes **one pair** of arguments:

-   `Attendance!A:A` contains our _Student Numbers_, and is the column that we are applying a condition to.
-   `Students!A2` is the _Student Number_ of our first student, and is the condition that we are applying to the column.

Together, these arguments instruct the function to count how many rows in the **Attendance** table belong to _Jenny Adams_.

---

#### Example B: Cell `Students!H2`

![Basic COUNTIFS Students!G2](./images/BasicCountifs_Students_H2.png)

The formula in cell `H2` reads as follows:

```
=COUNTIFS(Attendance!A:A, Students!A2, Attendance!H:H, "Present")
```

The call to `COUNTIFS()` takes **two pairs** of arguments:

-   `Attendance!A:A` contains our _Student Numbers_, and is the column that we are applying a condition to.
-   `Students!A2` is the _Student Number_ of our first student, and is the condition that we are applying to the column.
-   `Attendance!H:H` contains the student's _Present Status_ for the day, and is the column that tells us whether to count the record under _Days Present_.
-   `"Present"` is the value that we are counting.

Together, these arguments instruct the function to count how many times _Jenny Adams_ had a record in **Attendance** that was marked _Present_.

### Using `SUMIFS()` to Conditionally Sum Values

#### Syntax

Like `COUNTIFS()`, `SUMIFS()` takes its arguments in **pairs**, with the first element being the _target column_ and the second being the _condition_ applied to that column.

Additionally, the first argument to `SUMIFS()` is the column that's actually being summed.

```
SUMIFS([summation_column], [column1], [condition1], [column2], [condition2]...)
```

#### Example C: Cell `Homerooms!B2`

![Basic COUNTIFS Homerooms!B2](./images/BasicCountifs_Homerooms_B2.png)

The formula in cell `H2` reads as follows:

```
=SUMIFS(Students!G:G, Students!F:F, Homerooms!A2)
```

The call to `SUMIFS()` takes **one** summation column and **one pair** of arguments:

-   `Students!G:G` contains the _Days Enrolled_ and is the column being summed to get the total _Student-Days Enrolled_ for the homeroom.
-   `Students!F:F` contains the _Homeroom_ and is the column being filtered against.
-   `Homerooms!A2` contains the teacher of our first homeroom, and is the criterion against which `Students!F:F` is filtered.

Together, these criteria instruct `SUMIFS()` to aggregate the _Days Enrolled_ for all students in the _Bowe_ homeroom.

### Using `COUNTIFS()` With Relational Operators

In the previous examples, we directed `COUNTIFS()` and `SUMIFS()` to test for when a column **equals** a value. It's also possible to check for when a column is _greater than_ or _less than_ a value.

#### Example D: Cell `Stats!B2`

![Basic COUNTIFS Stats!B2](./images/BasicCountifs_Stats_B2.png)

The formula in cell `B2` reads as follows:

```
=COUNTIFS(Students!$D:$D, 9, Students!$I:$I, 1)/COUNTIFS(Students!$D:$D, 9)
```

There are two calls to `COUNTIFS()`:

-   The first call takes **two pairs** of arguments:
    -   The first pair instructs `COUNTIFS()` to count only students whose _Grade Level_ (`Students!D:D`) equals `9`.
    -   The second pair instructs `COUNTIFS()` to count only students whose _ISA_ (`Students!I:I`) equals `100%`.
-   The second call takes **one pair** of arguments, identical to the first pair from the first call.

---

#### Example E: Cell `Stats!B3`

![Basic COUNTIFS Stats!B3](./images/BasicCountifs_Stats_B3.png)

The formula in cell `B3` reads as follows:

```
=COUNTIFS(Students!$D:$D, 9, Students!$I:$I, ">="&0.95)/COUNTIFS(Students!$D:$D, 9)
```

There are two calls to `COUNTIFS()`:

-   The first call takes **two pairs** of arguments:
    -   The first pair instructs `COUNTIFS()` to count only students whose _Grade Level_ (`Students!D:D`) equals `9`.
    -   The second pair instructs `COUNTIFS()` to count only students whose _ISA_ (`Students!I:I`) is  _at least_ `95%`.
-   The second call takes **one pair** of arguments, identical to the first pair from the first call.

!!! note "Attaching Relational Operators"

    Relational operators need to be attached in a somewhat unconventional way, by enclosing the operator in quotes and concatenating it to the value with an ampersand (`&`).  For example, _less than 95%_ is coded as `"<="&0.95`

---

### Example F: `Stats!B4`

![Basic COUNTIFS Stats!B4](./images/BasicCountifs_Stats_B4.png)

The formula in cell `B4` reads as follows:

```
=COUNTIFS(Students!$D:$D, 9, Students!$I:$I, "<"&0.9)/COUNTIFS(Students!$D:$D, 9)
```

There are two calls to `COUNTIFS()`:

-   The first call takes **two pairs** of arguments:
    -   The first pair instructs `COUNTIFS()` to count only students whose _Grade Level_ (`Students!D:D`) equals `9`.
    -   The second pair instructs `COUNTIFS()` to count only students whose _ISA_ (`Students!I:I`) is  _less than_ `90%`.
-   The second call takes **one pair** of arguments, identical to the first pair from the first call.

## Official Documentation
### `COUNTIFS()`
Official documentation on `COUNTIFS()` from [Google](https://support.google.com/docs/answer/3256550?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/countifs-function-dda3dc6e-f74e-4aee-88bc-aa8c2a866842)

### `SUMIFS()`
Official documentation on `SUMIFS()` from [Google](https://support.google.com/docs/answer/3238496?hl=en) and from [Microsoft](https://support.microsoft.com/en-gb/office/sumifs-function-c9e748f5-7ea7-455d-9406-611cebce642b?ui=en-us&rs=en-gb&ad=gb)
