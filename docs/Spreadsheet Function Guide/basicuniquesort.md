# Basic Use of `UNIQUE()` and `SORT()`
This is a primer for specialists who are learning the ins and outs of these fundamental array functions.

## Introduction

`UNIQUE()` and `SORT()` are two of the most commonly used **array functions**, and represent a good introduction to how array functions work.

As their names suggest, `SORT()` sorts an array based on specific columns, while `UNIQUE()` returns each unique row only once - analogous to the _Remove Duplicates_ function in Excel.

Functionality is best shown by example.

!!! warning "Prerequisites"

    This guide assumes proficiency with the `FILTER()` function. If you need a refresher, see [Basic Use of `FILTER()`](./basicfilter.md)

    Additionally, this guide assumes proficiency with the `ISBLANK()` and `ISNUMBER()` format detection functions. If you need a refresher, see the examples in [Basic Use of `FILTER()`](./basicfilter.md)

## Example

In [Unique_Sort.xlsx](./assets/Unique_Sort.xlsx), the **StoredGrades** sheet contains all of the stored grades for the 10th graders at _Apple Grove High School_. In preparation for next year's scheduling, we need to determine which credits students have earned.

Complicating the matter is that many students have taken the same class or credit type multiple times. Therefore, we want a listing of **unique** credits earned by each student.

#### File Snapshot
| **EarnedCredits** | **StoredGrades** |
| --- | --- |
| ![Unique/Sort EarnedCredits](./images/UniqueSort_EarnedCredits.png) | ![Unique/Sort StoredGrades](./images/UniqueSort_StoredGrades.png) |

**Our goal is to form a listing of all credits earned by each student, with each credit represented only once**.

### Taking Unique Rows
We will use the `UNIQUE()` function to obtain a list of every **unique combination** of:

-   Student ID
-   Student Number
-   Student Name (`LastFirst`)
-   Course Number
-   Course Name
-   Credit Type

Fortunately, these are the first six columns in the **StoredGrades** table.

!!! note "Moving Columns"

    If they were not the first six columns, we would have three options:
    1.  Rearrange the columns
    2.  Switch to Google Sheets and use an array literal to select the columns we want
    3.  Use extra parameters in `UNIQUE()` to ignore the intervening columns (this, however, is beyond our scope)

#### Example A: Cell `EarnedCredits!A2`

![EarnedCredits A2](./images/UniqueSort_EarnedCredits_A2.png)

The formula in cell `A2` reads as follows:

```
=UNIQUE(FILTER(StoredGrades!A:F, ISNUMBER(StoredGrades!A:A)))
```

The `UNIQUE()` function takes a single parameter, which in this case is a `FILTER()` function.

The `FILTER()` is used to pull only rows that contain a numeric Student ID, so that we don't display the header row twice.

!!! note "Header Row"

    Instead of copy-pasting the header row and then filtering it back out, there would be nothing at all wrong with calling `UNIQUE(StoredGrades!A:F)` - which would place the header row at the top - and calling it a day.  However, that's boring, and we're here to learn.

    Besides, in the next step we'll need to filter out the header, so we might as well learn how it's done.

### Sorting Rows
Taking the unique rows was easy enough, now let's sort them to make the information more accessible. We want the data to be sorted:

-   Ascending by **name**
-   Descending by **credit type** (just to be difficult :) )

#### Example B: Cell `EarnedCredits!H2`

![EarnedCredits H2](./images/UniqueSort_EarnedCredits_H2.png)

The formula in cell `H2` reads as follows:

```
=SORT(
  SORT(
    FILTER(A:F, ISNUMBER(A:A))
  , 6, -1)
, 3, 1)
```

Because we have two sorts, we need to invoke the `SORT()` function twice.

!!! note "Google Sheets"

    Google Sheets does not have this limitation. A single call to `SORT()` can include multiple columns and directions.

_We will examine the function from the inside out._

-   The innermost call is `FILTER(A:F, ISNUMBER(A:A))` which is our familiar `FILTER()` call to pull only the rows with a Student ID, thus excluding the header row.
-   The innermost `SORT()` call sorts by the **6th column** (`Credit_Type`) in **descending** order (indicated by the `-1`).
-   The outermost `SORT()` call sorts by the **3rd column** (`LastFirst`) in **ascending** order (indicated by the `+1`).

## Official Documentation
### `UNIQUE()`
Official documentation on `UNIQUE()` from [Google](https://support.google.com/docs/answer/3093198?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/unique-function-c5ab87fd-30a3-4ce9-9d1a-40204fb85e1e)

### `SORT()`
Official documentation on `SORT()` from [Google](https://support.google.com/docs/answer/3093150?hl=en&ref_topic=3105422) and from [Microsoft](https://support.microsoft.com/en-us/office/sort-function-22f63bd0-ccc8-492f-953d-c20e8e44b86c)
