# Repeating List of Numbers
e.g. _1, 2, 3, ..., n, 1, 2, 3..._

Often, a list of consecutive numbers is needed, either to serve as identifiers for existing data, or to feed into formulas to generate new data.  Further, it is often necessary for this list to reset every _n_ elements, as in the case of numbering the days of the month.

Accomplishing this with traditional formulas is challenging because the addition or removal of a row can alter not just the current cycle, but potentially all subsequent cycles as well.

Through clever use of `FILTER()`, `ROW()`, and `MOD()`, this can be easily surmounted.

!!! warning "Prerequisites"

    This is a follow-up to [Generating a List of Consecutive Numbers](./consecutivenumbers.md), and familiarity with it is recommended.

    Additionally, experience with the `MOD()` function is strongly recommended. If you need a refresher, see [Using `MOD()` to filter courses by term length](./courselengthusingmod.md).

The examples that follow come from [RepeatingList.xlsx](./RepeatingList.xlsx)

#### File Snapshot
| **FieldTrip** | **Chaperones** |
| --- | --- |
| ![Repeating List FieldTrip](./images/RepeatingList_FieldTrip.png) | ![Repeating List Chaperones](./images/RepeatingList_Chaperones.png) |

## Numbering an Existing Data Set
In this example, we will parcel a data set into 12 groups, numbering them along the way.

### Introduction
The **FieldTrip** sheet contains a list of all 9th graders. To form chaperone groups, the 9th grade needs to be split into 12 groups, each of which will be assigned a teacher.

### Mechanics
For this example, we will number students from 1-12. Once 12 is reached, the counter should reset back to 1 for the next student.

#### Example A: Cell `FieldTrip!F2`

![FieldTrip Cell F2](./images/RepeatingList_FieldTrip_F2.png)

The formula in `FieldTrip!F2` reads as follows:

```
=FILTER(MOD(ROW(A:A)-2, 12)+1, ISNUMBER(A:A))
```

-   The first argument to `FILTER()` is `MOD(ROW(A:A)-2, 12)+1` which will be analyzed in parts:
    -   `ROW(A:A)-2` returns the row number of each cell in Column `A`. We subtract 2 so that the first student (on row `2`) has a value of `0` (we will see why we use `0` instead of `1` shortly).
    -   `12` is the divisor against which we are taking the remainder. In other words, each row number is divided by 12 and the remainder kept. This has the effect of giving us the numbers `0-11` over and over.
    -   `+1` is added onto the output of `MOD()` so that, instead of the numbers `0-11`, we get `1-12`.
-   The second argument to `FILTER()` is `ISNUMBER(A:A)`. This has the effect of continuing the numbering until we run out of numeric Student IDs in Column `A`.

## Cyclically Retrieving Items From a Data Set
In this example, we will attach a chaperone to each student by cycling through the list of chaperones in the same manner as we numbered the students.

### Introduction
The **FieldTrip** sheet contains a list of all 9th graders. In **Example A** we assigned each student a group number.  While it would be easy enough to create a numbered list of chaperones and `VLOOKUP()` each student's group number, let's be fancy and cycle through the chaperone list directly.

### Mechanics
For this example, we will cycle through the teachers on the **Chaperones** sheet, from `Accatino, Steve` to `Schmidt, Andrew G`. Once we reach Mr. Schmidt, we should loop back around to Mr. Accatino and continue this process until all students are assigned.

#### Example B: Cell `FieldTrip!G2`

![Field Trip Cell G2](./images/RepeatingList_FieldTrip_G2.png)

The formula in `FieldTrip!F2` reads as follows:

```
=FILTER(INDEX(Chaperones!B:B, MOD(ROW(A:A)-2, 12)+2), ISNUMBER(A:A))
```

!!! note "`INDEX()` Without `MATCH()`"

    This is one of the relatively few times that you will encounter `INDEX()` without `MATCH()`.

The `FILTER()` function is called identically to **Example A**.

The `INDEX()` function takes two arguments:

-   The first argument is `Chaperones!B:B`, which is the column containing our chaperone names.
-   The second argument is `MOD(ROW(A:A)-2, 12)+2`, which will be analyzed in parts:
    -   `ROW(A:A)-2` returns the row number of each cell in Column `A`. We subtract 2 so that the first student (on row `2`) has a row of `0` (we will see why we use `0` instead of `1` shortly).
    -   `12` is the divisor against which we are taking the remainder. In other words, each row number is divided by 12 and the remainder kept. This has the effect of giving us the numbers `0-11` over and over.
    -   `+2` is added onto the output of `MOD()` so that, instead of the numbers `0-11`, we get `2-13`.  These correspond to the rows in **Chaperones** with teachers.

!!! note "Adding 2"

    In **Example A** we added `1` so that the first student was in _Group 1_.  In **Example B** we are adding `2` so that the first name we pick up is on Row `2`.  If we only added `1`, the first group would have a chaperone named `LastFirst`.

## Official Documentation
### `FILTER()`
Official documentation on `FILTER()` from [Google](https://support.google.com/docs/answer/3093197?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/filter-function-f4f7cb66-82eb-4767-8f7c-4877ad80c759)

### `INDEX()`
Official documentation on `INDEX()` from [Google](https://support.google.com/docs/answer/3098242?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/index-function-a5dcf0dd-996d-40a4-a822-b56b061328bd)

### `ROW()`
Official documentation on `ROW()` from [Google](https://support.google.com/docs/answer/3093316?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/row-function-3a63b74a-c4d0-4093-b49a-e76eb49a6d8d)

### `MOD()`
Official documentation on `MOD()` from [Google](https://support.google.com/docs/answer/3093497?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/mod-function-9b6cd169-b6ee-406a-a97b-edf2a9dc24f3)

### `ISNUMBER()`
Official documentation on `ISNUMBER()` from [Google](https://support.google.com/docs/answer/3093296?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/is-functions-0f2d7971-6019-40a0-a171-f2d869135665)
