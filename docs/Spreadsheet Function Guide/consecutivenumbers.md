# Generating a List of Consecutive Numbers
Often, a list of consecutive numbers is needed, either to serve as identifiers for existing data, or to feed into formulas to generate new data.

Traditionally, such sequences have been generated by filling down a formula that operates on the cell above (e.g. `=A2+1`). This method has several drawbacks, namely:

-   Formulas must be filled down to account for all the necessary cells, leading to issues if the data exceeds the budgeted cells
-   Numbering does not automatically recalibrate to handle added or deleted rows

Through clever use of the `FILTER()` and `ROW()` functions, one or both of these drawbacks can be surmounted.

!!! warning "Prerequisites"

    This guide assumes proficiency with the `FILTER()` function. If you need a refresher, see [Basic Use of `FILTER()`](./basicfilter.md)

    Additionally, this guide assumes proficiency with the `ISBLANK()` and `ISNUMBER()` format detection functions. If you need a refresher, see the examples in [Basic Use of `FILTER()`](./basicfilter.md)

The examples that follow come from [ConsecutiveNumbers.xlsx](./ConsecutiveNumbers.xlsx)

#### File Snapshot

| **Higgins** | **Calendar** | **Ruler** |
| --- | --- | --- |
| ![Consecutive Numbers Higgins](./images/ConsecutiveNumbers_Higgins.png) | ![Consecutive Numbers Calendar](./images/ConsecutiveNumbers_Calendar.png) | ![Consecutive Numbers Ruler](./images/ConsecutiveNumbers_Ruler.png) |

## Numbering an Existing Data Set
In this example, we will generate consecutive numbers for the duration of a data set, thereby numbering the rows.

### Introduction
The row numbers of the spreadsheet provide a handy, ready-made set of consecutive integers that

-   Stretches to the bottom of your data set
-   Automatically renumbers if a row is added or deleted

We would be fools not to use such a gift from above!

### Mechanics
For this example, we will number the students in Mr. Higgins' Homeroom class consecutively from `1-n`

#### Example A: Cell `Higgins!A2`

![Higgins Cell A2](./images/ConsecutiveNumbers_Higgins_A2.png)

The formula in `Higgins!A2` reads as follows:

```
=FILTER(ROW(B:B)-1, ISNUMBER(B:B))
```

-   The first argument instructs `FILTER()` to return the row number from Column `B`, decremented by 1 so that the first number occurs on Row 2.
-   The second argument instructs `FILTER()` to only return these row numbers for rows that have a number in Column `B` - in other words, only for rows that contain a student.

!!! note "Bonus - Automatic Incrementation"

    Try adding or removing a row.  The row numbers update automatically!

## Generating Numbers to Feed a Formula
In this example, we will generate a collection of consecutive numbers that can be used as parameters for another function.

### Introduction
As before, we can use the row numbers as a built-in set of consecutive integers. The catch this time is that we need to tell the function where to stop. We can do this in either of two ways:

-   The numbers 1-_n_ that says adjusts to stay 1-_n_, even if rows are added or removed
-   The numbers 1-_n_ that becomes 1-_n+1_ if a row is added, or 1-_n-1_ if a row is removed

### Mechanics
-   For **Example B** we will generate a range, numbered 1-31, so that the dates of May, 2021 can be enumerated.  If a row is deleted, the numbers will adjust to contain exactly 31 days.

-   For **Example C** we will generate a cell for every inch between 5-foot-2 and 6-foot-9, but will do so in a manner that expands the list if a row is added or contracts it if a row is removed.

#### Example B: Cell `Calendar!A2`

![Calendar A2](./images/ConsecutiveNumbers_Calendar_A2.png)

The formula in `Calendar!A2` reads as follows:

```
=FILTER(ROW(B:B), ROW(B:B)<=31)
```

-   The first argument instructs `FILTER()` to return the row number from Column `B`.  Unlike **Example A**, we do not need to decrement the row number.
-   The second argument instructs `FILTER()` to stop after 31 rows.

!!! note "Circular References"

    Since our formula is in Column `A`, we use Column `B` in our arguments to avoid circular references.

---

#### Example C: Cell `Ruler!A2`

![Ruler A2](./images/ConsecutiveNumbers_Ruler_A2.png)

The formula in `Ruler!A2` reads as follows:

```
=FILTER(ROW(B:B), ROW(B:B)<=ROW(B20))
```

-   The first argument is unchanged from **Example B**, and instructs `FILTER()` to return the row number from Column `B`.
-   The second argument instructs `FILTER()` to stop after 20 rows, but does so by calling cell `B20` instead of a specific number.  This way, if rows are added or removed, the reference will adjust accordingly.

## Official Documentation
### `FILTER()`
Official documentation on `FILTER()` from [Google](https://support.google.com/docs/answer/3093197?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/filter-function-f4f7cb66-82eb-4767-8f7c-4877ad80c759)

### `ROW()`
Official documentation on `ROW()` from [Google](https://support.google.com/docs/answer/3093316?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/row-function-3a63b74a-c4d0-4093-b49a-e76eb49a6d8d)

### `ISNUMBER()`
Official documentation on `ISNUMBER()` from [Google](https://support.google.com/docs/answer/3093296?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/is-functions-0f2d7971-6019-40a0-a171-f2d869135665)
