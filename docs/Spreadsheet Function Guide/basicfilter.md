# Basic Use of `FILTER()`
This is a primer for specialists who are new to the `FILTER()` function.

## Video
A video of this tutorial is available on Google Drive.
[![Video Thumbnail](./images/BasicFilter_VideoThumbnail.png)](https://drive.google.com/file/d/1S1NNT_x6eMLEZPf0SsFwaI_o-Nagl8G6/view?usp=sharing)

## Introduction

The `FILTER()` function is among the most useful functions in the spreadsheet arsenal. Just think of how many times you filter a spreadsheet and then either delete rows to get the set you want, or copy-paste the filtered set to a new sheet. Now you can do all that work formulaically.

In addition, gone are the days of filling down formulas and hoping your data doesn't gain a row. Instead, the `FILTER()` function will fill down your formula exactly as far as it needs to go, no matter how many rows are added (or removed) from your sheet.

!!! warning "Prerequisites"

    This guide assumes proficiency with the `VLOOKUP()` function.  If you need a refresher, see [Lookup Basics](./basiclookup.md).

But enough talk, let's look at some examples.

The examples that follow come from the [BasicFilter](https://docs.google.com/spreadsheets/d/18Gg2PEBui-7cvKIU5JGx-23oPTCy0gFgdmiuI4pt-6g) file on Google Drive.

!!! note "Excel vs. GSheets"

    While Excel does have `FILTER()` functionality, it's limited to one criterion. GSheets, meanwhile, can filter by multiple criteria so for the sake of demonstration we will stick to GSheets here.

## Example

In the [BasicFilter](https://docs.google.com/spreadsheets/d/18Gg2PEBui-7cvKIU5JGx-23oPTCy0gFgdmiuI4pt-6g) file, the **Students** sheet contains all of the 11th grade students at _Apple Grove High School_, and the **StoredGrades** sheet contains all of their SY19-20 grades through Semester 1.

Starting Semester 2, any student who failed either Math or English during Semester 1 will be required to sacrifice an elective period for Study Hall.

#### File Snapshot
| **S1_ELA** | **S1_Math** | **Students** | **StudyHall** | **StoredGrades** |
| --- | --- | --- | --- | --- |
| ![BasicFilter S1_ELA](./images/BasicFilter_S1ELA.png) | ![BasicFilter S1_Math](./images/BasicFilter_S1Math.png) | ![BasicFilter Students](./images/BasicFilter_Students.png) | ![BasicFilter StudyHall](./images/BasicFilter_StudyHall.png) | ![BasicFilter StoredGrades](./images/BasicFilter_StoredGrades.png) |

**Our goal is to identify the students who failed Math or English during Semester 1**.

### Filtering an Array By One or More Conditions

Right now, the **StoredGrades** sheet contains _Quarter 1_ and _Quarter 2_ grades, in addition to the _Semester 1_ grades that we're after.

Conceivably, we could just filter the table and delete those rows, but if we ever have to update the **StoredGrades** sheet, then we would have to do it all over again

Instead, we can use the `FILTER()` function to save some effort.

#### Example A: Cell `S1_ELA!A2`

![S2_ELA Cell A2](./images/BasicFilter_S1ELA_A2.png)

The formula in cell `A2` reads as follows:

```
=filter(StoredGrades!A:O, StoredGrades!M:M="S1", StoredGrades!G:G="ENG")
```

The three arguments are as follows:

-   `StoredGrades!A:O` is the array that we are filtering
-   The remaining two arguments define the filters:
    -   `StoredGrades!M:M` is the column containing the `StoreCode`, and we only want rows where it is equal to `S1`
    -   `StoredGrades!G:G` is the column containing the `Credit_Type`, and we only want rows where it is equal to `ENG`

!!! note "Syntax"

    Unlike `COUNTIFS()`, `SUMIFS()`, and related functions, `FILTER()` does not take its arguments as comma-delimited pairs. To state that Column `A:A` must equal `19`, you can enter `A:A=19` rather than `A:A, 19`.

---

#### Example B: Cell `S1_Math!A2`

![S2_ELA Cell A2](./images/BasicFilter_S1Math_A2.png)

The formula in cell `A2` reads as follows:

```
=filter(StoredGrades!A:O, StoredGrades!M:M="S1", StoredGrades!G:G="MAT")
```

The three arguments are:

-   `StoredGrades!A:O` is the array that we are filtering
-   The remaining two arguments define the filters:
    -   `StoredGrades!M:M` is the column containing the `StoreCode`, and we only want rows where it is equal to `S1`
    -   `StoredGrades!G:G` is the column containing the `Credit_Type`, and we only want rows where it is equal to `MAT`

### Using `FILTER()` to auto-fill a formula

Now that we have our Semester 1 grades for English and Math on their respective sheets, it's time to use `VLOOKUP()` to attach them to students.

This provides us with an opportunity to explore another aspect of `FILTER()`: it converts most standard formulas into **array** formulas, capable of operating on multiple rows at once.

Normally, one would just write a single `VLOOKUP()` formula and fill it down the column. Using a `FILTER()` function, however, we can do it with a _single formula_ which, in addition to making us feel positively _L33T_, means fewer formulas to keep track of.

To do this, we invoke the `ISNUMBER()` function, which is introduced below.

### `ISNUMBER()` Function: A Primer

The `ISNUMBER()` function does more or less what it says on the tin: if it is passed a number, it returns `TRUE`; otherwise, it returns `FALSE`.  Importantly, **numbers that are formatted as text will return `FALSE`**.

| _x_ | `ISNUMBER(x)` | Comments |
| --- | --- | --- |
| `21` |  `TRUE` | 21 is a number. |
| `3/7/2020` | `TRUE` | Dates are actually stored as numbers. |
| `TRUE` | `FALSE` | Booleans are not stored as numbers, but can be converted to them with `N()` |
| `Hello` | `FALSE` | Hello is not a number. |
| `'21'` | `FALSE` | This 21 was formatted as text. |

#### Example C: Cell `Students!D2`

![Students Cell D2](./images/BasicFilter_Students_D2.png)

The formula in cell `D2` reads as follows:

```
=filter(vlookup(A:A, S1_ELA!B:J, 9, false), isnumber(A:A))
```

The two arguments to `FILTER()` are:

-   `vlookup(A:A, S1_ELA!B:J, 9, false)` is a standard call to `VLOOKUP()`.  If this is unfamiliar, consult [Basic Lookup](./basiclookup.md).
-   `isnumber(A:A)` which tells `FILTER()` to keep only rows which contain numbers in the `Student_Number` column.

!!! note "Why filter by `ISNUMBER(A:A)`?"

    Filtering by `ISNUMBER(A:A)` tells the `FILTER()` function to start using the `VLOOKUP()` function as soon as it sees a student record (i.e. a record with a `Student_Number`, and, more importantly, to _stop_ calling the `VLOOKUP()` as soon as it runs out of students.

    Omitting this filter would lead to hundreds of empty cells with lookup errors in them.

---

#### Example D: Cell `Students!E2`

![Students Cell E2](./images/BasicFilter_Students_E2.png)

The formula in cell `E2` reads as follows:

```
=filter(vlookup(A:A, S1_Math!B:J, 9, false), isnumber(A:A))
```

The two arguments to `FILTER()` are:

-   `vlookup(A:A, S1_Math!B:J, 9, false)` is a standard call to `VLOOKUP()`.
-   `isnumber(A:A)` which tells `FILTER()` to keep only rows which contain numbers in the `Student_Number` column.

### Using `FILTER()` With an `OR` Operator

All of our filters so far have implicitly had the `AND` operator on their criteria: for example we filtered by _Semester 1_ **and** _ELA_, or _Semester 1_ **and** _Math_.

In order to pull the students for Study Hall, though, we want students who failed Math **or** who failed English.

This is easy enough to accomplish using a minor change in syntax.

#### Example E: Cell `StudyHall!A2`

![Students Cell E2](./images/BasicFilter_StudyHall_A2.png)

The formula in cell `A2` reads as follows:

```
=filter(Students!A:E, (Students!D:D="F")+(Students!E:E="F"))
```

As before, this `FILTER()` call has three arguments: a source array and two criteria. However, this time the criteria are enclosed in parentheses and _added_ together, telling `FILTER()` that **either** condition is acceptable.

!!! note "Why Addition?"

    Inside the spreadsheet engine, any `TRUE/FALSE` value gets converted to `1/0`, allowing them to be added or multiplied. When it's time to interpret the values, `0` is turned back into `FALSE` and any positive number is turned into `TRUE`.

    (Yes this is an oversimplification. Sue me.)

### Errors
#### Collisions with Existing Content

If the filter, when fully expanded, collides with existing non-empty cells, then a `#REF` error will result. (In this example, rows 3 and below were copy-pasted, interfering with the `FILTER()` formula.)

![Filter #REF Error](./images/BasicFilter_Error_Ref.png)

## Official Documentation
### `FILTER()`
Official documentation on `FILTER()` from [Google](https://support.google.com/docs/answer/3093197?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/filter-function-f4f7cb66-82eb-4767-8f7c-4877ad80c759)

### `ISNUMBER()`
Official documentation on `ISNUMBER()` from [Google](https://support.google.com/docs/answer/3093296?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/is-functions-0f2d7971-6019-40a0-a171-f2d869135665)
