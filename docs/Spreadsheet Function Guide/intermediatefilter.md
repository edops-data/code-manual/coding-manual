# Intermediate Use of `FILTER()`
Nested filters and the use of `VLOOKUP()` to identify whether values exist in a set.

## Introduction

Elsewhere in this manual, we have used the `FILTER()` function for two primary purposes:

-   Filtering an array to return only rows that match one or more conditions
-   Automatically filling down a formula through an array, stopping when data runs out.

Here, we will explore intermediate use of the `FILTER()` function, including:

-   Nested filters and array literals to construct modified arrays for filtering
-   Use of the `VLOOKUP()` function to filter based on whether a value exists in a secondary array

!!! warning "Prerequisites"

    This guide is a sequel to the [Basic Use of `FILTER()`](./basicfilter.md) guide, and full proficiency with its components is expected.

    Additionally, this guide assumes proficiency with the `VLOOKUP()` function.  If you need a refresher, see [Lookup Basics](./basiclookup.md).

The examples that follow come from the [IntermediateFilter](https://docs.google.com/spreadsheets/d/1WoW2AY0FnLlW0nX9WqegTURTotoZRUjmLld2fVPXhbw) file on Google Drive.

#### File Snapshot

| **MAP** | **ELA** | **Students** | **MakeUp** |
| --- | --- | --- | --- |
| ![IntermediateFilter MAP](./images/IntermediateFilter_MAP.png) | ![IntermediateFilter ELA](./images/IntermediateFilter_ELA.png) | ![IntermediateFilter Students](./images/IntermediateFilter_Students.png) | ![IntermediateFilter MakeUp](./images/IntermediateFilter_MakeUp.png) |

!!! note "Excel vs. GSheets"

    While Excel does have `FILTER()` functionality, it's limited. To explore the full functionality of `FILTER()`, we will use GSheets, which has a far more versatile implementation.

## Example

The **MAP** sheet contains all of the Winter MAP scores for _Apple Grove High School_.  In addition to the usual **RIT** and **CGP** scores, we are interested in the **standard-level** scores in Columns `BM:CN`, a preview of which are below:

| RITtoReadingScore | RITtoReadingMin | RITtoReadingMax | Goal1Name                            | Goal1RitScore | Goal1StdErr | Goal1Range | Goal1Adjective | Goal2Name                                     | Goal2RitScore | Goal2StdErr | Goal2Range | Goal2Adjective | Goal3Name                                 | Goal3RitScore | Goal3StdErr | Goal3Range | Goal3Adjective | Goal4Name                                          | Goal4RitScore | Goal4StdErr | Goal4Range | Goal4Adjective | Goal5Name                       | Goal5RitScore | Goal5StdErr | Goal5Range | Goal5Adjective |
|-------------------|-----------------|-----------------|--------------------------------------|---------------|-------------|------------|----------------|-----------------------------------------------|---------------|-------------|------------|----------------|-------------------------------------------|---------------|-------------|------------|----------------|----------------------------------------------------|---------------|-------------|------------|----------------|---------------------------------|---------------|-------------|------------|----------------|
|                   |                 |                 | Operations and Algebraic Thinking    |           209 |         6.4 | 203-215    | Low            | The Real and Complex Number Systems           |           221 |         6.4 | 215-227    | LoAvg          | Geometry                                  |           205 |           6 | 199-211    | Low            | Statistics and Probability                         |           220 |         6.4 | 214-226    | LoAvg          |                                 |               |             |            |                |
| 910L              | 810L            | 960L            | Literary Text: Key Ideas and Details |           215 |         7.4 | 208-222    | LoAvg          | Literary Text: Language, Craft, and Structure |           208 |         6.9 | 201-215    | LoAvg          | Informational Text: Key Ideas and Details |           213 |         7.7 | 205-221    | LoAvg          | Informational Text: Language, Craft, and Structure |           213 |         7.7 | 205-221    | LoAvg          | Vocabulary: Acquisition and Use |           209 |           8 | 201-217    | LoAvg          |
|                   |                 |                 | Operations and Algebraic Thinking    |           202 |         5.8 | 196-208    | Low            | The Real and Complex Number Systems           |           192 |         5.9 | 186-198    | Low            | Geometry                                  |           196 |         5.8 | 190-202    | Low            | Statistics and Probability                         |           205 |         5.7 | 199-211    | Low            |                                 |               |             |            |                |
| 260L              | 160L            | 310L            | Literary Text: Key Ideas and Details |           182 |         6.6 | 175-189    | Low            | Literary Text: Language, Craft, and Structure |           171 |         8.2 | 163-179    | Low            | Informational Text: Key Ideas and Details |           192 |         8.5 | 184-201    | Low            | Informational Text: Language, Craft, and Structure |           176 |         8.3 | 168-184    | Low            | Vocabulary: Acquisition and Use |           181 |         8.2 | 173-189    | Low            |
|                   |                 |                 | Operations and Algebraic Thinking    |           226 |         6.3 | 220-232    | LoAvg          | The Real and Complex Number Systems           |           214 |         6.2 | 208-220    | Low            | Geometry                                  |           221 |           6 | 215-227    | LoAvg          | Statistics and Probability                         |           209 |           6 | 203-215    | Low            |                                 |               |             |            |                |
| 990L              | 890L            | 1040L           | Literary Text: Key Ideas and Details |           226 |         8.5 | 218-235    | HiAvg          | Literary Text: Language, Craft, and Structure |           204 |         8.2 | 196-212    | Low            | Informational Text: Key Ideas and Details |           213 |         7.7 | 205-221    | LoAvg          | Informational Text: Language, Craft, and Structure |           225 |         8.6 | 216-234    | Avg            | Vocabulary: Acquisition and Use |           213 |         7.3 | 206-220    | LoAvg          |
|                   |                 |                 | Operations and Algebraic Thinking    |           209 |         6.1 | 203-215    | Low            | The Real and Complex Number Systems           |           218 |           6 | 212-224    | LoAvg          | Geometry                                  |           212 |         6.1 | 206-218    | Low            | Statistics and Probability                         |           210 |         6.3 | 204-216    | Low            |                                 |               |             |            |                |
| 870L              | 770L            | 920L            | Literary Text: Key Ideas and Details |           210 |         7.2 | 203-217    | LoAvg          | Literary Text: Language, Craft, and Structure |           206 |         7.8 | 198-214    | Low            | Informational Text: Key Ideas and Details |           205 |         7.5 | 198-213    | Low            | Informational Text: Language, Craft, and Structure |           222 |         8.2 | 214-230    | Avg            | Vocabulary: Acquisition and Use |           205 |         7.5 | 198-213    | Low            |

At first glance, the biggest issue with the data becomes clear: the specific content standards are scattered among five columns, making it nearly impossible to suss out how everybody did on a particular standard.

Fortunately, we can solve this with some intensive use of array literals and the `FILTER()` function.

### (Review) Pulling RIT and CGP using `VLOOKUP()` and `FILTER()`

The **ELA** sheet contains a listing of all students who took the Winter MAP assessment. We begin by pulling their **RIT** and **CGP** scores with a single formula.

#### Example A: Cell `ELA!D2`

![Intermediate Filter ELA!D2](./images/IntermediateFilter_ELA_D2.png)

The formula in cell `D2` reads as follows:

```
=filter(vlookup(A:A, {MAP!H:H, MAP!Z:Z, MAP!AN:AN}, {2, 3}, false), isnumber(A:A))
```

We will ignore the `FILTER()` wrapper for now. If you're uncertain how it operates, see [Basic Use of Filter](./basicfilter.md). Instead, we will focus on the `VLOOKUP()` call, which has 4 arguments:

-   `A:A` - this is our _key_ column containing the Student IDs that we are looking up
-   `{MAP!H:H, MAP!Z:Z, MAP!AN:AN}` - There's nothing stopping us from just selecting `MAP!H:AN` and counting columns to form our `VLOOKUP()` statement. However, that's just too. many. columns. Instead, we use an array literal to simplify the process. The array literal contains only the three columns we need:

    -   Our _key_ column `MAP!H:H`
    -   Our _RIT_ column `MAP!Z:Z`
    -   Our _CGP_ column `MAP!AN:AN`
-   `{2, 3}` - this array literal instructs `VLOOKUP()` to return values from _both_ the second (RIT) and third (CGP) columns, reducing the number of function calls.
-   `FALSE` to return only exact maches, per usual.

### Naming the Standards Using `TRANSPOSE()`

Just staring at the standards information in `MAP!BM:CN` it's impossible to discern just how many unique standards there are. Rather than eyeball the situation, we will use a `UNIQUE()` function to pull the standards, and then **transpose** them so that they appear as column headers.

!!! note "Transpose"

    Transposing an array basically flips it on its side. Here, we will use it to turn a _column_ of standards into a _row_ of headers.

#### Example B: Cell `ELA!F1`

![Intermediate Filter ELA!F1](./images/IntermediateFilter_ELA_F1.png)

The formula in cell `F1` reads as follows:

```
=transpose(
  unique(
    filter({MAP!BP:BP;MAP!BU:BU;MAP!BZ:BZ;MAP!CE:CE;MAP!CJ:CJ},
      {MAP!N:N;MAP!N:N;MAP!N:N;MAP!N:N;MAP!N:N}="Reading"
    )
  )
)
```

We will examine the formula from the inside out.

-   The `FILTER()` call uses two array literals:
    -   `{MAP!BP:BP;MAP!BU:BU;MAP!BZ:BZ;MAP!CE:CE;MAP!CJ:CJ}` is a stack of the _Goal Name_ columns, separated by semicolons so that they are stacked _vertically_ into a single mega-column
    -   `{MAP!N:N;MAP!N:N;MAP!N:N;MAP!N:N;MAP!N:N}` is a stack of the _Discipline_ column, stacked five times so that it matches the _Goal Name_ stack in length
-   The `FILTER()` function filters the _Goal Name_ column such that only goals associated with a `Reading` _Discipline_ are returned
-   `UNIQUE()` returns only the unique goals, still as a column
-   `TRANSPOSE()` transforms this single column into a single row

### Pulling a Standard Score using Nested Filters

With the easy work out of the way, we are ready to pull the standard scores. Conceptually, here's what will happen:

-   In the **MAP** sheet, the standard scores come in 5 sets of five columns (Goals 1-5). We will use an array literal to stack these 5 sets, so that all of the standard names appear in one giant column, and the scores next to them in another giant column.
-   We will filter the scores so that only those pertaining to the currently selected standard are returned.
-   Using `VLOOKUP()` we will look up each student's score from the array
-   The entire operation is wrapped in a `FILTER()` function to automatically fill down.

#### Example C: Cell `ELA!F2`

![Intermediate Filter ELA!F2](./images/IntermediateFilter_ELA_F2.png)

The formula in cell `F2` reads as follows:

```
=filter(
  vlookup($A:$A,
    filter({
        {MAP!$H:$H;MAP!$H:$H;MAP!$H:$H;MAP!$H:$H;MAP!$H:$H}, {MAP!$BQ:$BQ;MAP!$BV:$BV;MAP!$CA:$CA;MAP!$CF:$CF;MAP!$CK:$CK}
      },
      {MAP!$BP:$BP;MAP!$BU:$BU;MAP!$BZ:$BZ;MAP!$CE:$CE;MAP!$CJ:$CJ}=F$1
      ), 2, false
    ), isnumber($A:$A)
)
```

As before, we will examine this monster from the inside out.

The innermost `FILTER()` call is used to filter the standard scores by standard:

-   The array `{{MAP!$H:$H;...MAP!$H:$H}, {MAP!$BQ:$BQ;...MAP!$CK:$CK}}` is a 2-column array.
    -   The first column is just the _Student IDs_ stacked on top of each other in order to facilitate the `VLOOKUP()` function by Student ID. They are stacked 5 times because the second column contains scores for five standards.
    -   The second column is formed from the various _Goal`x` RIT Score_ columns, likewise stacked so that they form one giant column instead of 5 smaller columns.
-   The array `{MAP!$BP:$BP;...MAP!$CJ:$CJ}` is a stack formed from the various _Goal`x` Name_ columns. It is set equal to the column header (`F$1`) so that the `FILTER()` function returns only the rows with student scores corresponding to the current standard.

The `VLOOKUP()` call has four arguments:

-   `$A:$A` is the column of _Student IDs_, which are our _key_ values.
-   The `FILTER()` function is described above, and outputs two columns: the _Student ID_ and _RIT Score_ for the standard in Cell `F1`.
-   `2` instructs `FILTER()` to return the second column, which is the _RIT Score_
-   `FALSE` specifies an exact match, per usual.

The entire operation is then wrapped in a `FILTER()` function to fill it down.

### Using `VLOOKUP()` to Filter For Items Missing From a Set

As part of the MAP Winter administration, any students who did not take the test must be scheduled for make-ups. Since we have a list of the students who tested (**MAP**) and a list of all students (**Students**), we can use a single `FILTER()` function to immediately pull our list of make-ups.

#### Example D: Cell `MakeUp!A2`

![Intermediate Filter MakeUp!A2](./images/IntermediateFilter_MakeUp_A2.png)

The formula in cell `A2` reads as follows:

```
=filter(Students!A:H, isnumber(Students!A:A), iserror(vlookup(Students!A:A, MAP!H:H, 1, false)))
```

The `FILTER()` call has three arguments:

-   `Students!A:H` are the columns from the **Students** sheet that we are pulling.
-   `isnumber(Students!A:A)` pulls only rows with a numeric Student ID (i.e excluding the headers and stopping when we run out of students).
-   `iserror(vlookup(Students!A:A, MAP!H:H, 1, false))` will be broken into pieces:
    -   `vlookup(Students!A:A, MAP!H:H, 1, false)` _attempts_ to look up each student from **Students** in the **MAP** sheet. Students who did not test will, of course, fail this lookup.
    -   `ISERROR()` returns `TRUE` if the `VLOOKUP()` failed, i.e. if the student has no test score.

In total, the function call filters the **Students** sheet to return only students who _fail_ a lookup in the **MAP** sheet; i.e. our make-up students.

## Official Documentation
### Array Literals
Official documentation on arrays from [Google](https://support.google.com/docs/answer/6208276?hl=en)

### `FILTER()`
Official documentation on `FILTER()` from [Google](https://support.google.com/docs/answer/3093197?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/filter-function-f4f7cb66-82eb-4767-8f7c-4877ad80c759)

### `ISNUMBER()`
Official documentation on `ISNUMBER()` from [Google](https://support.google.com/docs/answer/3093296?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/is-functions-0f2d7971-6019-40a0-a171-f2d869135665)

### `ISERROR()`
Official documentation on `ISERROR()` from [Google](https://support.google.com/docs/answer/3093349?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/is-functions-0f2d7971-6019-40a0-a171-f2d869135665)

### `VLOOKUP()`
Official documentation on `VLOOKUP()` from [Google](https://support.google.com/docs/answer/3093318) and from [Microsoft](https://support.microsoft.com/en-us/office/vlookup-function-0bbc8083-26fe-4963-8ab8-93a18ad188a1)

### `TRANSPOSE()`
Official documentation on `TRANSPOSE()` from [Google](https://support.google.com/docs/answer/3094262?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/transpose-function-ed039415-ed8a-4a81-93e9-4b6dfac76027)
