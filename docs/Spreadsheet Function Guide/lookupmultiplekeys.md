# Lookups on Multiple Keys
Often, one needs to perform a lookup on multiple keys: for example, returning the grade that corresponds to a given student _and_ course number.

In such cases, there are two ways to perform the lookup:

-   Creating a `Key` column _in the spreadsheet_ by concatenating the relevant columns
-   Creating a `Key` column _inside the formula_ by concatenating the relevant columns

!!! warning "Prerequisites"

    This guide assumes proficiency with the `VLOOKUP()`, `INDEX()`, and `MATCH()` functions.  If you need a refresher, see [Lookup Basics](./basiclookup.md).

## Video
A video of this tutorial is available on Google Drive.
[![Video Thumbnail](./images/LookupMultipleKeys_VideoThumbnail.png)](https://drive.google.com/file/d/11ifZ3ujIEqFxoI2bjqdj9H_ccCAMQEtm/view?usp=sharing)

## Concatenation
**Concatenation** is just a fancy term for "linking a bunch of stuff together in sequence".  Excel has two ways of concatenating text strings:

-   The **ampersand** (`&`) operator
-   The `CONCAT()` function

The `CONCAT()` function is useful when there is no need for a delimiter between fields.  For example, joining `Smith` and `John` to `SmithJohn` is unlikely to cause any issues even though there is no character separating the first and last names.

The `&` operator is useful when a delimiter must be specified.  For example, joining `10` and `9908` produces the same result as joining `109` and `908` (`109908`) which can be a problem.  In such cases, a delimiter should be inserted between the values to produce `10|9908` and `109|908`

!!! note "Delimiters"

    While the choice of delimiter is up to you, we recommend use of the pipe (`|`) character for two reasons:

    -   Unlike commas, spaces, or semicolons, the pipe will practically never show up in the text you're joining, reducing the likelihood of collisions
    -   Unlike most other characters, the pipe carries no significance on its own so it can't be mistaken by the spreadsheet software for some other type of operator

## Example

In [LookupMultipleKeys.xlsx](./assets/LookupMultipleKeys.xlsx):

-   The **Students** sheet contains a listing of all 9th grade students and the numbers of their English courses.
-   The **StoredGrades** sheet contains all of their Q1 stored grades _for all courses_.
-   The **PGFinalGrades** sheet contains all of their Live grades in English _for all terms_.

#### File Snapshot
| **Students** | **StoredGrades** | **PGFinalGrades** |
| --- | --- | --- |
| ![LookupMultipleKeys.xlsx Students](./images/LookupMultipleKeys_Students.png) | ![LookupMultipleKeys.xlsx StoredGrades](./images/LookupMultipleKeys_StoredGrades.png) | ![LookupMultipleKeys.xlsx PGFinalGrades](./images/LookupMultipleKeys_PGFinalGrades.png) |

**Our goal is to look up _Stored_ Q1 grades and _Live_ Q2 grades for each student in English.**

## Creating a `Key` column _in the spreadsheet_ using concatenation
### Introduction
Conceptually, the process is simple enough: on each sheet we create a `Key` column that is a combination of the two keys that we're interested in, resulting in a unique, _hybrid key_ for every student/course combination. Then, we perform the lookup using the _hybrid key_.

### Mechanics
We are going to use a `Key` column to perform the lookup on the **stored grade for Quarter 1**.

In the **Students** sheet, Column `G` has been designated the `Key` column. Notice how each value is just the concatenation of the `ID` and `English Course` columns.

Likewise, in the **StoredGrades** sheet, Column `A` has been designated the `Key` column, with each value being the concatenation of `StudentID` and `Course_Number`.

In this arrangement, each student-course combination can be uniquely identified.  

!!! note "StoredGrades sheet"

    Note that in the **StoredGrades** table only `Q1` grades are represented so we don't need to filter by term. This could, however, be accomplished easily enough by expanding the `Key` column to include the term.

### Formulas
#### Example A: Cell `Students!G2`

![Students Cell G2](./images/LookupMultipleKeys_Students_G2.png)

The formula in `Students!G2` reads as follows:

```
=B2&"|"&F2
```

Here, we use the `&` operator so that we can insert a pipe character between the student's `ID` and `English Course` fields. (Some, but not all, course numbers are strictly numeric so `CONCAT()` is not advisable in this case.)

---

#### Example B: Cell `StoredGrades!A2`

![StoredGrades Cell A2](./images/LookupMultipleKeys_StoredGrades_A2.png)

The formula in `StoredGrades!A2` reads as follows:

```
=B2&"|"&C2
```

The same two fields (`StudentID` and `Course_Number`) are concatenated in the same manner as on the **Students** sheet. Notice that more than just English classes are represented in the **StoredGrades** sheet; hence, the need for this `Key` column.

---

#### Example C: Cell `Students!H2`

![Students Cell H2](./images/LookupMultipleKeys_Students_H2.png)

The formula in `Students!H2` reads as follows:

```
=VLOOKUP(G2, StoredGrades!A:I, 9, FALSE)
```

This is a standard `VLOOKUP()` except that, instead of looking up the `ID` _or_ the `English Course` columns, we are using the `Key` column as a means of looking up both simultaneously.

## Creating a `Key` column _inside the formula_ using concatenation
### Introduction
Conceptually, the process is identical to the first set of examples: we combine two columns to create a _hybrid key_ in both the source and destination sheets, and then perform the lookup on that _hybrid key_.

The critical difference is that this method can be done entirely inside the lookup formula, eliminating the need for `Key` columns and, as a result, reducing clutter and increasing versatility.

### Mechanics
We are going to use an embedded `Key` column to perform the lookup on the **live grade for Quarter 2**.

!!! note "`INDEX()/MATCH()`"

    `VLOOKUP()` - at least in Excel - doesn't take kindly to column manipulations.  While this can be worked around by using the `CHOOSE()` function, it's simpler just to switch to `INDEX()/MATCH()`.

As before, we concatenate the Student's `ID` and `English Course`, separated by a pipe (`|`), to form the key.  The difference is that the concatenation occurs inside the lookup formula.

!!! note "PGFinalGrades Sheet"

    Unlike the **StoredGrades** sheet, the **PGFinalGrades** sheet contains grades for _all quarters_.  As a result, we will need to perform _two concatenations_: one for the course number and one for the term.

#### Example D: Cell `Students!I2`

![Students Cell I2](./images/LookupMultipleKeys_Students_I2.png)

The formula in `Students!I2` reads as follows:
```
=INDEX(PGFinalGrades!E:E,
  MATCH(B2&"|"&F2&"|"&"Q2", PGFinalGrades!A:A&"|"&PGFinalGrades!B:B&"|"&PGFinalGrades!D:D,
  0))
```

The first argument in the `INDEX()` function is our target column: `PGFinalGrades!E:E`, which contains the letter grades.

The second argument in the `INDEX()` function is our `MATCH()` clause, broken down as follows:

-   The first argument (`B2&"|"&F2&"|"&"Q2"`) concatenates our three _key_ values, delimited with pipes, to produce a _hybrid key_.

!!! note "Hardcoded term"

    For simplicity, the term (`Q2`) was hardcoded as text.  This need not be the case, and indeed it could be pulled from a column header, allowing one formula to pull for multiple terms.

-   The second argument (`PGFinalGrades!A:A&"|"&PGFinalGrades!B:B&"|"&PGFinalGrades!D:D`) concatenates our three _key_ columns in the destination sheet, in the same order and with the same delimiters as our source sheet, producing values that match our _hybrid key_.
-   The third argument (`0`) specifies that an exact match is required.

## Official Documentation
### `CONCAT()`
Official documentation on the `CONCAT()` function from [Google](https://support.google.com/docs/answer/3093592?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/concat-function-9b1a9a3f-94ff-41af-9736-694cbd6b4ca2)

### `&`
Official documentation on the ampersand (`&`) operator from [Google](https://support.google.com/docs/answer/3093592?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/concatenate-function-8f8ae884-2ca8-4f7a-b093-75d702bea31d)

### `INDEX()/MATCH()`
Official documentation on the `INDEX()/MATCH()` pair from [Microsoft](https://support.microsoft.com/en-us/office/look-up-values-with-vlookup-index-or-match-68297403-7c3c-4150-9e3c-4d348188976b)
