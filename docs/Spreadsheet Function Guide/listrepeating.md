# List of Repeating Numbers
e.g. _1, 1, 2, 2, ..., n, n_

Often, a list of consecutive numbers is needed, either to serve as identifiers for existing data, or to feed into formulas to generate new data. Further, it is often necessary for each number to repeat multiple times, e.g. when importing multiple records per student.

Accomplishing this with traditional formulas (e.g. `=A1+1`) is challenging, since there are to competing rules in play. Further, adding or removing a row can throw the entire arrangement into disarray.

Through clever use of `FILTER()`, `ROW()` and `FLOOR()`, however, such a list is a straightforward task.

!!! warning "Prerequisites"

    This is a follow-up to [Repeating Lists of Numbers](./repeatinglist.md), and familiarity with it is recommended.

    Familiarity with `FLOOR()`, while helpful, is not required since a primer on the function will be provided.

The examples that follow come from [ListRepeating](./ListRepeating.xlsx)

#### File Snapshot
| **Rounding** | **FieldTrip** | **Chaperones** |
| --- | --- | --- |
| ![List Repeating Rounding](./images/ListRepeating_Rounding.png) | ![List Repeating FieldTrip](./images/ListRepeating_FieldTrip.png) | ![List Repeating Chaperones](./images/ListRepeating_Chaperones.png) |

## `FLOOR()` function: a Primer

`FLOOR()` and `CEILING()` are companion functions to the more familiar `ROUND()` function.

In short:

-   `FLOOR()` always rounds **down**
-   `CEILING()` always rounds **up**

Google Sheets defaults to the nearest integer, while Excel requires an argument providing the significance (i.e. multiple of 1) to which the value will be rounded.

!!! note "Syntax"

    The `ROUND()` function takes a **number of digits** to be rounded: positive numbers imply digits to the _right_ of the decimal and negative numbers imply digits to the _left_ of the decimal.

    This differs from `FLOOR()` and `CEILING()`, which take a **significance**.

    Since we're not dealing with `ROUND()` in these examples, our consideration of this nuance ends here.

### Examples - found on the `Rounding` sheet

#### `1937.238` with various significances

| Significance | `ROUND()` | `FLOOR()` | `CEILING()` |
| --- | --- | --- | --- |
| `0.01` | `1937.24` |  `1937.23` | `1937.24` |
| `0.1` | `1937.2` | `1937.2` | `1937.3` |
| **`1`** | **`1937`** | **`1937`** | **`1938`** |
| `10` | `1940` | `1930` | `1940` |
| `100` | `1900` | `1900` | `2000` |

#### Fractions

Feeding successive fractions into `FLOOR()` has the effect of generating the same integer multiple times, which is the secret to generating our list of repeating numbers:

| _x_ | `FLOOR(x, 1)` |
| --- | --- |
| `0/3` | `0` |
| `1/3` | `0` |
| `2/3` | `0` |
| `3/3` | `1` |
| `4/3` | `1` |
| `5/3` | `1` |

## Numbering an Existing Data Set
In this example, we will parcel a data set into groups of 7, numbering them along the way.

### Introduction
The **FieldTrip** sheet contains a list of all 9th graders. To form chaperone groups, the 9th grade needs to be split into groups of 7, each of which will be assigned a teacher.

!!! note "Spooky Similarity"

    Yes, this **is** similar to the example from [Repeating Lists of Numbers](./repeatinglist.md)! In that example, we decided how _many_ groups to form, and sorted the kids into groups with no regard to the group size.

    This time, we're stating the _size_ of the group, and assigning them to as many groups as it takes.

### Mechanics
For this example, we will assign the first seven students to group `1`; the next seven to group `2`; and so forth until we run out of students.

#### Example A: Cell `FieldTrip!F2`

![FieldTrip Cell F2](./images/ListRepeating_FieldTrip_F2.png)

The formula in `FieldTrip!F2` reads as follows:

```
=FILTER(FLOOR((ROW(A:A)-2)/7, 1)+1, ISNUMBER(A:A))
```

-   The first argument to `FILTER()` is `FLOOR((ROW(A:A)-2)/7, 1)+1` which will be analyzed in parts:
    -   `ROW(A:A)-2` returns the row number of each cell in Column `A`. We subtract 2 so that the first student (on row `2`) has a value of `0`.
    -   `/7` divides the adjusted row number by our stated group size, producing a series of fractions {`0/7`, `1/7`, `2/7`...}
    -   `FLOOR()` rounds each fraction down to the nearest integer, giving us the sequence {`0, 0, 0..., 1, 1, 1...`}
    -   `+1` is added onto the series so that our groups start with `1`: {`1, 1, 1..., 2, 2, 2...`}
-   The second argument to `FILTER()` is `ISNUMBER(A:A)`. This has the effect of continuing the numbering until we run out of numeric Student IDs in Column `A`.


!!! note "`CEILING()`"

    Could we have used `CEILING()` instead of adding `1` and using `FLOOR()`? Absolutely! But using `FLOOR()` gave us results that more closely matched the _Repeating List_ example, so here we are.

## Retrieving Items From a Data Set
In this example, we will attach a chaperone to each student by cycling through the list of chaperones in the same manner as we numbered the students.

### Introduction
The **FieldTrip** sheet contains a list of all 9th graders. In **Example A** we assigned each student a group number. While it would be easy enough to create a numbered list of chaperones and `VLOOKUP()` each student's group number, let's be fancy and cycle through the chaperone list directly.

### Mechanics
For this example, we will cycle through the teachers on the **Chaperones** sheet, from `Abram, Michael` to `Wright, Scott`.  Mr. Abram will get the seven students in group `1`; Mr. Accatino will get the seven in group `2`, and so on. Not all staff members will be used.

#### Example B: Cell `FieldTrip!G2`

![Field Trip Cell G2](./images/ListRepeating_FieldTrip_G2.png)

The formula in `FieldTrip!F2` reads as follows:

```
=FILTER(INDEX(Chaperones!B:B, FLOOR((ROW(A:A)-2)/7, 1)+2), ISNUMBER(A:A))
```

!!! note "`INDEX()` Without `MATCH()`"

    This is one of the relatively few times that you will encounter `INDEX()` without `MATCH()`.

The `FILTER()` function is called identically to **Example A**.

The `INDEX()` function takes two arguments:

-   The first argument is `Chaperones!B:B`, which is the column containing our chaperone names.
-   The second argument is `FLOOR((ROW(A:A)-2)/7, 1)+2)`, which will be analyzed in parts:
    -   `ROW(A:A)-2` returns the row number of each cell in Column `A`. We subtract 2 so that the first student (on row `2`) has a row of `0`.
    -   `/7` divides the adjusted row number by our stated group size, producing a series of fractions {`0/7`, `1/7`, `2/7`...}
    -   `FLOOR()` rounds each fraction down to the nearest integer, giving us the sequence {`0, 0, 0..., 1, 1, 1...`}
    -   `+2` is added onto the series so that our groups start with `2`: {`2, 2, 2..., 3, 3, 3...`}  These correspond to the rows in **Chaperones** with teachers.

!!! note "Adding 2"

    In **Example A** we added `1` so that the first student was in _Group 1_.  In **Example B** we are adding `2` so that the first name we pick up is on Row `2`.  If we only added `1`, the first group would have a chaperone named `LastFirst`.

## Official Documentation
### `FILTER()`
Official documentation on `FILTER()` from [Google](https://support.google.com/docs/answer/3093197?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/filter-function-f4f7cb66-82eb-4767-8f7c-4877ad80c759)

### `INDEX()`
Official documentation on `INDEX()` from [Google](https://support.google.com/docs/answer/3098242?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/index-function-a5dcf0dd-996d-40a4-a822-b56b061328bd)

### `ROW()`
Official documentation on `ROW()` from [Google](https://support.google.com/docs/answer/3093316?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/row-function-3a63b74a-c4d0-4093-b49a-e76eb49a6d8d)

### `FLOOR()`
Official documentation on `FLOOR()` from [Google](https://support.google.com/docs/answer/3093487?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/floor-function-14bb497c-24f2-4e04-b327-b0b4de5a8886)

### `ISNUMBER()`
Official documentation on `ISNUMBER()` from [Google](https://support.google.com/docs/answer/3093296?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/is-functions-0f2d7971-6019-40a0-a171-f2d869135665)
