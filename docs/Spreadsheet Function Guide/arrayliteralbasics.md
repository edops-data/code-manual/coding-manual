# Basic Use of Array Literals
This is a primer on the use of array literals to combine columns or arrays into larger, more useful arrays. It is a prerequisite for a number of other topics, notably [lookups when the key is to the right of the data](./leftlookup.md).

## Video
A video of this tutorial is available on Google Drive.
[![Video Thumbnail](./images/ArrayLiterals_VideoThumbnail.png)](https://drive.google.com/file/d/1sFIusJDUX_euzy9WOLEgPYkpfrXWKBFu/view?usp=sharing)

## Introduction

Array literals allow you to construct a virtual "table" (_array_) by combining smaller arrays.  Use cases include:

-   "gluing" smaller arrays together to form a larger array,
-   rearranging the rows/columns of an existing array into a more convenient format, or
-   "cutting" rows/columns out of an existing array to make it easier to manipulate

The term _literal_ stems from the fact that the array is not created by a formula, but is rather constructed, piece by piece, by the user.

!!! note "Terminology"

    A collection of cells is henceforth known as an **array**.  Conceptually, think of it interchangeably with the term **table**.  However, _table_ has a specific meaning in spreadsheets, and will thus be avoided for this purpose.

## Syntax
The examples come from [ArrayLiterals](https://docs.google.com/spreadsheets/d/1Kg2UMwGhcUgVmpdf3-e2mkPnZp06aPUpzCaXWklAuUw) on Google Drive.

#### File Snapshot
| Students | Students_Uniform | Roster |
| --- | --- | --- |
| ![ArrayLiterals Students Tab](./images/ArrayLiterals_Students.png) | ![ArrayLiterals Students_Uniform Tab](./images/ArrayLiterals_Students_Uniform.png) | ![ArrayLiterals Roster Tab](./images/ArrayLiterals_Roster.png) |

### Commas Combine Columns
To attach columns (or arrays) to each other **horizontally**, separate them with commas.

In the **Students** sheet of the example file, grades 9, 10, and 12 have the `Student_Number` in the _leftmost_ column, while grade 11 has it in the _rightmost_ column.  Additionally, in grade 10 the `LastFirst` and `Grade_Level` columns are swapped.

In the **Students_Uniform** sheet, we want each grade's columns to appear identically.  This could conceivably be done with diligent copy-pasting, but if any data point on **Students** changed, the process would need to be repeated.  Alternatively, hundreds of cell formulas could be used to perform the processing, but again, if any rows were added to **Students** they might not appear on **Students_Uniform** unless the formulas were filled down with room to spare.

Using an array literal solves both problems, allowing us to perform the transformations with **four formulas**.

#### Example A: `'Students_Uniform'!A3`

![Array Literals Students_Uniform!A3](./images/ArrayLiterals_A3.png)

The formula in `'Students_Uniform'!A3` reads as follows:

```
={Students!A3:C}
```

!!! note "Curly Braces Define an Array"

    An array is always surrounded by curly braces (`{}`), which denote that the resultant array is to be treated as a single object, indistinguishable from a regular range of cells.

    Arrays are recursive and can be composed of smaller arrays, each in turn defined by a pair of curly braces.

In this case, the columns were in the correct order so they were simply transferred to the target sheet.

**Note** that the starting point can be a specific cell, while the endpoint can be just a column. Doing this automatically expands the array to the end of the sheet.

---

#### Example B: `'Students_Uniform'!E3`

![Array Literals Students_Uniform!E3](./images/ArrayLiterals_E3.png)

The formula in `'Students_Uniform'!E3` reads as follows:

```
={Students!E3:E, Students!G3:G, Students!F3:F}
```

The columns are referenced in the order that we want them, separated by commas to indicate that they are to be joined **horizontally**.  The result is that the data is displayed in the order that we want.

---

#### Example C: `'Students_Uniform'!I3`


![Array Literals Students_Uniform!I3](./images/ArrayLiterals_I3.png)

The formula in `'Students_Uniform'!I3` reads as follows:

```
={Students!K3:K, Students!I3:J}
```

As before, the columns are referenced in the order that we want them, though since the last two columns are already in the correct order they can be referenced as a block.

### Semicolons Combine Rows
To attach rows (or arrays) to each other **vertically**, separate them with semicolons.

Suppose that, having cleaned up the data on **Students_Uniform**, we want to combine all four grades into a single mega-roster by "stacking" the four data sets on top of one another.  As before, this could be accomplished using copy-paste or a large number of cell formulas, but in both cases a lot of work would be wasted if even a tiny change or addition was made to the source data.

Instead, we can accomplish our goal with a **single formula**.

#### Example D: `Roster!A3`

![Array Literals Roster!A3](./images/ArrayLiterals_RosterA3.png)

The formula in `Roster!A3` reads as follows:

```
={Students_Uniform!A3:C; Students_Uniform!E3:G; Students_Uniform!I3:K; Students_Uniform!M3:O}
```

The groups of columns are now stacked, one on top of another, using semicolons.

!!! warning "Dimensions"

    For this stacking to work, each array must be the same width - in this case, 3 columns.

**Note**: If you scroll down, you will notice significant amounts of whitespace. This is because each grade doesn't have the same number of students. This can be addressed easily enough, but doing so is outside the scope of this lesson.

### Arrays Inside Arrays
Rather than using the **Students_Uniform** sheet to get everything in the right order before forming our roster, we can perform the entire operation - column rearrangement and stacking - with a **single formula**.  Here's how.

#### Example E: `Roster!E3`

![Array Literals Roster!E3](./images/ArrayLiterals_RosterE3.png)

The formula in `Roster!E3` reads as follows:

```
={{Students!A3:C};
  {Students!E3:E, Students!G3:G, Students!F3:F};
  {Students!K3:K, Students!I3:J};
  {Students!M3:O}}
```

Here, you can see each of the original arrays, with their columns joined _horizontally_ by _commas_, and surrounded by curly braces. Those smaller arrays are, in turn, joined _vertically_ by _semicolons_ in a larger array that is also enclosed with curly braces.

### Errors

#### Mismatched Array Sizes

If the component arrays are not the same sizes (same number of rows if combining horizontally or same number of columns if combining vertically) then a `#VALUE` error will result.  (In this example, the last array contains only one column while the others each contain 3.)

![Array Literals #VALUE Error](./images/ArrayLiterals_ValueError.png)

#### Collisions with Existing Content

If the array, when fully expanded, collides with existing non-empty cells, then a `#REF` error will result. (In this example, rows 4 and below were pasted while row 3 contains the array literal.)

![Array Literals #REF Error](./images/ArrayLiterals_RefError.png)

## Official Documentation
### Array Literals
Official documentation on arrays from [Google](https://support.google.com/docs/answer/6208276?hl=en)
