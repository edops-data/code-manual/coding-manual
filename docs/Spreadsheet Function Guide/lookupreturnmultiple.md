# Lookups that Return Multiple Values
Lookups seldom happen in isolation, and quite often one needs to look up multiple values off of the same key. For example, given a `Student ID` one might need `LastFirst` and `Grade_Level`, or the `Grade` and `Percent` for a given course grade.

In such cases, an **array literal** can be used to reference multiple destination columns at once.

!!! warning "Prerequisites"

    This guide assumes proficiency with the `VLOOKUP()`, `INDEX()`, and `MATCH()` functions. If you need a refresher, see [Lookup Basics](./basiclookup.md).


The examples that follow come from [LookupReturnMultiple.xlsx](./assets/LookupReturnMultiple.xlsx)

#### File Snapshot
| **StoredGrades** | **Students** |
| --- | --- |
| ![LookupReturnMultiple.xlsx StoredGrades](./images/LookupReturnMultiple_StoredGrades.png) | ![LookupReturnMultiple.xlsx Students](./images/LookupReturnMultiple_Students.png) |

## Returning Multiple Columns With `VLOOKUP()`
### Introduction
This process is a straightforward extension of `VLOOKUP()`'s basic functionality. Instead of specifying **one** _value_ column, we will pass an **array** of _value_ columns. The result will be an array of results instead of a single value.

### Mechanics
For this example, we will look up the student's _Last Name_ and _First Name_ using a single `VLOOKUP()` call.

#### Example A: Cell `StoredGrades!M2`

![Stored Grades M2](./images/LookupReturnMultiple_StoredGrades_M2.png)

The formula in `StoredGrades!M2` reads as follows:

```
=VLOOKUP(A2, Students!B:D, {2,3}, FALSE)
```

The third argument - which is normally a lone integer - has been replaced with the array literal `{2,3}` indicating that values from _both_ the second and third columns are to be returned.  Sure enough, columns `M` and `N` both populate from the same formula.

## Returning Multiple Columns With `INDEX()/MATCH()`
### Introduction
This process is a straightforward extension of the basic `INDEX()/MATCH()` functionality. Normally, when using `INDEX()/MATCH()` as a replacement for `VLOOKUP()`, the `MATCH()` clause returns the appropriate _row_ and the _column_ is simply the only column passed to `INDEX()`.

However, `INDEX()` is far more versatile than this and is capable of traversing an array horizontally _and_ vertically.  To that end, we will provide `INDEX()` with an _array_ instead of just a column, and specify the target _columns_ with an additional argument.

### Mechanics
For this example, we will look up both the student's `Grade_Level` and `Student_Number` values from the **Students** sheet using a single command.

#### Example B: Cell `StoredGrades!O2`

![Stored Grades O2](./images/LookupReturnMultiple_StoredGrades_O2.png)

The formula in `StoredGrades!M2` reads as follows:

```
=INDEX(Students!A:E, MATCH(A2, Students!B:B, 0), {5,1})
```

The **first argument** - which is normally a single column - has been replaced with an array (`Students!A:E`), enabling us to look up multiple values at once.

The **third argument** - which is normally _absent_ - is now an array literal, instructing the `INDEX()` function to return data from Columns `5` and `1` from the array; i.e. the `Grade_Level` and `Student_Number` columns.

!!! note "Out-of-Order Columns"

    It is indeed possible to pull the columns out of order! That's why the array reads `{5,1}`: it lets us pull the `Grade_Level` column _before_ the `Student_Number` column, even though `Student_Number` is to the _left_ of `Grade_Level` in the original table.

### Errors
#### Collisions with Existing Content
If the array, when fully expanded, collides with existing non-empty cells, then a `#SPILL` error will result. (`#REF` in GSheets).

![Lookup #SPILL error](./images/LookupReturnMultiple_SpillError.png)

In this example, the `Gender` column interfered with where the formula was attempting to put the `Student_Number`; hence the `#SPILL` error.

## Official Documentation
### `INDEX()/MATCH()`
Official documentation on the `INDEX()/MATCH()` pair from [Microsoft](https://support.microsoft.com/en-us/office/look-up-values-with-vlookup-index-or-match-68297403-7c3c-4150-9e3c-4d348188976b)
