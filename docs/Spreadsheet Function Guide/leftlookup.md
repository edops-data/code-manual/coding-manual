# Lookups When the Key is to the Right of the Target Data
Sometimes when performing a lookup, the _key_ column is not to the left of the _value_ column and moving the _key_ column is either inconvenient or impossible.

In such cases, there are three ways to perform the lookup:

-   Modify the `VLOOKUP()` function with an **array literal** (GSheets Only)
-   Modify the `VLOOKUP()` function with the `CHOOSE()` function
-   Ditch `VLOOKUP()` entirely in favor of `INDEX()/MATCH()`

!!! warning "Prerequisites"

    This guide assumes proficiency with the `VLOOKUP()` function.  If you need a refresher, see [Lookup Basics](./basiclookup.md).

    Additionally, this guide assumes proficiency with **array literals**.  If you need a refresher, see [Array Literal Basics](./arrayliteralbasics.md)

## Video
A video of this tutorial is available on Google Drive.
[![Video Thumbnail](./images/LeftLookup_VideoThumbnail.png)](https://drive.google.com/file/d/10-mvOHtzV3u7KbigXDpW79NCW22mzKhi/view?usp=sharing)

## `VLOOKUP()` With an Array Literal
### Introduction
Conceptually, this process is simple enough: use an **array literal** to rearrange the target columns such that the _key_ column is to the left of the _value_ column.

!!! note "Excel vs. GSheets"

    The steps that follow apply only to **GSheets**, as Excel lacks the ability to embed array literals inside of `VLOOKUP()` functions.

The examples that follow come from the [LeftLookup](https://docs.google.com/spreadsheets/d/1IFPk6k0XoeRmZ5nskEA4qYQVDZnzyfME19qPByJu1cQ) file on Google Drive.

### File Snapshot
| **PGFinalGrades** | **Sections** |
| --- | --- |
| ![LeftLookup PGFinalGrades](./images/LeftLookup_PGFinalGrades.png) | ![LeftLookup Sections](./images/LeftLookup_Sections.png) |

### Mechanics
For each grade in the **PGFinalGrades** table, we want to look up the corresponding `Course_Number` entry from the **Sections** table. Normally this would be a straightfoward `VLOOKUP()`, except that the primary key (`Sections.ID`) is located to the right of the desired value (`Sections.Course_Number`).

Assume for the sake of this example that rearranging the columns in the **Sections** table is not possible.

To solve this, we will construct an **array literal** that has `Sections.ID` as the leftmost column and `Sections.Course_Number` as the second column.  We will then feed this array literal into a `VLOOKUP()` call.

### Formulas

#### Example A: Cell `PGFinalGrades!H2`

![PGFinalGrades!H2](./images/LeftLookup_PGFinalGrades_H2.png)

The formula in `PGFinalGrades!H2` reads as follows:

```
=vlookup(B2, {Sections!C:C, Sections!A:A}, 2, false)
```

The second argument, which is normally a spreadsheet range, has been replaced with an array literal that places the columns out of order.  Now, the "leftmost" column is the _key_ and the "rightmost" column is the _value_.  Consequently, when the third argument calls for column number `2`, it returns `Sections!A:A` which is our sought-after value.

## `VLOOKUP()` With the `CHOOSE()` function
### Introduction
This is an Excel-Compatible equivalent of the array literal method.  We use the `CHOOSE()` function to create a mock array with the columns in our preferred order.

The examples that follow come from the [LeftLookup.xlsx](./assets/LeftLookup.xlsx)

!!! warning "Resource-Intensive"

    This method is computationally intense and will vastly extend the calculation time on your spreadsheet. As such, this may not be the best method in cases where large volumes of data are being processed.

### File Snapshot
| **PGFinalGrades** | **Sections** |
| --- | --- |
| ![LeftLookup PGFinalGrades](./images/LeftLookupXLSX_PGFinalGrades.png) | ![LeftLookup Sections](./images/LeftLookupXLSX_Sections.png) |

### Mechanics
For each grade in the **PGFinalGrades** table, we want to look up the corresponding `Course_Number` entry from the **Sections** table. Normally this would be a straightfoward `VLOOKUP()`, except that the primary key (`Sections.ID`) is located to the right of the desired value (`Sections.Course_Number`).

Assume for the sake of this example that rearranging the columns in the **Sections** table is not possible.

To solve this, we will use the `CHOOSE()` function to associate the necessary indices (`1` for the _key_ and `2` for the _value_) to their respective columns.

### Formulas

#### Example B: Cell `PGFinalGrades!H2`

![PGFinalGrades!H2](./images/LeftLookupXLSX_PGFinalGrades_H2.png)

The formula in `PGFinalGrades!H2` reads as follows:

```
=VLOOKUP(B2, CHOOSE({1,2}, Sections!C:C, Sections!A:A), 2, FALSE)
```

The second argument, which is normally a spreadsheet range, has been replaced with the `CHOOSE()` function which assigns the indices `1` and `2` to the columns `Sections!C:C` and `Sections!A:A` respectively

The third argument to the `VLOOKUP()` function remains `2`, since there are two columns in our array, the second of which is our _value_ column.

## `INDEX()/MATCH()`
### Introduction
Conceptually, we are simply going to break the `VLOOKUP()` function into two steps:

-   Search for the _key_ in the _key_ column
-   Return a corresponding value from the _value_ column

The examples the follow come from [LeftLookup.xlsx](./assets/LeftLookup.xlsx).

### File Snapshot
| **PGFinalGrades** | **Sections** |
| --- | --- |
| ![LeftLookup PGFinalGrades](./images/LeftLookupXLSX_PGFinalGrades.png) | ![LeftLookup Sections](./images/LeftLookupXLSX_Sections.png) |

### Mechanics
For each grade in the **PGFinalGrades** table, we want to look up the corresponding `Expression` entry from the **Sections** table. Normally this would be a straightfoward `VLOOKUP()`, except that the primary key (`Sections.ID`) is located to the right of the desired value (`Sections.Expression`).

Assume for the sake of this example that rearranging the columns in the **Sections** table is not possible.

To solve this, we will use `MATCH()` to find the _key_ (`SectionID`) in the _key_ column (`Sections.ID`), and then use `INDEX()` to return the corresponding _value_ from the _value_ column (`Sections.Expression`).

### Formulas

#### Example C: Cell `PGFinalGrades!I2`

![PGFinalGrades!H2](./images/LeftLookupXLSX_PGFinalGrades_I2.png)

The formula in `PGFinalGrades!I2` reads as follows:

```
=INDEX(Sections!B:B, MATCH(PGFinalGrades!B2, Sections!C:C, 0))
```

The `MATCH()` clause of the formula searches for our _key_ (`PGFinalGrades!B2`) in the _key_ column (`Sections!C:C`) and returns the **row number** of the result. (In this case, Section `277` is on row `74`).

Then, the `INDEX()` clause of the formula traverses the _value_ column (`Sections!B:B`) until it reaches the row provided by the `MATCH()` clause (in this case, row `74`) and returns the appropriate value (`1(A-B)`).

## Official Documentation
### `CHOOSE()`
Official documentation on `CHOOSE()` from [Microsoft](https://support.microsoft.com/en-us/office/choose-function-fc5c184f-cb62-4ec7-a46e-38653b98f5bc)

### `INDEX()/MATCH()`
Official documentation on the `INDEX()/MATCH()` pair from [Microsoft](https://support.microsoft.com/en-us/office/look-up-values-with-vlookup-index-or-match-68297403-7c3c-4150-9e3c-4d348188976b)
