# Basic Use of `VLOOKUP()` and `HLOOKUP()`
This is a primer for specialists who are learning the ins and outs of the fundamental lookup functions.

## Introduction

`VLOOKUP()` and `HLOOKUP()` are used to, well, look up data from an array. They start by finding a value in one row or column (the _key_) and then return a corresponding value from a different row or column.

!!! note "Scope"

    For simplicity, this document will focus primarily on `VLOOKUP()` with notes on `HLOOKUP()` generally added as an afterthought. This should not pose a problem since `VLOOKUP()` is far and away the more commonly-used function.

Functionality is best shown by example.

## Example

In [VLOOKUP.xlsx](./assets/VLOOKUP.xlsx), the **Students** sheet contains a listing of all 9th grade students at _Apple Grove High School_.  The **CC** sheet, meanwhile, contains the listing of all the aforementioned students' 1st period classes.

#### File Snapshot
| **Students** | **CC** |
| --- | --- |
| ![VLOOKUP Example, Students Tab](./images/VLOOKUP_Students.png) | ![VLOOKUP Example, CC Tab](./images/VLOOKUP_CC.png) |

**Our goal is to attach each student to their 1st period class**.

### Key
The _Key_ in this instance is the `Student_Number` column, since it appears in both the **Students** and **CC** tables (columns `A` and `B` respectively).

### Mechanics
For each student, we are going to use the `VLOOKUP()` function to:

-   Find that student's `Student_Number` in the **CC** table
-   Traverse the same _row_ until we reach the `Course_Name` column (Column `E`) and return the corresponding _Course Name_
-   Repeat the process, this time traversing the row until we reach the `LastFirst` column (Column `M`) and return the corresponding _Teacher Name_.

### Formulas
#### Example A: Cell `D2`

![Cell D2](./images/VLOOKUP_D2.png)

The formula in cell `D2` reads as follows:

```
=VLOOKUP(A2, CC!B:M, 4, FALSE)
```

The four arguments are:

-   **`A2`** - this is the cell containing the _key_, in this case the `Student_Number`.
-   **`CC!B:M`** - this is the array containing the _key_ column (`Student_Number`), the _value_ column (`Course_Name`), and all the columns in between.

!!! note "Key Column"

    With `VLOOKUP()`, the _key_ column must be the **leftmost column** in the array.

    Similarly, with `HLOOKUP()`, the _key_ row must be the **top row** in the array.

-   **`4`** - this is the number of columns we are to traverse in the array to get from our _key_ column (`B`) to our _value_ column (`E`). Note that we have not used the entire array, which stretches to Column `M`.  That's fine.

!!! note "`HLOOKUP()`"

    While `VLOOKUP()` traverses **columns** to the **right** of the _key_, `HLOOKUP()` traverses **rows** **below** the _key_.

-   **`FALSE`** - this boolean specifies that we are looking for an _exact match_.  See the note (_Exact vs. Approximate Matches_) below for other possible values.

---

#### Example B: Cell `E2`

![Cell E2](./images/VLOOKUP_E2.png)

The formula in cell `E2` reads as follows:

```
=VLOOKUP(A2, CC!B:M, 12, FALSE)
```

The four arguments are:

-   **`A2`** - as before, this contains the _key_ (`Student_Number`).
-   **`CC!B:M`** - as before, this is the array containing the _key_ column (`Student_Number`), the _value_ column (`LastFirst`), and all columns in between.

!!! note "Column Names"

    While the **values** in the _key_ columns must match, their **headers** do not. Notice that on the **CC** sheet, `Student_Number` has a `[1]` in front of it and the formula still works without issue.

-   **`12`** - this is the number of columns we are to traverse in the array to get from our _key_ column (`B`) to our _value_ column (`M`).
-   **`FALSE`** - as before, we want an _exact match_. See the note (_Exact vs. Approximate Matches_) below for other possible values.

!!! note "Exact vs. Approximate Matches"

    In almost every case, the fourth parameter of `VLOOKUP()` will be set to `FALSE`, indicating that only an exact match of the _key_ is acceptable.

    At times, however, it may be useful to return the _nearest_ match to the _key_.  In this case, set the fourth parameter to `TRUE` and the function will find the **first** entry in the _key_ column that is **less than or equal to** the target value.

## Output

### Success
If the `VLOOKUP()` function successfully identifies the _key_ value in the _key_ column, it will return the sought-after value.

### Failure
If the `VLOOKUP()` function does **not** successfully identify the _key_ value in the _key_ column, then it will return a `#N/A` error.  This is what happened in cell `D3`, among others

In this instance, such an error is an indication that the student was not properly scheduled and has no 1st period class.

![VLOOKUP Failure](./images/VLOOKUP_failure.png)

## Official Documentation
### `VLOOKUP()`
Official documentation on `VLOOKUP()` from [Google](https://support.google.com/docs/answer/3093318) and from [Microsoft](https://support.microsoft.com/en-us/office/vlookup-function-0bbc8083-26fe-4963-8ab8-93a18ad188a1)

### `HLOOKUP()`
Official documentation on `HLOOKUP()` from [Google](https://support.google.com/docs/answer/3093375) and from [Microsoft](https://support.microsoft.com/en-us/office/hlookup-function-a3034eec-b719-4ba3-bb65-e1ad662ed95f)
