# Using `MOD()` to filter courses by term length

The `MOD()` function is a simple function with a seemingly obscure purpose, but it can be surprisingly useful when parsing data. What follows is one example of its utility when scouring grade data.

## Introduction

PowerSchool identifies marking periods (terms) using a unique nomenclature:

-  The 2-digit `YearID` is the number of years since 1991. So, for example, the 2018-19 school year is `28`.
-  The yearlong term is the `YearID` multiplied by `100`.  For example, yearlong classes during SY18-29 would have `TermID=2800`.
-  Semesters, quarters, and smaller terms are numbered sequentially in the order that they were configured in the system.

Assuming a normal setup for SY18-19:

| Term | `TermID` |
| --- | --- |
| 18-19 Year | `2800` |
| Semester 1 | `2801` |
| Semester 2 | `2802` |

All of this is to say that:

**Yearlong terms are evenly divisible by 10, while shorter terms are not**.

The examples that follow come from [MOD.xlsx](./assets/MOD.xlsx).

#### File Snapshot

| **Sections** |
| --- |
| ![MOD Sections](./images/MOD_Sections.png) |

## Example
Most schools offer full credit for yearlong courses and half credit for semester courses. However, it's not unusual for a well-intentioned registrar to accidentally schedule a full-credit course for a semester or a half-credit course for a full year. Identifying such courses is essential to make sure that students receive appropriate credit.

While it is certainly possible to manually filter, sort, and compare records to make sure that yearlong courses have 1 credit and semester-long courses have 0.5 credits, doing so is laborious and may result in missed entries.

A `MOD()` function and a little cleverness make the task trivial.

### The `SIGN()` Function: a Primer

In the example that follows we will make use of the `SIGN()` function, which returns an integer corresponding to the sign of a number.

| _x_ | `SIGN(x)` |
| --- | --- |
| -9 | -1 |
| -0.22 | -1 |
| 0 | 0 |
| 1.74 | 1 |
| 3 | 1 |

In particular, we will utilize the fact that `SIGN()` reduces all positive numbers to `1`.

### Comparing Term Length and Credit Amount

The **Sections** sheet contains all of the sections for the 2019-20 school year in _Apple Grove Unified School District_. We will compare the Term ID in Column `F` to the Credit Hours in Column `G`.

#### Example A: Cell `Sections!H2`

![Sections A2](./images/MOD_Sections_H2.png)

The formula in cell `H2` reads as follows:

```
=G2=IF(MOD(F2, 10)=0, 1, 0.5)*SIGN(G2)
```

The formula appears complicated and uses some unorthodox syntax, so we will analyze it _conceptually_ before analyzing it _formulaically_.

#### Conceptual Breakdown

-   If the term is a _yearlong_ term (i.e. divisible by 10), then we should expect `Credit_Hours` to equal `1`.  Otherwise, we should expect `Credit_Hours` to equal `0.5`.
-   There are, however, zero-credit courses. If `Credit_Hours` equals `0`, then we should disregard the logic above and give the course a pass.
-   Once we know what value to _expect_ from the `Credit_Hours` column, we compare it to the actual value, returning a `TRUE` or `FALSE` value.

#### Formulaic Breakdown

We work, to the best of our ability, from the inside out.

-   `MOD(F2, 10)=0` checks whether the `TermID` has a remainder of `0` when divided by `10`; i.e. it checks whether `TermID` is a multiple of `10`.
-   `IF(MOD(F2, 10)=0, 1, 0.5)` returns our _expected_ credit hours: `1` if the course is yearlong (i.e. if `MOD()` returns a `0`), and `0.5` otherwise.
-   `*SIGN(G2)` multiplies the output of the `IF()` statement by either `1` or `0`:
    -   If `Credit_Hours` is _nonzero_, `SIGN(G2)` equals `1` and the formula is essentially unchanged.
    -   If `Credit_Hours` is _zero_, `SIGN(G2)` equals `0` and the `IF()` statement is zeroed out.  **This is our means of disregarding courses with no credit.**
-   `=G2=` looks like a typo, with an extra equal sign. What we're doing, though, is asking Excel _whether_ the two quantities are equal; i.e. whether the _observed_ value of `Credit_Hours` matches the _expected_ value. This syntax will return a `TRUE` or a `FALSE` accordingly.

## Official Documentation
### `MOD()`
Official documentation on `MOD()` from [Google](https://support.google.com/docs/answer/3093497?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/mod-function-9b6cd169-b6ee-406a-a97b-edf2a9dc24f3)

### `SIGN()`
Official documentation on `SIGN()` from [Google](https://support.google.com/docs/answer/3093513?hl=en) and from [Microsoft](https://support.microsoft.com/en-us/office/sign-function-109c932d-fcdc-4023-91f1-2dd0e916a1d8)
