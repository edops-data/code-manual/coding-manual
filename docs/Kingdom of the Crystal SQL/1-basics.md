# Basics of Relational Databases and SQL Queries (`SELECT`s and `JOIN`s)
In this module, we will define what a **Relational Database** is, and examine the basic anatomy of a **SQL query**.

By the end of the module, you should be able to write a query that pulls all of the contacts associated with a given student in our training database.

## Video
A video of this module is available on [Google Drive](https://drive.google.com/file/d/1jZI3dn0LNxIU_YbNhn83D58rRw8r0Qjv/view?usp=share_link):
![Module 1 Video Thumbnail](./images/Module1_VideoThumbnail.png)

## Assets
For this module, you will need access to:

-   The Training Database (`TrainingDB.sqlite`)
-   Some additional assets on [Google Drive](https://drive.google.com/drive/folders/1kK5LctdUC5PZW-5gaiuqTyfFXELv0NHP)

## Introduction
A **relational database** is a collection of data with some basic structure imposed. Specifically, you will become familiar with the concepts of:

-   Schemas
-   Tables
-   Rows
-   Columns
-   Data Types
-   Primary/Foreign Keys

**Structured Query Language (SQL)** is the language that we use to interact with relational databases. While SQL contains many commands, only two are necessary to get started:

-   `SELECT`
-   `JOIN`

We will use this information to form some basic queries that return student contacts.

## Relational Databases
Let's start by getting an understanding of what a _database_ is in the first place, and what makes them _relational_.

### A Lame Database
A database is just a collection of data. In fact, we can create one pretty easily in our favorite spreadsheet editor. Enter [**LameDB**](https://docs.google.com/spreadsheets/d/1HWYaNKztV82hjyWviqUoSKN70cH-h_LZT34DZPByZFQ).

#### Tables
A **table** is a single lump of data, provided it follows a few structural requirements. In our _LameDB_, each _sheet_ (or tab) is a table.

In particular a **table** has:

-   One record per **row**.  In a database, _row_ and _record_ are interchangeable terms.
-   One **column** per field.  Think of a field as a data element, such as `Last_Name` or `DOB`.
-   One **data type** per column. Note that every `Last_Name` entry is text, and every `DOB` entry is a date. We'll get into data types in another module, but what matters here is that all of the data in any given column is of one type. No mixing numbers and text!

#### Schemas
Together, the configuration of **tables**, **columns**, and **data types** are called a **schema**. The schema for the _LameDB_ is below. Notice how each **column** has its own **data type**, which every **row** of data must follow.

=== "Students"

    | Column Name | Data Type |
    | --- | --- |
    | `Last_Name` | TEXT |
    | `First_Name` | TEXT |
    | `Middle_Name` | TEXT |
    | `DOB` | DATE |
    | `Gender` | TEXT |
    | `FedEthnicity` | BOOLEAN |
    | `Street` | TEXT |
    | `City` | TEXT |
    | `State` | TEXT |
    | `Zip` | TEXT |

=== "StoredGrades"

    | Column Name | Data Type |
    | --- | --- |
    | `DCID` | NUMERIC |
    | `Comment` | TEXT |
    | `Course_Equiv` | TEXT |
    | `Course_Name` | TEXT |
    | `Course_Number` | TEXT |
    | `Credit_Type` | TEXT |
    | `DateStored` | DATE |
    | `EarnedCrHrs` | NUMERIC |
    | `ExcludeFromClassRank` | BOOLEAN |
    | `ExcludeFromGPA` | BOOLEAN |
    | `ExcludeFromGraduation` | BOOLEAN |
    | `ExcludeFromGradeSuppression` | BOOLEAN |
    | `ExcludeFromHonorRoll` | BOOLEAN |
    | `ExcludeFromTranscripts` | BOOLEAN |
    | `GPA_AddedValue`| NUMERIC |
    | `Grade` | TEXT |
    | `Grade_Level` | NUMERIC |
    | `Percent` | NUMERIC |
    | `PotentialCrHrs` | NUMERIC |
    | `Replaced_DCID` | NUMERIC |
    | `SchoolID` | NUMERIC |
    | `SchoolName` | TEXT |
    | `StoreCode` | TEXT |
    | `Teacher_Name` | TEXT |
    | `TermBinsName` | TEXT |
    | `TermID` | NUMERIC |

#### So Why Is It Lame?
Why are we being rude and calling this the _LameDB_? Because, well, there's not much you can do with it.

Suppose we wanted to retrieve all of the `Q1` grades for `Jane Bradshaw`. Jane's name appears in the `Students` table, but not in the `StoredGrades` table, so it's a challenge to figure out which grades belong to her.

What we need is a way to **relate** (get it?) the data from one table to the data in another. Let's look at how.

### Making It Relational
Let's make a couple upgrades to our _LameDB_, resulting in the file known as [**RelationalDB**](https://docs.google.com/spreadsheets/d/1vh1tDb9hBMHzgm6qey7jDiMSShnul01ke_UpNYSVdS0).

#### Primary Keys
Notice how the _Students_ tab now has an extra column: `StudentID`. `StudentID` has several important properties:

-   It's **unique**. No two students can ever share a `StudentID`.
-   It's **sequential**, incrementing by `1` every time a student is added. Sequential isn't strictly required, but it's basically how you guarantee uniqueness.
-   It's **numeric**, so things like leading zeroes don't interfere. `08 = 008 = 8`.

Together, these criteria make `StudentID` into what is known as a **primary key**. A **primary key** is a unique, numeric identifier for each record in a table. Every table should have one and only one primary key.

#### Foreign Keys
Now let's look at the _StoredGrades_ table. Notice how `StudentID` makes an appearance here as well.

In this case, it's not a **primary** key, principally because it's not **unique**: a given student (represented by their `StudentID`) can have multiple grades, of course.

It's still important, though, since it tells us which unique record in the `Students` table belongs to that grade. Therefore, it's still a key. In particular, we call it a **foreign key** since it keys us to a different table.

A table can have multiple foreign keys, depending on how many different data elements it relates. `StoredGrades`, for example, contains two foreign keys:

-   `StudentID`, which tells us which student owns a given grade, and
-   `SectionID`, which tells us which class section the grade was from.

!!! note "Primary Key"

      Notice that `StoredGrades` has its own primary key, simply called `ID`.

### Review of Database Terminology
That was a lot of new words! Let's review.

-   A **database** is one or more collections of data, organized into tables.
-   A **table** is a collection of data with the following structure:
    -   Every record exists in exactly one **row**
    -   Every data element exists in exactly one **column**
    -   Every column has its own **data type**
-   The information that defines the table/column structure is called the **schema**.
-   A **relational database** is all of the above, _plus_:
    -   Every table has a **primary key** that uniquely identifies each row, and
    -   which can be used by other tables as a **foreign key** to attach records to one another.

## Basic SQL Queries
Now that we know the basics of what a relational database is, let's try talking to one.

Open DBeaver and [connect to the Training DB](./0-prerequistes.md#connecting-dbeaver-to-the-trainingdb).

### `SELECT` Statement
Since all we're doing in this course is retrieving and manipulating data (i.e. never putting anything _back_ in the database), every one of our queries will contain a `SELECT` statement.

`SELECT` asks the database to return some data. What data? Good question! We need to specify whence that data is coming, and we do that with a `FROM` clause. Combine these two keywords and you get the simplest possible SQL query:

#### Example A

=== "Source Code"

    ``` sql
    SELECT *
    FROM Students
    ```

=== "Results"

    | DCID | STUDENTID | LAST_NAME    | FIRST_NAME | MIDDLE_NAME | DOB      | GENDER | GRADE_LEVEL | STREET                  | CITY    | STATE | ZIP   | MAILING_STREET          | MAILING_CITY | MAILING_STATE | MAILING_ZIP | ENROLL_STATUS | ENROLLMENTCODE | HOME_ROOM | ENTRYDATE | ENTRYCODE | TRANSFERCOMMENT | EXITDATE | EXITCODE | EXITCOMMENT | SCHOOLID | PERSON_ID |
    |------|-----------|--------------|------------|-------------|----------|--------|-------------|-------------------------|---------|-------|-------|-------------------------|--------------|---------------|-------------|---------------|----------------|-----------|-----------|-----------|-----------------|----------|----------|-------------|----------|-----------|
    | 120  | 7223      | Subway       | Michael    | P           | ######## | M      | 0           | 1069 Cedar Hill Dr      | Jackson | MS    | 39206 | 1069 Cedar Hill Dr      | Jackson      | MS            | 39206       | 2             | 0              | Abbot     | ########  |           |                 | ######## |          |             | 700      | 3179      |
    | 4803 | 7703      | Babcock      | Joshua     | P           | 1/9/2014 | M      | 1           | 814 W Northside Dr      | Jackson | MS    | 39206 | 814 W Northside Dr      | Jackson      | MS            | 39206       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6478      |
    | 4804 | 7704      | Sorenson     | Joshua     | D           | ######## | M      | 1           | 52 Copperfield Ct       | Jackson | MS    | 39206 | 52 Copperfield Ct       | Jackson      | MS            | 39206       | 4             | 0              |           | 8/1/2022  |           |                 | ######## | W1       |             | 700      | 6577      |
    | 4805 | 7705      | Kartchner    | Nicholas   | L           | ######## | M      | 1           | 4246 Breazeale St       | Jackson | MS    | 39209 | 4246 Breazeale St       | Jackson      | MS            | 39209       | 4             | 0              |           | ########  |           |                 | ######## |          |             | 700      | 6578      |
    | 4806 | 7706      | Adams        | Jade       | P           | ######## | F      | 1           | 534 Meadowbrook Rd      | Jackson | MS    | 39206 | 534 Meadowbrook Rd      | Jackson      | MS            | 39206       | 4             | 0              |           | ########  |           |                 | ######## |          |             | 700      | 6579      |
    | 4807 | 7707      | Damico       | Carlos     | M           | ######## | M      | 1           | 1244 Dardanelle Dr      | Jackson | MS    | 39204 | 1244 Dardanelle Dr      | Jackson      | MS            | 39204       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 928       |
    | 4808 | 7708      | Gardner      | Nathan     | T           | 3/6/2014 | M      | 1           | 4612 Clinton Blvd       | Jackson | MS    | 39209 | 4612 Clinton Blvd       | Jackson      | MS            | 39209       | 4             | 0              |           | ########  |           |                 | ######## | W1       |             | 700      | 929       |
    | 4809 | 7709      | Zabriskie    | Alixandra  | B           | 4/1/2014 | F      | 1           | 2636 Maria Ter          | Jackson | MS    | 39204 | 2636 Maria Ter          | Jackson      | MS            | 39204       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 930       |
    | 4810 | 7710      | Frederickson | Dallin     | G           | ######## | M      | 1           | 2725 Hemingway Cir      | Jackson | MS    | 39209 | 2725 Hemingway Cir      | Jackson      | MS            | 39209       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6375      |
    | 4811 | 7711      | Jacobsen     | Kellimae   | G           | ######## | F      | 1           | 5441 Queen Christina Ln | Jackson | MS    | 39209 | 5441 Queen Christina Ln | Jackson      | MS            | 39209       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6376      |
    | 4812 | 7712      | Lukes        | Keith      | A           | ######## | M      | 1           | 105 Arbor Hill Dr       | Jackson | MS    | 39204 | 105 Arbor Hill Dr       | Jackson      | MS            | 39204       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6377      |
    | 4837 | 7737      | Crook        | Jed        | A           | 4/6/2012 | M      | 3           | 2121 Oakhurst Dr        | Jackson | MS    | 39204 | 2121 Oakhurst Dr        | Jackson      | MS            | 39204       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6485      |
    | 4838 | 7738      | Davidson     | Kate       | K           | ######## | F      | 3           | 2033 Wisteria Dr        | Jackson | MS    | 39204 | 2033 Wisteria Dr        | Jackson      | MS            | 39204       | 4             | 0              |           | 8/1/2022  |           |                 | ######## | W1       |             | 700      | 6486      |
    | 4839 | 7739      | Whytock      | Matthew    | S           | 5/9/2012 | M      | 3           | 5552 Draughn Dr         | Jackson | MS    | 39209 | 5552 Draughn Dr         | Jackson      | MS            | 39209       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6487      |
    | 4840 | 7740      | Nielsen      | Jeffrey    | D           | ######## | F      | 3           | 1418 Norman St          | Jackson | MS    | 39209 | 1418 Norman St          | Jackson      | MS            | 39209       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6586      |
    | 4841 | 7741      | Gessel       | Larry      | L           | ######## | M      | 3           | 376 Boling St           | Jackson | MS    | 39209 | 376 Boling St           | Jackson      | MS            | 39209       | 4             | 0              |           | ########  |           |                 | ######## |          |             | 700      | 6587      |
    | 4842 | 7742      | Jeppson      | Heather    | D           | ######## | F      | 3           | 156 Queen Victoria Ln   | Jackson | MS    | 39209 | 156 Queen Victoria Ln   | Jackson      | MS            | 39209       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6588      |
    | 4843 | 7743      | Bjerke       | Michael    | F           | ######## | M      | 4           | 236 Millsaps Ave        | Jackson | MS    | 39202 | 236 Millsaps Ave        | Jackson      | MS            | 39202       | 4             | 0              |           | ########  | P2        | promoted        | ######## | W2       | so long...  | 700      | 937       |
    | 4813 | 7713      | Gossard      | Brooks     | L           | ######## | M      | 1           | 2690 Maddox Rd          | Jackson | MS    | 39209 | 2690 Maddox Rd          | Jackson      | MS            | 39209       | 4             | 0              |           | 8/1/2022  |           |                 | ######## |          |             | 700      | 6491      |

This query tells the database to return **everything** (indicated by the `*`) from the `Students` table. Try it out and notice that it returns all 27 columns and 517 rows.

!!! warning "`SELECT *`"

    Never use `SELECT *` in production code. It's considered poor form, and it can result in issues when multiple tables have columns with the same name (e.g. `ID`). When building your queries, it's a quick and dirty way of getting a bunch of information. Just make sure that, by the time you're writing production code, you call every column explicitly.

If we want only a specific set of columns (or want them in a particular order), we can specify that easily enough:

#### Example B

=== "Source Code"

    ``` sql
    SELECT StudentID,
      Last_Name,
      First_Name,
      Grade_Level
    FROM Students
    ```

=== "Results"

    | StudentID      | Last_Name | First_Name | Grade_Level |
    |----------------|-----------|------------|-------------|
    | 7223      | Subway       | Michael    | 0           |
    | 7703      | Babcock      | Joshua     | 1           |
    | 7704      | Sorenson     | Joshua     | 1           |
    | 7705      | Kartchner    | Nicholas   | 1           |
    | 7706      | Adams        | Jade       | 1           |
    | 7707      | Damico       | Carlos     | 1           |
    | 7708      | Gardner      | Nathan     | 1           |
    | 7709      | Zabriskie    | Alixandra  | 1           |
    | 7710      | Frederickson | Dallin     | 1           |
    | 7711      | Jacobsen     | Kellimae   | 1           |


!!! note "Syntax"

    Some schools of thought (particularly, people who grew up using Microsoft SQL) like to put the comma _before_ the column name. Others prefer to put the comma _after_ the column name. Ultimately, there is no practical difference between the two schools of thought, as long as they're applied consistently.

    The [EdOps SQL Style Guide](../sqlstyleguide.md) specifies that we place commas _after_ the column name.

### `JOIN` Clauses
Being able to select columns from one table is fine and dandy, but very seldom does all the data we want live in a single table (in fact, having tables with lots of columns - a.k.a "wide" tables - is considered poor architecture!). It's time for us to make use of those **primary and foreign keys** to link data from among tables.

To pull data from multiple tables, we must **join** them. **Joining** tables basically establishes a relationship between the **primary key** in one table and the **foreign key** in another. You probably do this all the time in spreadsheets using `VLOOKUP()`: you supply the function with a primary key, and it tracks down the record in another dataset with the matching foreign key.

#### Example C

=== "Source Code"

    ``` sql
    SELECT s.StudentID,
      s.Last_Name,
      s.First_Name,
      s.Grade_Level,
      r.RaceCD
    FROM Students s
    LEFT JOIN StudentRace r ON r.StudentID = s.StudentID
    ```

=== "Results"

    | StudentID      | Last_Name | First_Name | Grade_Level | RaceCd |
    |----------------|-----------|------------|-------------|--------|
    | 7223      | Subway       | Michael    | 0           | HI     |
    | 7703      | Babcock      | Joshua     | 1           | HI     |
    | 7704      | Sorenson     | Joshua     | 1           | WH     |
    | 7705      | Kartchner    | Nicholas   | 1           | PI     |
    | 7706      | Adams        | Jade       | 1           | WH     |
    | 7707      | Damico       | Carlos     | 1           | AM     |
    | 7708      | Gardner      | Nathan     | 1           | HI     |
    | 7709      | Zabriskie    | Alixandra  | 1           | WH     |
    | 7710      | Frederickson | Dallin     | 1           | BL     |
    | 7711      | Jacobsen     | Kellimae   | 1           | HI     |

This query pulls the student's student number, name, and grade level from the `Students` table, as well as the student's race(s) from the `StudentRace` table.

There are a couple aspects of this query worth noting:

1.   Notice how each column is now prepended with a letter and a dot (e.g. `s.Grade_Level`). Since we're now working with multiple tables, we must specify from which table each column is pulled.
2.   We give each table an abbreviation (formally, an _alias_) when the table is first called (e.g. `FROM Students s`). Thereafter, we can use the alias instead of the full table name - how convenient!
3.   We have more rows than we have students! This is because some students have multiple races defined, and joining returns _every_ record that matches the primary key. This is a departure from `VLOOKUP()` which only returns the _first_ match.

!!! hint "Do I _have_ to specify the table?"

    Technically, you only have to specify the origin table of a given column if multiple tables have the same column name. In this case, the only column name shared by the two tables is `ID`, and we're not using that column, so conceivably we could have done without the aliases. However, they make your code far more comprehensible at the expense of a letter and a dot, so it's best to get in the habit.

## Applying What We've Learned: Student Contacts
The Student Contacts schema uses a _lot_ of tables, and that means we get a lot of practice with joins! Let's cook up a query that pulls all of a student's contacts.

The tables we'll want are:

| Table | Contents | Primary Key | Foreign Keys |
| --- | --- | --- | --- |
| **Students** | All of the students at _Washington Elementary_ | `StudentID` | _none_ |
| **StudentContactAssoc** | Relates each student to one or more contacts in the `Person` table | `StudentContactAssocID` | `StudentID`, `PersonID` |
| **Person** | Contains all of the adults associated with students at _Washington Elementary_ | `ID` | _none_ |
| **PersonEmailAddressAssoc** | Relates each person in the `Person` table with one or more email addresses in the `EmailAddress` table | `PersonEmailAddressAssocID` | `PersonID`, `EmailAddressID` |
| **EmailAddress** | All of the email addresses possessed by people in the `Person` table | `EmailAddressID` | _none_ |

The workflow is fairly straightforward. For each student in the `Students` table, we want:

-   All records in `StudentContactAssoc` associated with that student's `StudentID`
-   The contacts in the `Person` table pointed to by `StudentContactAssoc` for that student
-   All records in `PersonEmailAddressAssoc` associated with a given person
-   The email addresses in the `EmailAddress` table pointed to in `PersonEmailAddressAssoc` for that person

#### Example D

=== "Source Code"

    ``` sql
    SELECT s.StudentID,
      s.Last_Name,
      s.First_Name,
      s.Grade_Level,
      p.LastName,
      p.FirstName,
      e.EmailAddress
    FROM Students s
    LEFT JOIN StudentContactAssoc sca ON sca.StudentID = s.StudentID
    LEFT JOIN Person p ON p.ID = sca.PersonID
    LEFT JOIN PersonEmailAddressAssoc peaa ON peaa.PersonID = p.ID
    LEFT JOIN EmailAddress e ON e.EmailAddressID = peaa.EmailAddressID
    ```

=== "Results"

    | StudentID      | Last_Name | First_Name | Grade_Level | LASTNAME | FIRSTNAME | EMAILADDRESS                     |
    |----------------|-----------|------------|-------------|----------|-----------|----------------------------------|
    | 7223      | Subway    | Michael    | 0           | Subway    | Katie     | KatieSubway@powerschool.org     |
    | 7223      | Subway    | Michael    | 0           | Gilbert   | Subway    | SubwayGilbert@powerschool.org   |
    | 7223      | Subway    | Michael    | 0           | Roberts   | Brady     | BradyRoberts@powerschool.org    |
    | 7703      | Babcock   | Joshua     | 1           | Babcock   | Amanda    | AmandaBabcock@powerschool.org   |
    | 7703      | Babcock   | Joshua     | 1           | Babcock   | Austin    | AustinBabcock@powerschool.org   |
    | 7703      | Babcock   | Joshua     | 1           | Shobe     | Benjamin  | BenjaminShobe@powerschool.org   |
    | 7704      | Sorenson  | Joshua     | 1           | Sorenson  | Chelsea   | ChelseaSorenson@powerschool.org |
    | 7704      | Sorenson  | Joshua     | 1           | Sorenson  | Bradley   | BradleySorenson@powerschool.org |
    | 7704      | Sorenson  | Joshua     | 1           | Pettit    | Erik      | ErikPettit@powerschool.org      |
    | 7705      | Kartchner | Nicholas   | 1           | Kartchner | Kacee     | KaceeKartchner@powerschool.org  |


!!! note "Join Order"

    The [EdOps SQL Style Guide](../sqlstyleguide.md) specifies that, when writing a `JOIN` statement, we place the new table on the _left side_ of the equal sign, and the old table on the _right side_. That is, when joining `StudentContactAssoc` onto `Students`, the statement reads `sca.StudentID = s.ID` and not `s.ID = sca.StudentID`.

    Does the order actually matter? No. But when your joins have multiple criteria spanning multiple tables, it makes debugging a lot easier if they're all in the same format.

## Practice
In [Example D](#example-d) we pulled the email addresses for every student's _contacts_. But what about the _students'_ email addresses?

That's your task.

The tables you'll want are:

| Table | Contents | Primary Key | Foreign Keys |
| --- | --- | --- | --- |
| **Students** | All of the students at _Washington Elementary_ | `StudentID` | `Person_ID` |
| **Person** | Contains everybody - adult and student - at _Washington Elementary_ | `ID` | _none_ |
| **PersonEmailAddressAssoc** | Relates each person in the `Person` table with one or more email addresses in the `EmailAddress` table | `PersonEmailAddressAssocID` | `PersonID`, `EmailAddressID` |
| **EmailAddress** | All of the email addresses possessed by people in the `Person` table | `EmailAddressID` | _none_ |

The workflow is very similar to [Example D](#example-d). From the `Students` table, you will join the `Person`, `PersonEmailAddressAssoc`, and `EmailAddress` tables in order. All you have to work out is the primary/foreign keys. Good luck!

When you're ready to check out the solution, it's [here](./solutions/1-basics.md).
