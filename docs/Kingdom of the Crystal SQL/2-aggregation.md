# Aggregation, Grouping, and Ordering
In this module, we will apply some **aggregate functions** to our data. This, in turn, will expose us to situations where we need to **restrict** or **group** our data.

By the end of the module, you should be able to write a query that restricts the data pull using one or more criteria, and aggregates across one or more dimensions of that data.

## Video
A video of this module is available on [Google Drive](https://drive.google.com/file/d/18IrG9Hge6y0oZYSBa6alSOCeFkee2tIh/view?usp=share_link):
![Module 2 Video Thumbnail](./images/Module2_VideoThumbnail.png)

## Introduction
In database-speak, an **aggregate function** is a function which takes in multiple data points and returns a single data point. You're probably already familiar with the most common aggregate functions from your work with spreadsheets:

-   `SUM()`, which adds multiple values to return a single value
-   `COUNT()`, which counts the number of rows in a data set, returning a single value
-   `AVERAGE()`, which combines multiple values into a single average

Conveniently, these exact aggregate functions exist in databases as well, and we will explore them here.

## Aggregation
While we think of _aggregation_ being synonymous with _adding_, it's actually a general term for any function that turns multiple data points into a single one. Aggregation includes

-   Counting
-   Averaging
-   Finding minima and maxima
-   Joining objects into a list

### Aggregation Without Grouping
Let's start by getting a count of how many students we have in the first place. In other words, we're going to count the number of `StudentID` values present in our data set.

In this instance, we don't have any **grouping** columns since we're not subdividing our data at all:

#### Example A

=== "Query"

    ``` sql
    SELECT COUNT(StudentID)
    FROM Students
    ```

=== "Results"

    | COUNT(StudentID) |
    |---|
    | 570 |


!!! info "Aggregation Column"

    Did we have to count `StudentID`? Not necessarily. In fact, if you replace it with `COUNT(Last_Name)` you should get the same result (570 rows). However, aggregating over a _primary key_ has a few advantages that make it a good candidate for aggregation:

    -   It's **never blank** (i.e. _non-null_), which means rows can't be accidentally skipped
    -   It's **unique**, so there's no ambiguity surrounding duplicate values


## Grouping
If there's a trick to aggregate functions - and I'm not saying that there is - it's that the fields have to be divided into those being **aggregated** and those being **grouped**. The **grouped** columns are **any column that appears in the `SELECT` statement without being part of an aggregate function**.

### Aggregation With Grouping
Now, let's find out how many students are in _each grade_. That is, we will be **aggregating** the number of `StudentID` values, but **grouping** by `Grade_Level`.

Notice that our query now has a `GROUP BY` clause, which tells the database to separate the aggregation by grade level:

#### Example B

=== "Query"

    ``` sql
    SELECT Grade_Level,
    	 COUNT(StudentID)
    FROM Students
    GROUP BY Grade_Level
    ```

=== "Results"

    | GRADE_LEVEL | "COUNT(StudentID)" |
    |-------------|--------------------|
    | 0           | 100                |
    | 1           | 97                 |
    | 2           | 94                 |
    | 3           | 105                |
    | 4           | 88                 |
    | 5           | 86                 |

!!! hint "Positioning the `GROUP BY` Clause"

    The `GROUP BY` clause is the **second-to-last** clause that will appear in a query, followed only by sorting (`ORDER BY`) clauses.

### Grouping By Multiple Columns
Often, we have to group by multiple criteria, including criteria that we wouldn't normally think of as _grouped_, per se.

Suppose, for instance, that we wanted to pull each student's average grade, across all of his/her classes. That is, we're going to:

-   `SELECT` every student from the `Students` table,
-   `JOIN` the `StoredGrades` table to retrieve their grades,
-   `AVERAGE` the `Percent` column,
-   `GROUP` by student

Conceptually, _"grouping by student"_ feels like we're grouping by a _single_ quantity - say, `StudentID`. But recall from earlier that **any columns not being aggregated are grouped**. Therefore, our `GROUP BY` clause must contain

-   `StudentID`
-   `Last_Name`
-   `First_Name`
-   `Grade_Level`

#### Example C

=== "Query"

    ``` sql
    SELECT s.StudentID,
      	s.Last_Name,
      	s.First_Name,
      	s.Grade_Level,
      	AVG(sg.Percent)
    FROM Students s
    LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
    GROUP BY s.StudentID, s.Last_Name, s.First_Name, s.Grade_Level
    ```

=== "Results"

    | StudentID | Last_Name | First_Name | Grade_Level | AVG(sg.Percent) |
    |---|---|---|---|---|
    | 7218      | Hagen     | Kellie     | 0           | 55.6              |
    | 7219      | Hamilton  | Kirsten    | 0           | 57.0              |
    | 7220      | Riley     | Michael    | 0           | 62.8              |
    | 7221      | Kemink    | Corey      | 0           | 61.9              |
    | 7222      | Mcbride   | Dusty      | 0           | 58.1              |
    | 7223      | Subway    | Michael    | 0           | 61.1              |
    | 7224      | Demie     | Adam       | 0           | 52.2              |
    | 7225      | Janousek  | Sarrah     | 0           | 62.8              |
    | 7226      | Hart      | Scott      | 0           | 52.1              |
    | 7227      | Smith     | Jason      | 0           | 46.3              |

!!! note "Table Aliases"

    Note that, in this example, all column names are prepended with the **alias** of their table. This is because we have a `JOIN` clause, and therefore must specify the origin of each column.

!!! hint "Listing Grouping Columns"

    There are two stylistic notes regarding the enumeration of grouping columns:

    -   The grouping columns should, when possible, follow the **same order** in the `GROUP BY` clause as they follow in the `SELECT` clause.

    -   The grouping columns can appear on the same line, as long as they're under about 70 characters in total. Beyond that, you will want to place one column per line, a'la the `SELECT` clause.

## Ordering

In [Example B](#example-b), we were lucky that our counts happened to be in ascending order by grade level. In [Example C](#example-c), though, the students appeared in any old order. In fact, if you ran the query multiple times, the order would likely change randomly (known as _stochastic behavior_) because the database has no instructions on how to display the data.

To fix this, we can give the query explicit instructions on how to sort, or **order**, the data. In this case, we want to order by:

-   `Grade_Level`, _ascending_, followed by
-   `Last_Name`, _ascending_, followed by
-   `First_Name`, _ascending_

#### Example D

=== "Query"

    ``` sql
    SELECT s.StudentID,
      	s.Last_Name,
      	s.First_Name,
      	s.Grade_Level,
      	AVG(sg.Percent)
    FROM Students s
    LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
    GROUP BY s.StudentID, s.Last_Name, s.First_Name, s.Grade_Level
    ORDER BY s.Grade_Level ASC, s.Last_Name ASC, s.First_Name ASC
    ```

=== "Results"

    | StudentID | Last_Name | First_Name | Grade_Level | AVG(sg.Percent) |
    |---|---|---|---|---|
    | 7260      | Alu       | Matthew    | 0           | 46.8              |
    | 7279      | Andersen  | Anthony    | 0           | 41.8              |
    | 7694      | Anderson  | Bruce      | 0           | 62.8              |
    | 7293      | Ashcroft  | Justin     | 0           | 42.3              |
    | 7252      | Attridge  | Michael    | 0           | 46.2              |
    | 24966     | Avery     | Ashlee     | 0           | 61.1              |
    | 24965     | Bateman   | Heather    | 0           | 52.2              |
    | 7265      | Benson    | Jordan     | 0           | 51.0              |
    | 7263      | Brush     | Jed        | 0           | 47.2              |
    | 24957     | Bybee     | Greg       | 0           | 46.3              |

!!! hint "Ordering Columns"

    As with the `GROUP BY` clause, the `ORDER BY` clause can be on a single line, provided the line is of a reasonable length. Otherwise, it should be spread onto one line per column.

!!! note "Can I sort by Average?"

    **No.** Since the average is an **aggregated** column, it can't be sorted by in the same query. We'll look at how to sort by such things when we dive into **subqueries** in a later module.

## Filtering Data
So far, all of our queries have returned _every available row_, regardless of whether or not we wanted it. More often than not, though, we want to filter our data to return only a particular subset, or to suppress undesired values.

In SQL, filtering is done using a `WHERE` clause, which specifies the criteria by which the data will be filtered.

### Filtering By a Single Criterion

Consider our basic queries from [Module 1](./1-basics.md), which always returned all available rows for _every_ student at Washington Elementary. Let's modify one of those queries to focus on the _3rd grade_.

#### Example E

=== "Query"

    ```sql
    SELECT StudentID,
      Last_Name,
      First_Name,
      Grade_Level
    FROM Students
    WHERE Grade_Level = 3
    ```

=== "Results"

    | StudentID | Last_Name | First_Name | Grade_Level |
    |---|---|---|---|
    | 7737      | Crook       | Jed        | 3           |
    | 7738      | Davidson    | Kate       | 3           |
    | 7739      | Whytock     | Matthew    | 3           |
    | 7740      | Nielsen     | Jeffrey    | 3           |
    | 7741      | Gessel      | Larry      | 3           |
    | 7742      | Jeppson     | Heather    | 3           |
    | 7724      | Lavery      | Christoph  | 3           |
    | 7725      | Rushton     | Joshua     | 3           |
    | 7726      | Allred      | Charles    | 3           |
    | 7727      | Butterfield | Candice    | 3           |


### Filtering By Multiple Criteria

Now, let's modify one of our earlier examples from this module to return only students who are _Female_ and in the _4th Grade_.

Since there are two criteria here, we will need to use the `AND` operator.

#### Example F

=== "Query"

    ```sql
    SELECT s.StudentID,
      	s.Last_Name,
      	s.First_Name,
      	s.Grade_Level,
      	AVG(sg.Percent)
    FROM Students s
    LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
    WHERE s.Gender = 'F'
      AND s.Grade_Level = 4
    GROUP BY s.StudentID, s.Last_Name, s.First_Name, s.Grade_Level
    ```

=== "Results"

    | StudentID | Last_Name | First_Name | Grade_Level | AVG(sg.Percent) |
    |---|---|---|---|---|
    | 7530      | Reyes     | Heidi      | 4           | 71.5625 |
    | 7531      | Hagen     | Kirsten    | 4           | 74.0    |
    | 7533      | Hodges    | Sarah      | 4           | 69.3125 |
    | 7535      | Anderson  | Nancy      | 4           | 68.9375 |
    | 7540      | Ham       | Angelina   | 4           | 69.0625 |
    | 7542      | Janousek  | Sara       | 4           | 73.3125 |
    | 7545      | Sharpe    | Maria      | 4           | 72.0625 |
    | 7555      | Lewis     | Janna      | 4           | 75.375  |
    | 7556      | Ritland   | Lauren     | 4           | 71.1875 |
    | 7557      | Lawrence  | Kathryn    | 4           | 67.9375 |

!!! note "Placement"

    The `WHERE` clause goes **after** all `JOIN` statements, but **before** any `GROUP BY` or `ORDER BY` clauses.

!!! hint "Separate Lines"

    The [EdOps SQL Style Guide](../sqlstyleguide.md) dictates that each criterion gets its own line, and lines `2-n` are indented.

## Nuanced Filtering
Filtering can be done outside the `WHERE` clause as well. In particular, certain constraints can be inserted in the `JOIN` clause to restrict the data _before_ it gets added to the query.

For example: presently, our query averages _every_ grade it finds for a student. However, a quick inspection of the `StoredGrades` table shows that it contains grades from all four quarters of the year. Suppose that we only want `Q1` grades to be averaged.

We can't accomplish this restriction using the `WHERE` clause since it is executed **after** the average. Instead, we need to perform the restriction at the point of the join.

#### Example G

=== "Query"

    ```sql
    SELECT s.StudentID,
        s.Last_Name,
        s.First_Name,
        s.Grade_Level,
        AVG(sg.Percent)
    FROM Students s
    LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
      AND sg.StoreCode = 'Q1'
    WHERE s.Gender = 'F'
      AND s.Grade_Level = 4
    GROUP BY s.StudentID, s.Last_Name, s.First_Name, s.Grade_Level
    ```

=== "Results"

    |StudentID|Last_Name|First_Name|Grade_Level|AVG(sg.Percent)|
    |--------------|---------|----------|-----------|---------------|
    | 7530      | Reyes     | Heidi      | 4           | 65.125 |
    | 7531      | Hagen     | Kirsten    | 4           | 69.5   |
    | 7533      | Hodges    | Sarah      | 4           | 62.625 |
    | 7535      | Anderson  | Nancy      | 4           | 65.625 |
    | 7540      | Ham       | Angelina   | 4           | 65.25  |
    | 7542      | Janousek  | Sara       | 4           | 62.75  |
    | 7545      | Sharpe    | Maria      | 4           | 65.875 |
    | 7555      | Lewis     | Janna      | 4           | 69.25  |
    | 7556      | Ritland   | Lauren     | 4           | 67.25  |
    | 7557      | Lawrence  | Kathryn    | 4           | 67.875 |


!!! note "Order of Operations"

    With the exception of the `GROUP BY` clause, SQL statements are more or less executed in the order written. As such, the `WHERE` clause is typically the _last_ clause executed by the parser.

    The [EdOps SQL Style Guide](../sqlstyleguide.md) says, when possible, to place data restrictions in the `WHERE` clause, and in general, this is good practice. As you gain familiarity with SQL queries, you will learn when to place a restriction in a `JOIN` clause instead.

## Practice

In [Example G](#example-g) we pulled:

-   An average
-   Of stored grades
-   That only included `Q1` grades
-   For students who were Female and in the 4th grade

Your task is to make a few modifications to this query. Specifically, we want to pull:

-   The number (i.e. `COUNT`)
-   Of stored grades
-   That only includes `Q2` grades
-   For students who are:
    -   In the **3rd Grade**

In addition to the count of grades, your query should include the following columns:

-   `StudentID`
-   `Last_Name`
-   `First_Name`
-   `Gender`
-   `DOB`

Finally, your query should be **sorted** by:

-   `Last_Name`, _ascending_, followed by
-   `First_Name`, _ascending_

Good luck!

When you're ready to check out the solution, it's [here](./solutions/2-aggregation.md).
