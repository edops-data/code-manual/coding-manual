# Module 4 Solution

## Practice

In this practice, you're going to create a **consolidated listing of student course grades**.

In our training database, grades are stored in two locations:

-   The `PGFinalGrades` table, which contains the grades for the current year, _as they appear in the teacher's gradebook_
-   The `StoredGrades` table, which contains the grades for this and previous years, _as they appeared at the time of storage_

You must unite these tables into a single listing, consisting of the columns:

| Alias | Description | `PGFinalGrades` Name | `StoredGrades` Name |
| --- | --- | --- | --- |
| `StudentID` | The Student's internal ID | `StudentID` | `StudentID` |
| `Course_Number` | The course to which the grade belongs | Not present. You will need to join the `Sections` table using `SectionID` to retrieve `Course_Number` | `Course_Number` |
| `GradingTerm` | The quarter in which the grade was earned | `FinalGradeName` | `StoreCode` |
| `Percent` | The percent earned in the course | `Percent` | `Percent` |
| `Grade` | The letter grade corresponding to `Percent` | `Grade` | `Grade` |
| `Passing` | Whether the grade counts toward course mastery | `Passing` | `EarnedCrHrs` |

Additionally, the `Passing` column should be represented as **text**, specifically the values `Pass`/`Fail`, corresponding to the existing values of `1`/`0`. Finally, any records where `Grade` is missing (represented either by a `NULL` or by `--`) should be **excluded**.

This is a complex ask, so here are some hints and tips.

### 1. Prep the queries separately, sans casting and filtering
First, construct the appropriate query on the `StoredGrades` table, which will involve

-   Selecting the appropriate columns
-   Aliasing the `StoreCode` column

Then, set that query aside and construct the one for the `PGFinalGrades` table, which will involve

-   Selecting the appropriate columns
-   Joining the `Sections` table to obtain the `Course_Number` column
-   Aliasing the `FinalGradeName` column

### 2. Construct the `Passing` column
For each of your two queries, add the code that casts `Passing` into text. My advice? Use a `CASE` statement, which you can review in [Module 3](./3-aliases-cases-nulls.md#cases)

### 3. Filter out missing grades
For each of your two queries, add the clause that removes missing grades. This is best done with a `WHERE` clause, which you can review in [Module 2](./2-aggregation.md#filtering-by-a-single-criterion). The clause will look slightly different for each query, since missing grades are coded differently in the two source tables.

### 4. Unite the queries
Use a `UNION` statement to combine your two queries. You **will** want to exclude duplicate values, since grades for the current year will appear in both tables.

Good luck!

## Solution

=== "Query"

    ```sql
    SELECT StudentID,
    	Course_Number,
    	StoreCode AS GradingTerm,
    	Percent,
    	Grade,
    	CASE
    		WHEN EarnedCrHrs = 1 THEN 'Pass'
    		WHEN EarnedCrHrs = 0 THEN 'Fail'
    		ELSE NULL
    	END AS Passing
    FROM StoredGrades
    WHERE Grade IS NOT NULL

    UNION

    SELECT pgf.StudentID,
    	sec.Course_Number,
    	pgf.FinalGradeName AS GradingTerm,
    	pgf.Percent,
    	pgf.Grade,
    	CASE
    		WHEN Passing = 1 THEN 'Pass'
    		WHEN Passing = 0 THEN 'Fail'
    		ELSE NULL
    	END AS Passing
    FROM PGFinalGrades pgf
    LEFT JOIN Sections sec ON sec.ID = pgf.SectionID
    WHERE pgf.Grade <> '--'
        AND pgf.Grade IS NOT NULL
    ```

=== "Results"

    | StudentID | Course_Number | GradingTerm | Percent | Grade | Passing |
    |-----------|---------------|-------------|---------|-------|---------|
    | 7218 | EL1    | Q1 | 77 | C | Pass |
    | 7218 | EL1    | Q1 | 90 | A | Fail |
    | 7218 | EL1    | Q2 | 73 | C | Pass |
    | 7218 | EL1    | Q2 | 83 | B | Fail |
    | 7218 | EL1    | Q3 | 56 | F | Fail |
    | 7218 | EL1    | Q4 | 61 | D | Pass |
    | 7218 | EL1    | S1 | 75 | C | Pass |
    | 7218 | EL1    | S2 | 59 | F | Fail |
    | 7218 | EL1000 | Q1 | 91 | A | Pass |
    | 7218 | EL1000 | Q2 | 83 | B | Pass |
    | 7218 | EL1000 | Q2 | 87 | B | Fail |
    | 7218 | EL1000 | Q3 | 89 | B | Pass |
    | 7218 | EL1000 | Q4 | 83 | B | Pass |
    | 7218 | EL1000 | S1 | 87 | B | Pass |
    | 7218 | EL1000 | S2 | 86 | B | Pass |
    | 7218 | EL2000 | Q1 | 69 | D | Fail |
    | 7218 | EL2000 | Q1 | 96 | A | Pass |
    | 7218 | EL2000 | Q2 | 69 | D | Fail |
    | 7218 | EL2000 | Q2 | 95 | A | Pass |
    | 7218 | EL2000 | Q3 | 98 | A | Pass |