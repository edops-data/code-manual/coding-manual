# Module 2 Solution

## Practice

In [Example G](#example-g) we pulled:

-   An average
-   Of stored grades
-   That only included `Q1` grades
-   For students who were Female and in the 4th grade

Your task is to make a few modifications to this query. Specifically, we want to pull:

-   The number (i.e. `COUNT`)
-   Of stored grades
-   That only includes `Q2` grades
-   For students who are:
    -   In the **3rd Grade**

In addition to the count of grades, your query should include the following columns:

-   `StudentID`
-   `Last_Name`
-   `First_Name`
-   `Gender`
-   `DOB`

Finally, your query should be **sorted** by:

-   `Last_Name`, _ascending_, followed by
-   `First_Name`, _ascending_

Good luck!

## Solution

=== "Query"

    ```sql
    SELECT s.StudentID,
      s.Last_Name,
      s.First_Name,
      s.Gender,
      s.DOB,
      COUNT(sg.Percent)
    FROM Students s
    LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
      AND sg.StoreCode = 'Q2'
    WHERE s.Grade_Level = 3
    GROUP BY s.StudentID, s.Last_Name, s.First_Name, s.Gender, s.DOB
    ORDER BY s.Last_Name ASC, s.First_Name ASC
    ```

=== "Results"

    | Student_Number | Last_Name | First_Name | Gender | DOB        | COUNT(sg.Percent) |
    |----------------|-----------|------------|--------|------------|-------------------|
    | 7480      | Adams     | Jennifer   | F           | 11/23/2014 | 8 |
    | 7500      | Ainsworth | Cole       | M           | 3/14/2014  | 8 |
    | 7726      | Allred    | Charles    | M           | 9/25/2012  | 0 |
    | 24979     | Anderson  | Uentil     | F           | 7/28/2014  | 0 |
    | 7514      | Berg      | Stephanie  | F           | 4/19/2014  | 8 |
    | 7458      | Birkeland | Casey      | M           | 7/6/2014   | 8 |
    | 7488      | Bourassa  | Vanessa    | F           | 1/6/2014   | 8 |
    | 7733      | Bowles    | Crystal    | F           | 2/18/2012  | 0 |
    | 7495      | Bradshaw  | Charidee   | F           | 1/31/2014  | 8 |
    | 7523      | Brakke    | Rosally    | F           | 5/6/2014   | 8 |
