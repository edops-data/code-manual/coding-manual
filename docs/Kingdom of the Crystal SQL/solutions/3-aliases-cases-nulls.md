# Module 3 Solution

## Practice

In this practice, you're going to create a listing of **how many class sections each teacher is assigned** at _Washington Elementary_. To do this, you will use:

#### `Teachers` Table
The `Teachers` table contains, as you would expect, a record for every teacher in the _Apple Grove Unified School District_, which includes _Washington Elementary_. Since we haven't used this table before, you might want to `SELECT *` and explore the data a little bit. However, of the table's 8 columns, the only ones that will appear in your query are

-   `LastFirst` - the teacher's name, and
-   `ID` - the teacher's ID (and the table's primary key)

#### `Sections` Table
The `Sections` table contains a listing of every class section at _Washington Elementary_. Again, since we haven't used this table before, an exploratory `SELECT *` wouldn't go amiss. However, the only two columns we really need are

-   `ID` - the section ID number, and
-   `Teacher` - the foreign key which relates to `Teachers.ID`.

### Requirements
Your task is to create a query which:

-   Contains two columns: `TeacherName` and `SectionCount`, where
    -   `TeacherName` is the teacher's name (duh), and
    -   `SectionCount` is a count of the `Sections.ID` records assigned to that teacher
-   Displays _only_ teachers who are assigned to at least one section

### Hints

-   You can accomplish that last criterion either with an `INNER JOIN` or a `WHERE` clause - it's up to your level of comfort.
    -   If you go the `WHERE` clause route, I would recommend first making a listing of every section assigned to each teacher, prior to generating the count, to make sure that you have your filters correctly placed.
-   Since your query includes a **count**, you may want to review the module on [Aggregation](./2-aggregation.md), in particular how [grouping](./2-aggregation.md#grouping) works.
-   Obviously, there are not columns named `TeacherName` and `SectionCount`. Fortunately, today you learned how to name a column whatever you want.

Good luck!

## Solution (using an `INNER JOIN`)

=== "Query"

    ```sql
    SELECT t.LastFirst AS TeacherName,
    	COUNT(sec.ID) AS SectionCount
    FROM Teachers t
    INNER JOIN Sections sec ON sec.Teacher = t.ID
    GROUP BY t.LastFirst
    ```

=== "Results"

    | TeacherName              | SectionCount |
    |--------------------------|--------------|
    | Abbot, Julie       | 2 |
    | Adair, Amanda      | 2 |
    | Allen, Sarah F     | 2 |
    | Arnold, Paul H     | 2 |
    | Barker, Margaret J | 5 |
    | Blain, Barbara     | 5 |
    | Boren, Jennifer    | 5 |
    | Brooks, Jeffery R  | 5 |
    | Call, Tyler V      | 5 |
    | Childs, Michael Z  | 5 |

## Solution (using a `WHERE` clause)

=== "Query"

    ```sql
    SELECT t.LastFirst AS TeacherName,
    	COUNT(sec.ID) AS SectionCount
    FROM Teachers t
    LEFT JOIN Sections sec ON sec.Teacher = t.ID
    WHERE sec.ID IS NOT NULL
    GROUP BY t.LastFirst
    ```

=== "Results"

    | TeacherName              | SectionCount |
    |--------------------------|--------------|
    | Abbot, Julie       | 2 |
    | Adair, Amanda      | 2 |
    | Allen, Sarah F     | 2 |
    | Arnold, Paul H     | 2 |
    | Barker, Margaret J | 5 |
    | Blain, Barbara     | 5 |
    | Boren, Jennifer    | 5 |
    | Brooks, Jeffery R  | 5 |
    | Call, Tyler V      | 5 |
    | Childs, Michael Z  | 5 |
