# Module 1 Solution

## Problem
In [Example D](../1-basics.md#example-d) we pulled the email addresses for every student's _contacts_. But what about the _students'_ email addresses?

That's your task.

The tables you'll want are:

| Table | Contents | Primary Key | Foreign Keys |
| --- | --- | --- | --- |
| **Students** | All of the students at _Washington Elementary_ | `StudentID` | `Person_ID` |
| **Person** | Contains everybody - adult and student - at _Washington Elementary_ | `ID` | _none_ |
| **PersonEmailAddressAssoc** | Relates each person in the `Person` table with one or more email addresses in the `EmailAddress` table | `PersonEmailAddressAssocID` | `PersonID`, `EmailAddressID` |
| **EmailAddress** | All of the email addresses possessed by people in the `Person` table | `EmailAddressID` | _none_ |

The workflow is very similar to [Example D](#example-d). From the `Students` table, you will join the `Person`, `PersonEmailAddressAssoc`, and `EmailAddress` tables in order. All you have to work out is the primary/foreign keys. Good luck!

## Solution

=== "Query"

    ```sql
    SELECT s.StudentID,
      s.Last_Name,
      s.First_Name,
      s.Grade_Level,
      e.EmailAddress
    FROM Students s
    LEFT JOIN Person p ON p.ID = s.Person_ID
    LEFT JOIN PersonEmailAddressAssoc peaa ON peaa.PersonID = p.ID
    LEFT JOIN EmailAddress e ON e.EmailAddressID = peaa.EmailAddressID
    ```

=== "Results"

    | Student_Number | Last_Name | First_Name | Grade_Level | EMAILADDRESS |
    |---|---|---|---|---|
    | 7706      | Adams     | Jade       | 1           |                                 |
    | 7371      | Adams     | Brandon    | 1           | Adams.Brandon@powerschool.com   |
    | 7480      | Adams     | Jennifer   | 3           | Adams.Jennifer@powerschool.com  |
    | 7500      | Ainsworth | Cole       | 3           | Ainsworth.Cole@powerschool.com  |
    | 7726      | Allred    | Charles    | 3           |                                 |
    | 7664      | Allred    | Kathleen   | 5           | Allred.Kathleen@powerschool.com |
    | 7260      | Alu       | Matthew    | 0           | Alu.Matthew@powerschool.com     |
    | 7577      | Alu       | Anthony    | 4           | Alu.Anthony@powerschool.com     |
    | 7389      | Andersen  | Ashley     | 2           | Andersen.Ashley@powerschool.com |
    | 7430      | Andersen  | Apaulo     | 2           | Andersen.Apaulo@powerschool.com |

!!! note "Missing Values"

    The first couple dozen students lack email addresses, so I've omitted them from the _Results_ table. If you scroll down in your result set, though, you'll see them.

    If you want your results to look exactly like mine, add the line `ORDER BY Last_Name ASC` to the end of your query.


!!! hint "Is the `Person` table really necessary?"

    **No, it's not.** Unlike Example D, we're not actually extracting any information from the `Person` table, so assuming we can get all the other keys we need elsewhere (and we can!), there's no need to include it in our query.
    
    In particular, since `PersonID` exists as a foreign key in both the `Students` and `PersonEmailAddressAssoc` tables, we can skip right to the punch line and join using

    ```
    LEFT JOIN PersonEmailAddressAssoc peaa ON peaa.PersonID = s.Person_ID
    ```

    So why did I include it in the solution? First, parallel structure with the example is always a plus. Second, since you're likely still getting the hang of primary and foreign keys, maintaining the structure of joining every foreign key to its primary counterpart may help you conceptualize what's going on. If not, feel free to skip.