# Module 5 Solution

## Practice

In this practice, you're going to create a **listing of student GPAs**. For our purposes,

    GPA = [sum of grade points] * 1.00 / [count of grades]

Your query should return a listing of each student (`StudentID`, `Last_Name`, `First_Name`) with their SY2023 Q1 GPA, in descending order by GPA. Since you're sorting on a computed column (GPA), you will need, at the very least, a subquery. I recommend structuring your query similarly to [Example D](#example-d), where CTEs are used to:

1.  Pull the relevant data
2.  Perform the aggregation

Finally, the division and rounding can be performed in a subquery, or in a CTE that gets referenced in your final `SELECT` statement. Should you go this route, some hints about structure:

### 1. Pull the relevant data

With the exceptions of student names (which live in the `Students` table), you will be pulling all grade-related data from the `StoredGrades` table. You will want to filter this table for `TermID >= 3200` (i.e. SY2023) and `StoreCode = 'Q1'`.

You will want the following columns:

-   `DCID` - the unique identifier of each grade, which can be used to count the number of grades present
-   `GPA_Points` - the number of grade points associated with that grade

### 2. Perform the aggregation

Once you have pulled your data, you can `COUNT()` the number of grades, and `SUM()` the grade points, resulting in columns which can be divided at the end.

!!! hint "Additional Hint: Division by Zero"

    There are students who have **no stored grades**. For these students, your count of grades will be zero, and thus the GPA will contain a divide-by-zero error. While there are various ways to handle this circumstance, let's take the easy way and filter them out using a `WHERE` clause.

Good luck!

## Solution

=== "Query"

    ```sql
    WITH GradeCounts AS (
    	SELECT StudentID,
    		COUNT(DCID) AS GradeCount,
    		SUM(GPA_Points) AS GPAPoints
    	FROM StoredGrades
    	WHERE TermID >= 3200
    		AND StoreCode = 'Q1'
    	GROUP BY StudentID
    )
    SELECT StudentID,
    	Last_Name,
    	First_Name,
    	GPA
    FROM (SELECT s.StudentID,
    		s.Last_Name,
    		s.First_Name,
    		ROUND(g.GPAPoints * 1.00 / g.GradeCount, 4) AS GPA
    	FROM Students s
    	LEFT JOIN GradeCounts g ON g.StudentID = s.StudentID
    	WHERE g.GradeCount > 0
    ) g
    ORDER BY GPA DESC
    ```

=== "Results"

    | StudentID | Last_Name | First_Name | GPA |
    |----------------|-----------|------------|-----|
    | 7690 | Weltikol  | Cherstin | 3.0   |
    | 7392 | Wilson    | Bailey   | 3.0   |
    | 7379 | Gray      | Angela   | 3.0   |
    | 7466 | Wood      | Camille  | 3.0   |
    | 7553 | Zubke     | Matthew  | 3.0   |
    | 7590 | Watson    | Caroline | 3.0   |
    | 7434 | Johnson   | Jessica  | 3.0   |
    | 7525 | Nielsen   | Joshua   | 3.0   |
    | 7527 | Butcher   | Brian    | 3.0   |
    | 7422 | Rackley   | Philip   | 3.0   |
    | 7609 | Wetzel    | Katie    | 3.0   |
    | 7487 | Garbow    | Ryan     | 3.0   |
    | 7684 | Watson    | Keegan   | 2.875 |
    | 7620 | Hansen    | Austin   | 2.875 |
    | 7629 | Hansen    | Joey     | 2.875 |
    | 7382 | Koppelman | Mel      | 2.875 |
    | 7492 | Philpot   | Alison   | 2.875 |
    | 7576 | Sherwood  | Joshua   | 2.875 |
    | 7591 | Jones     | Diego    | 2.875 |
    | 7502 | Georges   | Deanna   | 2.875 |
