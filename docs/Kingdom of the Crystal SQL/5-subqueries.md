# Subqueries and Common Table Expressions

Occasionally it's impossible to perform all the necessary data processing in a single query. Instead, we have to build the query in pieces, combining those pieces strategically to form our output.

Additionally, you will often find yourself using the same lines of code across multiple queries, and want to be able to copy-paste the code from project to project.

Subqueries and Common Table Expressions (CTEs) allow this to happen.

By the end of this module, you should be able to write a query that:

-   Operates on a column that was the product of an aggregate function
-   Stores portions of the query in reusable CTEs

Additionally, you should leave this module with a sense of when to use a subquery versus a CTE.

## Video
A video of this module is available on [Google Drive](https://drive.google.com/file/d/1BlPs8_B6y5drCxc77BugNUTNjNkhUpoQ/view?usp=share_link):
![Module 5 Video Thumbnail](./images/Module5_VideoThumbnail.png)

## Subqueries

Back in [Module 2, Example D](./2-aggregation.md#example-d), we noted that it's impossible to sort by an aggregate column - at least, not within the query that performed the aggregation. Let's take a look at that query again:

``` sql
SELECT s.StudentID,
    s.Last_Name,
    s.First_Name,
    s.Grade_Level,
    AVG(sg.Percent) AS AverageGrade
FROM Students s
LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
GROUP BY s.StudentID, s.Last_Name, s.First_Name, s.Grade_Level
ORDER BY s.Grade_Level ASC, s.Last_Name ASC, s.First_Name ASC
```

Suppose we wanted to sort this list by the student's average grade, say to identify students for Honor Roll. We can perform the sort by wrapping our existing query inside _another query_.

#### Example A

=== "Query"

    ``` sql
    SELECT * FROM (
      SELECT s.StudentID,
        	s.Last_Name,
        	s.First_Name,
        	s.Grade_Level,
        	AVG(sg.Percent) AS AverageGrade
      FROM Students s
      LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
      GROUP BY s.StudentID, s.Last_Name, s.First_Name, s.Grade_Level
      ORDER BY s.Grade_Level ASC, s.Last_Name ASC, s.First_Name ASC
    ) StudentAverages
    ORDER BY AverageGrade DESC
    ```

=== "Results"

    | Student_Number | Last_Name | First_Name | Grade_Level | AverageGrade |
    |----------------|-----------|------------|-------------|--------------|
    | 7609 | Wetzel   | Katie    | 4 | 91.29 |
    | 7553 | Zubke    | Matthew  | 4 | 91.29 |
    | 7590 | Watson   | Caroline | 4 | 90.5  |
    | 7230 | Zubke    | Amanda   | 0 | 90.0  |
    | 7392 | Wilson   | Bailey   | 2 | 89.0  |
    | 7684 | Watson   | Keegan   | 5 | 88.71 |
    | 7690 | Weltikol | Cherstin | 5 | 87.86 |
    | 7379 | Gray     | Angela   | 2 | 87.5  |
    | 7402 | Tela     | Jessica  | 2 | 86.79 |
    | 7466 | Wood     | Camille  | 3 | 86.64 |


!!! note "`SELECT *`"

    Yes, it's bad form. But the goal was to decorate the query as little as possible so you could see the structure. Sue me.


!!! note "Alias"

    Wrapping our old query in parentheses tells the database engine to interpret it as though it were a _table_. As such, we need to **alias** that table so the engine knows what to call it.

### Using a Subquery to Apply a Filter

We know of two ways to filter a query: applying a restriction in the `WHERE` clause, and applying conditions to a `JOIN` statement. However, you may recall that the `WHERE` clause is among the _last_ statements to be processed in a SQL query. As a consequence, especially if your starting table is rather large, you can end up wasting considerable processing power wrangling an unnecessarily large data set, only to trim it down at the very end.

Strategically filtering tables in a subquery can help alleviate some of these processing concerns.

#### Example B

Consider this query, which enumerates every day that each student was enrolled this year (handy for auditing attendance):

!!! note "Julian Days"
    As alluded to in [Module 4](./4-unions-datatypes.md#example-d), SQLite is weird with dates. It doesn't contain a built-in date type for dates, storing them as one of two options:

    -   Strings (good to look at but a pain to process)
    -   Dates on the **Julian Calendar**, stored as an integer representing the number of days since November 24, 4714 B.C. (go figure).

    So that dates can be treated sequentially (something that text can't do), I've converted all the relevant dates to their Julian equivalents. Don't concern yourself with the details. Instead:

    -   Treat the _JulianDate_ columns as though they were regular _Date_ columns
    -   Ignore the `JULIANDAY()` function entirely, and just pretend that it's a regular date.
    -   Ignore the `STRFTIME()` function, which just reformats Julian dates to their human-readable equivalents.

```sql
SELECT s.StudentID,
	s.Last_Name,
	s.First_Name,
	s.EntryDate,
	s.ExitDate,
	STRFTIME('%m/%d/%Y', cd.JulianDate) AS Date
FROM Students s
LEFT JOIN Calendar_Day cd ON cd.SchoolID = s.SchoolID
	AND cd.InSession = 1
	AND JulianDate BETWEEN s.EntryDateJulian AND s.ExitDateJulian
WHERE s.EntryDateJulian >= JULIANDAY('2022-08-01')
```

The `Students` table contains lots of enrollments from past years -- some enrollments date all the way back to 2001! Our query currently does the following:

1.  Pull every student enrollment
2.  Attach every in-session day for that enrollment's year
3.  Discard every enrollment prior to SY2023

That's not exactly efficient, and indeed, if we were dealing with the entire Apple Grove data set (17k students!) it would be miserable to work with.

In theory, we could perform the restriction when we join the `Calendar_Day` table, but that would carry its own consequences (namely, if a student's `EntryDate` accidentally preceded the school year, that entire student's enrollment would be discarded - bad for data validation).

Instead, though, let's restrict the table before any processing is done to it at all by using a subquery:

```sql
SELECT StudentID,
  Last_Name,
  First_Name,
  EntryDate,
  EntryDateJulian,
  ExitDate,
  ExitDateJulian,
  SchoolID
FROM Students
WHERE EntryDateJulian >= JULIANDAY('2022-08-01')
```

!!! note "Columns"
    Because we can't, according to the Style Guide, `SELECT *` on the `Students` table, we need to take care to explicitly call every column that we could potentially need down the road. Annoying, but rules are rules.

Now, we put it together.

=== "Query"

    ``` sql
    SELECT s.StudentID,
    	s.Last_Name,
    	s.First_Name,
    	s.EntryDate,
    	s.ExitDate,
    	STRFTIME('%m/%d/%Y', cd.JulianDate) AS Date
    FROM (SELECT StudentID,
        Last_Name,
        First_Name,
        EntryDate,
        EntryDateJulian,
        ExitDate,
        ExitDateJulian,
        SchoolID
      FROM Students
      WHERE EntryDateJulian >= JULIANDAY('2022-08-01')
    ) s
    LEFT JOIN Calendar_Day cd ON cd.SchoolID = s.SchoolID
    	AND cd.InSession = 1
    	AND JulianDate BETWEEN s.EntryDateJulian AND s.ExitDateJulian
    ```

=== "Results"

    | StudentID | Last_Name | First_Name | EntryDate | ExitDate  | Date       |
    |----------------|-----------|------------|-----------|-----------|------------|
    | 7703 | Babcock      | Joshua    | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7705 | Kartchner    | Nicholas  | 12/14/2022 | 7/31/2023 | 01/02/2023 |
    | 7706 | Adams        | Jade      | 12/20/2022 | 7/31/2023 | 01/02/2023 |
    | 7707 | Damico       | Carlos    | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7709 | Zabriskie    | Alixandra | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7710 | Frederickson | Dallin    | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7711 | Jacobsen     | Kellimae  | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7712 | Lukes        | Keith     | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7737 | Crook        | Jed       | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7738 | Davidson     | Kate      | 8/1/2022   | 2/21/2023 | 01/02/2023 |

!!! note "Result Set"

    The default result set sorts by student, which isn't helpful for confirming accuracy. To reproduce my result set, add
    ```
    ORDER BY Date ASC
    ```
    to the end of your query.


!!! note "Style"
    The [EdOps SQL Style Guide](../sqlstyleguide.md) stipulates that subqueries - like any multi-line statement inside parentheses - gets indented by one level.

## Common Table Expressions

Subqueries have two drawbacks:

-   When longer than a couple lines, they make the overall query harder to read
-   If you need to invoke them multiple times, you have to copy-paste the entire subquery

Fortunately, we can solve both issues by transforming these subqueries into **Common Table Expressions** (CTEs)!

A **Common Table Expression** is just a fancy form of a subquery. A CTE:

-   Exists at the **beginning** of a query,
-   Is introduced with a `WITH` clause,
-   Gets processed **before** the rest of the query,
-   Gets a **unique name**, and
-   Can be called just like a table

Let's start by transforming our subquery from [Example B](#example-b) into a CTE.

#### Example C

=== "Query"

    ``` sql
    WITH
    SY23Students AS (
      SELECT StudentID,
          Last_Name,
          First_Name,
          EntryDate,
          EntryDateJulian,
          ExitDate,
          ExitDateJulian,
          SchoolID
        FROM Students
        WHERE EntryDateJulian >= JULIANDAY('2022-08-01')
    )
    SELECT s.StudentID,
    	s.Last_Name,
    	s.First_Name,
    	s.EntryDate,
    	s.ExitDate,
    	STRFTIME('%m/%d/%Y', cd.JulianDate) AS Date
    FROM SY23Students s
    LEFT JOIN Calendar_Day cd ON cd.SchoolID = s.SchoolID
    	AND cd.InSession = 1
    	AND JulianDate BETWEEN s.EntryDateJulian AND s.ExitDateJulian
    ```

=== "Results"

    | StudentID | Last_Name | First_Name | EntryDate | ExitDate  | Date       |
    |----------------|-----------|------------|-----------|-----------|------------|
    | 7703 | Babcock      | Joshua    | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7705 | Kartchner    | Nicholas  | 12/14/2022 | 7/31/2023 | 01/02/2023 |
    | 7706 | Adams        | Jade      | 12/20/2022 | 7/31/2023 | 01/02/2023 |
    | 7707 | Damico       | Carlos    | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7709 | Zabriskie    | Alixandra | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7710 | Frederickson | Dallin    | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7711 | Jacobsen     | Kellimae  | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7712 | Lukes        | Keith     | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7737 | Crook        | Jed       | 8/1/2022   | 7/31/2023 | 01/02/2023 |
    | 7738 | Davidson     | Kate      | 8/1/2022   | 2/21/2023 | 01/02/2023 |

!!! note "Style"
    The [EdOps SQL Style Guide](../sqlstyleguide.md) stipulates a few guidelines on how we format our CTEs:

    -   The `WITH` keyword gets its own line
    -   CTEs are named **before** the code (e.g. `WITH s AS (...)`)
    -   The query itself gets indented (as with any code between parentheses)
    -   CTEs should have as _descriptive_ a name as is reasonable, both to hint at their purpose and to guarantee uniqueness

#### Example D

A frequent use of CTEs is in the calculation of _In-Seat Attendance_, since the numerator and denominator both require aggregation.

Recall that **In-Seat Attendance (ISA)** is defined as:

    [Student-Days Present] / [Student-Days Enrolled] * 100

where _[Student-Days Present]_ is the count of present days across the student body, and _[Student-Days Enrolled]_ is the count of enrolled days across same.

Since we need to use these aggregations in our quotient, we will need at least one layer of subquery. Since the subqueries themselves are rather lengthy, for readability it makes more sense to use them as CTEs rather than proper subqueries.

!!! note "When to upgrade from a subquery to a CTE?"
    There's no rule per se, but if generally speaking a subquery should:

    -   Be under 10 lines
    -   Contain no `JOIN`s, `CASE`s, or other complications (an aggregation or two is acceptable, though)

    In short, only the simplest of queries should remain as subqueries; the rest should be made CTEs.

=== "Query"

    ``` sql
    WITH
    DaysEnrolled AS (
    	SELECT s.StudentID,
    		COUNT(cd.ID) AS Days
    	FROM Students s
    	LEFT JOIN Calendar_Day cd ON cd.SchoolID = s.SchoolID
    		AND cd.InSession = 1
    		AND cd.JulianDate BETWEEN s.EntryDateJulian AND s.ExitDateJulian
    	WHERE s.EntryDateJulian >= JULIANDAY('2022-08-01')
    	GROUP BY s.StudentID
    ),
    DaysAbsent AS (
    	SELECT att.StudentID,
    		COUNT(att.ID) AS Days
    	FROM Attendance att
    	LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
    	WHERE att.Att_Mode_Code = 'ATT_ModeDaily'
    		AND attc.Presence_Status_CD = 'Absent'
    		AND att.Att_DateJulian >= JULIANDAY('2022-08-01')
    	GROUP BY att.StudentID
    ),
    DaysPresent AS (
    	SELECT enr.StudentID,
    		enr.Days AS DaysEnrolled,
    		enr.Days - COALESCE(abs.Days, 0) AS DaysPresent
    	FROM DaysEnrolled enr
    	LEFT JOIN DaysAbsent abs ON abs.StudentID = enr.StudentID
    )
    SELECT StudentID,
    	DaysEnrolled,
    	DaysPresent,
    	ISA
    	FROM (SELECT StudentID,
    			DaysEnrolled,
    			DaysPresent,
    			ROUND(DaysPresent * 100.00 / DaysEnrolled, 2) AS ISA
    		FROM DaysPresent
    	) i
    ORDER BY ISA DESC
    ```

=== "Results"

    |StudentID|DaysEnrolled|DaysPresent|ISA  |
    |---------|------------|-----------|-----|
    | 7541 | 259 | 258 | 99.61 |
    | 7294 | 233 | 232 | 99.57 |
    | 7350 | 219 | 218 | 99.54 |
    | 7644 | 203 | 202 | 99.51 |
    | 7462 | 178 | 177 | 99.44 |
    | 7226 | 261 | 259 | 99.23 |
    | 7313 | 261 | 259 | 99.23 |
    | 7333 | 261 | 259 | 99.23 |
    | 7372 | 261 | 259 | 99.23 |
    | 7393 | 261 | 259 | 99.23 |

!!! note "Result Set"

    A _lot_ of students at _Washington Elementary_ have perfect attendance! To make the results more interesting, I filtered those values out. To replicate, add
    ```
    WHERE ISA < 99.62
    ```
    to the end of your query, **before** the `ORDER BY` clause.

!!! note "Negative Attendance"
    The test data set uses **negative attendance**, in which a student is _only marked absent_ and thus records for when the student was present _do not exist_. Therefore, rather than counting student-days present, we will count student-days absent and subtract it from the total days.

!!! warn "Multiple CTEs"
    If you use multiple CTEs, you must separate them with **commas**, with the final CTE lacking a comma.

## Practice

In this practice, you're going to create a **listing of student GPAs**. For our purposes,

    GPA = [sum of grade points] * 1.00 / [count of grades]

Your query should return a listing of each student (`StudentID`, `Last_Name`, `First_Name`) with their SY2023 Q1 GPA, in descending order by GPA. Since you're sorting on a computed column (GPA), you will need, at the very least, a subquery. I recommend structuring your query similarly to [Example D](#example-d), where CTEs are used to:

1.  Pull the relevant data
2.  Perform the aggregation

Finally, the division and rounding can be performed in a subquery, or in a CTE that gets referenced in your final `SELECT` statement. Should you go this route, some hints about structure:

### 1. Pull the relevant data

With the exceptions of student names (which live in the `Students` table), you will be pulling all grade-related data from the `StoredGrades` table. You will want to filter this table for `TermID >= 3200` (i.e. SY2023) and `StoreCode = 'Q1'`.

You will want the following columns:

-   `DCID` - the unique identifier of each grade, which can be used to count the number of grades present
-   `GPA_Points` - the number of grade points associated with that grade

### 2. Perform the aggregation

Once you have pulled your data, you can `COUNT()` the number of grades, and `SUM()` the grade points, resulting in columns which can be divided at the end.

!!! hint "Additional Hint: Division by Zero"

    There are students who have **no stored grades**. For these students, your count of grades will be zero, and thus the GPA will contain a divide-by-zero error. While there are various ways to handle this circumstance, let's take the easy way and filter them out using a `WHERE` clause.

Good luck!

When you're ready to check out the solution, it's [here](./solutions/5-subqueries.md).
