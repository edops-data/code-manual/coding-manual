# Unions and Data Types

Through joins, we have learned to combine data sets **horizontally** - that is, by adding columns while keeping (roughly) the same number of rows. Now, it's time to combine data sets **vertically** - that is, by adding _rows_ while maintaining the same number of columns.

Fundamentally, the process is very simple, requiring one line of code to accomplish. The real challenge lies in making sure that the sets to be combined are compatible, which in turn will lead us into a discussion of **data types**.

By the end of this module, you should be able to write a query that can:

-   Use a `UNION` statement to combine identically-formatted datasets
-   Alter the query to either retain or reject duplicates
-   Convert among the major data types to prepare queries for concatenation

## Video
A video of this module is available on [Google Drive](https://drive.google.com/file/d/1wtooDypyrASgMWt8gJESkxA357U12Tua/view?usp=share_link):
![Module 4 Video Thumbnail](./images/Module4_VideoThumbnail.png)

## Unions

A **union** allows us to stack two datasets on top of each other, which is particularly useful for combining datasets from multiple sources into a single, mega-set of data.

Uniting datasets can only be done if the sets are **identically structured**. In particular, corresponding columns must be:

-   Named identically
-   Of the same data type
-   In the same order

Assuming the above are true, the data sets can be united with one line, and one word: `UNION`.

!!! note "What about `UNION ALL`?"

    We actually very seldom use `UNION` on its own, preferring its cousin `UNION ALL`. We'll look at why in a [moment](#allowing-or-suppressing-duplicates).

### Uniting two data sets

A common problem in PowerSchool (from which our training database is built) is that student enrollments come from two sources:

-   Current enrollments are stored in the `Students` table, while
-   Past enrollments are stored in the `ReEnrollments` table.

Making matters slightly more complicated, the two tables don't use the same syntax for all of the columns. For example, the `TransferComment` column in the `Students` table is (more appropriately) named `EntryComment` in the `ReEnrollments` table.

Let's combine these two tables into a mega-table containing all student enrollments. The columns we're interested in are:

| Contents | Name in `Students` | Data Type in `Students` | Name in `ReEnrollments` | Data type in `ReEnrollments` |
| --- | --- | --- | --- | --- |
| Student ID | `StudentID` | Numeric | `StudentID` | Numeric |
| School ID | `SchoolID` | Numeric | `SchoolID` | Numeric |
| Entry Date | `EntryDate` | Date | `EntryDate` | Date |
| Entry Code | `EntryCode` | Text | `EntryCode` | Text |
| Entry Comment | `TransferComment` | Text | `EntryComment` | Text |
| Exit Date | `ExitDate` | Date | `ExitDate` | Date |
| Exit Code | `ExitCode` | Text | `ExitCode` | Text |
| Exit Comment | `ExitComment` | Text | `ExitComment` | Text |

Fortunately for us, a quick inspection shows that the columns of interest have the same data types in both the `Students` and `ReEnrollments` tables. They don't, however, have the same names, so we'll have to do some aliasing. Easy enough.

#### Example A

=== "Query"

    ``` sql
    SELECT StudentID,
    	SchoolID,
    	EntryDate,
    	EntryCode,
    	TransferComment AS EntryComment,
    	ExitDate,
    	ExitCode,
    	ExitComment
    FROM Students
    UNION
    SELECT StudentID,
    	SchoolID,
    	EntryDate,
    	EntryCode,
    	EntryComment,
    	ExitDate,
    	ExitCode,
    	ExitComment
    FROM ReEnrollments
    ```

=== "Results"

    | StudentID | SchoolID | EntryDate | EntryCode | EntryComment | ExitDate  | ExitCode | ExitComment |
    |-----------|----------|-----------|-----------|--------------|-----------|----------|-------------|
    | 7223 | 700 | 7/10/2023  |    |          | 7/31/2023  |    |                     |
    | 7380 | 700 | 7/12/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7631 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7639 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7640 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7656 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7658 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7659 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7667 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7682 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7691 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | 7703 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |
    | 7704 | 700 | 8/1/2022   |    |          | 11/27/2022 | W1 |                     |
    | 7705 | 700 | 12/14/2022 |    |          | 7/31/2023  |    |                     |
    | 7706 | 700 | 12/20/2022 |    |          | 7/31/2023  |    |                     |
    | 7707 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |
    | 7708 | 700 | 8/21/2022  |    |          | 9/11/2022  | W1 |                     |
    | 7709 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |
    | 7710 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |
    | 7711 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |

!!! note "Result Set"

    To obtain this result set, I had to sort and combine several other result sets. Don't bother attempting.

It's hard to tell at first glance, but this table now contains the students' current enrollments (those with `ExitDate`s after `8/1/2022`) from the `Students` table, as well as their past enrollments from the `ReEnrollments` table.

### Static Columns

Technically, this belongs under the [Data Types](#data-types) section, but now's as good a time as any to introduce it.

For our own reference, it might come in handy to identify from which table a record originates. Since the tables don't have their own `TableName` column, we'll have to create our own, which is easy enough since it's possible to alias a static value as its own column:

#### Example B

=== "Query"

    ``` sql
    SELECT 'Students' AS SourceTable,
      StudentID,
    	SchoolID,
    	EntryDate,
    	EntryCode,
    	TransferComment AS EntryComment,
    	ExitDate,
    	ExitCode,
    	ExitComment
    FROM Students
    UNION
    SELECT 'ReEnrollments' AS SourceTable,
      StudentID,
    	SchoolID,
    	EntryDate,
    	EntryCode,
    	EntryComment,
    	ExitDate,
    	ExitCode,
    	ExitComment
    FROM ReEnrollments
    ```

=== "Results"

    | SourceTable   | StudentID | SchoolID | EntryDate | EntryCode | EntryComment | ExitDate  | ExitCode | ExitComment |
    |---------------|-----------|----------|-----------|-----------|--------------|-----------|----------|-------------|
    | Students      | 7223 | 700 | 7/10/2023  |    |          | 7/31/2023  |    |                     |
    | ReEnrollments | 7380 | 700 | 7/12/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7631 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7639 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7640 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7656 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7658 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7659 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7667 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7682 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | ReEnrollments | 7691 | 700 | 7/14/2017  | R2 | promoted | 7/29/2018  | PR | Promote Same School |
    | Students      | 7703 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |
    | Students      | 7704 | 700 | 8/1/2022   |    |          | 11/27/2022 | W1 |                     |
    | Students      | 7705 | 700 | 12/14/2022 |    |          | 7/31/2023  |    |                     |
    | Students      | 7706 | 700 | 12/20/2022 |    |          | 7/31/2023  |    |                     |
    | Students      | 7707 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |
    | Students      | 7708 | 700 | 8/21/2022  |    |          | 9/11/2022  | W1 |                     |
    | Students      | 7709 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |
    | Students      | 7710 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |
    | Students      | 7711 | 700 | 8/1/2022   |    |          | 7/31/2023  |    |                     |

!!! note "Result Set"

    To obtain this result set, I had to sort and combine several other result sets. Don't bother attempting.

!!! note "Table Names"

    Henceforth, though, we're going to eschew flagging the source table. You'll see why in a moment.

### Allowing or Suppressing Duplicates

As alluded to earlier, there are actually two ways to unite tables: `UNION` and `UNION ALL`. The difference is straightforward:

-   `UNION` removes any duplicate rows after the tables are united
-   `UNION ALL` doesn't

In practical terms, more often than not the tables we're uniting are fully disjoint (i.e. sharing no rows) to begin with: if we could obtain the same data in both tables, we could get away with a single query and there would be no need for a `UNION` in the first place. Additionally, deduplicating datasets is one of the more computationally-intense tasks that a SQL engine has to perform, so using `UNION` can add significant performance delays to your queries.

> **Moral of the story**: 99%+ of the time `UNION ALL` will do you just fine.

Indeed, henceforth we will use `UNION ALL` exclusively.

!!! note "Table Names"

    Incidentally, this is why we're eschewing table names, since adding them necessarily makes otherwise-identical rows distinct.

## Data Types

!!! warn "SQLite"

    As it happens, SQLite is a really poor choice of database engine for this part of the lesson, since it does its absolute best to handle data type conflicts for you. When presented with two conflicting data types, SQLite will cast them to whatever type is most compatible - even if that type matches neither of the sources' types. This friendly behavior has its perks (the DRT relies heavily on it), but can get you into major trouble as well. Most database engines, when faced with a data type conflict, will simply throw an error.

We saw data types in passing way back in [Module 1](./1-basics.md), when we mentioned that, for a set of data to be a database, every column must have one and only one **data type**. And we left it at that, without discussion of what data types are available or how they interact.

Well, now's the time.

There are 5 principal data types that you should be familiar with:

| Data Type | Common Names | Properties | Example |
| --- | --- | --- | --- |
| Text | `Varchar`, `Text` | String of characters, usually up to ~2000 characters in length | `Students.Last_Name` |
| Integer | `Integer`, `Int` | Whole numbers, positive or negative. Occasionally limited to 10 digits (check your architecture) | `Students.StudentID` |
| Float | `Numeric`, `Float` | Numbers with a decimal point. Note that `10` (integer) and `10.0` (float) are not necessarily considered equal by all databases | `StoredGrades.GPA_Points` |
| Boolean | `Boolean` | Only acceptable values are `true`/`false` | `Calendar_Day.InSession` |
| Date (+/-Time) | `Date`, `Timestamp` | Dates, or dates with times, usually down to the millisecond | `Students.EntryDate` |


!!! warn "SQLite and Booleans"

    SQLite, for one, doesn't have a true Boolean data type. Instead, it stores Booleans as integers, using `0` for `False` and `1` for `True`. Where possible, though, it's better to use a proper Boolean data type.

### Casting, and a Warning
The act of converting data from one type to another is called **casting**. Most database architectures have a `CAST` function that accomplishes exactly that, using the syntax

``` sql
CAST([column] AS [type])
```

In addition, most architectures have more nuanced casting functions that allow you to specify how the data is formatted, and what parts, if any, are discarded in the process.

!!! warn "Different Architectures"

    Every database architecture has its own way of storing data, and thus its own way of casting data. The examples that follow are intended to work with our SQLite training database. In general, the same syntax will work with most other architectures, but know that you will probably find yourself googling the specific syntax for whatever architecture you're on.

    Best, then, to treat this section as more a theoretical exercise than any kind of practical knowledge.

### Casting to Text
The most reliable way to get different data sets to line up is just to throw them all to text. Is this the most responsible course of action? Hardly. But does it work? By golly, it (usually) does. (In fact, the OSSE ADT queries, unable to be calibrated to every SIS architecture, do exactly this - occasionally with bizarre results.)

There are two ways to cast to text: implicitly, using the `CAST()` function alluded to earlier, and explicitly using the `TO_CHAR()` function. Let's examine both.

The `CAST()` function gives the database engine full discretion over how to render the data as text. For basic things like numbers, there's not a lot to debate, so this works just fine.

#### Example C

The `Period` table defines all of the class periods in a given day. A quick examination of the `Period` column shows that most of the periods are abbreviated as a single number... except when they're not:

| Abbreviation |
| --- |
| 2 |
| 3 |
| 8 |
| P4 |
| P3 |

(Technically, these are all stored as text because the column has to have a single data type, but let's pretend for a moment that they're of different types.)

We want to make sure that all of these abbreviations are of the same data type. We can't cast letters to numbers, so we have no choice but to cast everybody to text.

=== "Query"

    ``` sql
    SELECT ID,
      CAST(Abbreviation AS Text) AS Abbreviation,
      Name
    FROM Period
    ```

=== "Results"

    | ID   | Abbreviation | Name     |
    |------|--------------|----------|
    | 1554 | P1 | Period 1 |
    | 1555 | P2 | Period 2 |
    | 1557 | P4 | Period 4 |
    | 1566 | 3  | Period 3 |
    | 1567 | 4  | Period 4 |
    | 1565 | 2  | Period 2 |
    | 1569 | 6  | Period 6 |
    | 1556 | P3 | Period 3 |
    | 1570 | 7  | Period 7 |
    | 1564 | 1  | Period 1 |

!!! note "Result Set"

    The default result set is less interesting than I'd like, so I added some filters. To replicate this behavior, add the line
    ```
    WHERE year_id = 22 AND schoolid IN (300, 700)
    ```
    to the end of your query.

!!! note "Aliases"

    `CAST()` is a function, which means we're obligated to alias its output with a human-friendly name.

#### Example D

More often that not, though, it's **dates** that you will be converting to text. Since there are so many differing conventions for how dates get represented, a mere `CAST()` call is inadequate. We need a more robust function that lets us specify a **mask** for how the data will be formatted. For that, we have the `TO_CHAR()` function.

!!! note "SQLite"

    Unfortunately, SQLite stores dates in an oddball manner, so we can't test this (otherwise very common) function in our training DB. What follows is how it would look if run in any other architecture. You can try it in PostgreSQL at [https://www.db-fiddle.com/f/4jyoMCicNSZpjMt4jFYoz5/3528](https://www.db-fiddle.com/f/4jyoMCicNSZpjMt4jFYoz5/3528).

=== "Query"

    ```sql
    SELECT EntryDate,
        TO_CHAR(EntryDate, 'YYYY-MM-DD') AS ISODate,
        TO_CHAR(EntryDate, 'DD/MM/YYYY') AS EUDate,
        TO_CHAR(EntryDate, 'DAY MONTH DD, YYYY') AS LongDate
    FROM ReEnrollments
    ```

=== "Results"

    |  ENTRYDATE |   ISODATE  |   EUDATE   |        LONGDATE        |
    |:----------:|:----------:|:----------:|:----------------------:|
    | 05/26/2013 | 2013-05-26 | 26/05/2013 | SUNDAY MAY 26, 2013    |
    | 05/27/2014 | 2014-05-27 | 27/05/2014 | TUESDAY MAY 27, 2014   |
    | 05/27/2015 | 2015-05-27 | 27/05/2015 | WEDNESDAY MAY 27, 2015 |
    | 05/26/2013 | 2013-05-26 | 26/05/2013 | SUNDAY MAY 26, 2013    |
    | 05/27/2014 | 2014-05-27 | 27/05/2014 | TUESDAY MAY 27, 2014   |
    | 05/27/2015 | 2015-05-27 | 27/05/2015 | WEDNESDAY MAY 27, 2015 |
    | 05/26/2013 | 2013-05-26 | 26/05/2013 | SUNDAY MAY 26, 2013    |
    | 05/27/2014 | 2014-05-27 | 27/05/2014 | TUESDAY MAY 27, 2014   |
    | 05/27/2015 | 2015-05-27 | 27/05/2015 | WEDNESDAY MAY 27, 2015 |
    | 05/26/2013 | 2013-05-26 | 26/05/2013 | SUNDAY MAY 26, 2013    |

### Back and Forth with Booleans

The other major data type that frequently needs to be cast is the Boolean, simply because `True`/`False` values have a tendency to be stored in all manner of syntaxes:

-   `Y`/`N`
-   `Yes`/`No`
-   `1`/`0`
-   `True`/`False` (as words)
-   `True`/`False` (as a Boolean)


There are multiple ways to cast **from** a Boolean, depending on your database architecture.

#### Example E

The first - and most compact way to convert a Boolean to either a number or text is to use the aforementioned `CAST()` function.

!!! note "SQLite"

    Unfortunately, neither SQLite nor Oracle has an inbuilt Boolean type - instead, Booleans are converted immediately to `0`/`1`. Therefore, this example will come from PostgreSQL. However, you can play with this exact data at [here](https://www.db-fiddle.com/f/4jyoMCicNSZpjMt4jFYoz5/3528)

=== "Query"

    ```sql
    SELECT DISTINCT SPED_Indicator,
    	CAST(SPED_Indicator AS int) AS SPED_Indicator_INT,
    	CAST(SPED_Indicator AS text) AS SPED_Indicator_Text
    FROM SpecialEducation
    ```

=== "Results"

    | sped_indicator | sped_indicator_int | sped_indicator_text |
    |:----------------:|----------------------|-----------------------|
    | false            | 0                    | "false"               |
    | true             | 1                    | "true"                |

#### Example F

Less compact - but better supported - is the use of a `CASE` statement, which we covered in [Module 3](./3-aliases-cases-nulls.md#cases). This method is supported in SQLite, so we can resume use of our Training DB.

=== "Query"

    ```sql
    SELECT DISTINCT EL_Indicator,
    CASE
    	WHEN EL_Indicator = 1 THEN 'Yes'
    	WHEN EL_Indicator = 0 THEN 'No'
    	ELSE NULL
    END AS EL_Indicator_Text
    FROM Students
    ```

=== "Results"

    | EL_Indicator | EL_Indicator_Text |
    |----------------|---------------------|
    | 1              | Yes                 |
    | 0              | No                  |


#### Example G

In PostgreSQL or other architectures with a proper Boolean datatype, [Example F](#example-f) gets modified slightly (again, you can experiment [here](https://www.db-fiddle.com/f/4jyoMCicNSZpjMt4jFYoz5/3528)):

=== "Query"

    ```sql
    SELECT DISTINCT SPED_Indicator,
    	CASE
    		WHEN SPED_Indicator = True THEN 'Yes'
    		WHEN SPED_Indicator = False THEN 'No'
    		ELSE NULL
    	END AS SPED_Indicator_Text
    FROM SpecialEducation
    ```

=== "Results"

    | SPED_Indicator | SPED_Indicator_Text |
    |----------------|---------------------|
    | false            | No                 |
    | [null]           | [null]             |
    | true             | Yes                |


!!! note "Can Booleans be `NULL`?"

    They sure can! That's why it's so important to distinguish between a _negative_ value and a _missing_ value in your code.

#### Example H

Although you're far more likely to convert data _from_ Booleans rather than _to_ them, it's certainly possible - at least in architectures that support proper Boolean datatypes (e.g. PostgreSQL).  The syntax looks a bit weird, though, so let's analyze it in some detail.

Suppose `AtRisk_Indicator` has a value of `Yes`. Then the statement

```sql
AtRisk_Indicator = 'Yes'
```

is _True_. Inversely, the statement

```sql
AtRisk_Indicator = 'No'
```

is _False_.

So, we can assign a Boolean variable to capture the **truth** of the statement, rather than the value of the column. Try it at [https://www.db-fiddle.com/f/4jyoMCicNSZpjMt4jFYoz5/3528](https://www.db-fiddle.com/f/4jyoMCicNSZpjMt4jFYoz5/3528).

=== "Query"

    ```sql
    SELECT DISTINCT AtRisk_Indicator,
    	(AtRisk_Indicator = 'Yes') AS AtRisk_Indicator_Bool
    FROM AtRisk
    ```

=== "Results"

    | AtRisk_Indicator | AtRisk_Indicator_Bool |
    |------------------|-----------------------|
    | "No"             | false                 |
    | [null]           | [null]                |
    | "Yes"            | true                  |

## Practice

In this practice, you're going to create a **consolidated listing of student course grades**.

In our training database, grades are stored in two locations:

-   The `PGFinalGrades` table, which contains the grades for the current year, _as they appear in the teacher's gradebook_
-   The `StoredGrades` table, which contains the grades for this and previous years, _as they appeared at the time of storage_

You must unite these tables into a single listing, consisting of the columns:

| Alias | Description | `PGFinalGrades` Name | `StoredGrades` Name |
| --- | --- | --- | --- |
| `StudentID` | The Student's internal ID | `StudentID` | `StudentID` |
| `Course_Number` | The course to which the grade belongs | Not present. You will need to join the `Sections` table using `SectionID` to retrieve `Course_Number` | `Course_Number` |
| `GradingTerm` | The quarter in which the grade was earned | `FinalGradeName` | `StoreCode` |
| `Percent` | The percent earned in the course | `Percent` | `Percent` |
| `Grade` | The letter grade corresponding to `Percent` | `Grade` | `Grade` |
| `Passing` | Whether the grade counts toward course mastery | `Passing` | `EarnedCrHrs` |

Additionally, the `Passing` column should be represented as **text**, specifically the values `Pass`/`Fail`, corresponding to the existing values of `1`/`0`. Finally, any records where `Grade` is missing (represented either by a `NULL` or by `--`) should be **excluded**.

This is a complex ask, so here are some hints and tips.

### 1. Prep the queries separately, sans casting and filtering
First, construct the appropriate query on the `StoredGrades` table, which will involve

-   Selecting the appropriate columns
-   Aliasing the `StoreCode` column

Then, set that query aside and construct the one for the `PGFinalGrades` table, which will involve

-   Selecting the appropriate columns
-   Joining the `Sections` table to obtain the `Course_Number` column
-   Aliasing the `FinalGradeName` column

### 2. Construct the `Passing` column
For each of your two queries, add the code that casts `Passing` into text. My advice? Use a `CASE` statement, which you can review in [Module 3](./3-aliases-cases-nulls.md#cases)

### 3. Filter out missing grades
For each of your two queries, add the clause that removes missing grades. This is best done with a `WHERE` clause, which you can review in [Module 2](./2-aggregation.md#filtering-by-a-single-criterion). The clause will look slightly different for each query, since missing grades are coded differently in the two source tables.

### 4. Unite the queries
Use a `UNION` statement to combine your two queries. You **will** want to exclude duplicate values, since grades for the current year will appear in both tables.

Good luck!

When you're ready to check out the solution, it's [here](./solutions/4-unions-datatypes.md).
