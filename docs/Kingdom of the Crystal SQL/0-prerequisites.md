# Prerequisites
There are a couple things you'll want to accomplish prior to the first lesson.

## DBeaver
DBeaver is poorly-named, but highly-useful software for accessing a number of different database types. We will use it to query our sample database.

### Installation via the Microsoft Store

1.  Navigate to [https://www.microsoft.com/store/apps/9PNKDR50694P](https://www.microsoft.com/store/apps/9PNKDR50694P).
2.  Click on **Get**, which should open the Microsoft Store on your machine.
3.  Click on **Install**.

### Installation via Download

1.  Navigate to [https://dbeaver.io/download/](https://dbeaver.io/download/)
2.  Click on **Windows 64-bit Installer**.
3.  Open the resultant file. Accept all defaults.

## TrainingDB
**TrainingDB** is the database that we will use for this course. [Download](./assets/trainingdb.sqlite) it and save somewhere convenient.

### Connecting DBeaver to the TrainingDB

1.  Open DBeaver.
2.  Navigate to **Database | New Database Connection**.
3.  Select **SQLite** and click **Next**.
4.  Click **Browse** and locate `TrainingDB.sqlite` where you saved it.
5.  Click **Finish**.
