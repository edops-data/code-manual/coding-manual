# Aliases, Cases, and the Joys of `NULL`s

This module covers three topics, though the first two are rather brief. In particular, we will be looking at

-   How to use **aliases** to rename columns in our queries,
-   How to use **cases** to handle logic conditions, and
-   What `NULL` values are, how to address or suppress them, and what information they can convey.

By the end of this module, you should be able to write a query that can:

-   Include identically-named columns from multiple tables,
-   Convert among differing syntaxes for a Boolean variable,
-   Identify missing records,
-   Reformat `NULL` values, and
-   Suppress `NULL` values using a `WHERE` clause, an `INNER JOIN`, or a combination of same

## Video
A video of this module is available on [Google Drive](https://drive.google.com/file/d/1I5cmPOj1dcD40XwtZMp6rL7BW-0MXyVg/view?usp=share_link):
![Module 3 Video Thumbnail](./images/Module3_VideoThumbnail.png)

## Aliases

You're already familiar with the concept of an **alias** from past modules. Whenever we joined two tables, we would give each table a one-letter abbreviation to make our code less cumbersome. For example, in the query

``` sql
SELECT s.StudentID,
  s.Last_Name,
  s.First_Name,
  r.RaceCD
FROM Students s
LEFT JOIN StudentRace r ON r.StudentID = s.ID
```

we aliased the `Students` table as `s` and the `StudentRace` table as `r` when the tables were called, and employed those aliases as prefixes to the column names so that the parser knew, unambiguously, from which table each column should be pulled.

So far, however, we have never aliased a _column_, which is the focus here.

### Aliasing a Column

The only difference between aliasing a column and aliasing a table is that, to alias a column, you need to include the word `AS` before your alias. Other than that, the same rules apply, namely:

-   Aliases **cannot** start with a number.
-   Aliases **cannot** contain a space.
-   Aliases must be **unique** to a query.
-   While tables and columns _can_ share an alias, it's considered poor form and should be avoided.

Way back in [Module 1, Example D](./1-basics.md#example-d), we pulled all of the contacts for each student at _Washington Elementary_ using the query below:

``` sql
SELECT s.StudentID,
  s.Last_Name,
  s.First_Name,
  s.Grade_Level,
  p.LastName,
  p.FirstName,
  e.EmailAddress
FROM Students s
LEFT JOIN StudentContactAssoc sca ON sca.StudentID = s.StudentID
LEFT JOIN Person p ON p.ID = sca.PersonID
LEFT JOIN PersonEmailAddressAssoc peaa ON peaa.PersonID = p.ID
LEFT JOIN EmailAddress e ON e.EmailAddressID = peaa.EmailAddressID
```

Strictly speaking, this query does **not** require aliases, because the columns `Last_Name` (i.e. the _student's_ last name) and `LastName` (i.e. the _contact's_ last name) are technically distinct from one another. However, this remains highly confusing to the end user, so we should alias those columns to be less ambiguous.

#### Example A

=== "Query"

    ``` sql
    SELECT s.StudentID,
      s.Last_Name AS StudentLastName,
      s.First_Name AS StudentFirstName,
      s.Grade_Level,
      p.LastName AS ContactLastName,
      p.FirstName AS ContactFirstName,
      e.EmailAddress
    FROM Students s
    LEFT JOIN StudentContactAssoc sca ON sca.StudentID = s.StudentID
    LEFT JOIN Person p ON p.ID = sca.PersonID
    LEFT JOIN PersonEmailAddressAssoc peaa ON peaa.PersonID = p.ID
    LEFT JOIN EmailAddress e ON e.EmailAddressID = peaa.EmailAddressID
    ```

=== "Results"

    | Student_Number | StudentLastName | StudentFirstName | Grade_Level | ContactLastName | ContactFirstName | EMAILADDRESS |
    |----------------|-----------|------------|-------------|----------|-----------|----------------------------------|
    | 7223      | Subway    | Michael    | 0           | Subway    | Katie    | KatieSubway@powerschool.org     |
    | 7223      | Subway    | Michael    | 0           | Gilbert   | Subway   | SubwayGilbert@powerschool.org   |
    | 7223      | Subway    | Michael    | 0           | Roberts   | Brady    | BradyRoberts@powerschool.org    |
    | 7703      | Babcock   | Joshua     | 1           | Babcock   | Amanda   | AmandaBabcock@powerschool.org   |
    | 7703      | Babcock   | Joshua     | 1           | Babcock   | Austin   | AustinBabcock@powerschool.org   |
    | 7703      | Babcock   | Joshua     | 1           | Shobe     | Benjamin | BenjaminShobe@powerschool.org   |
    | 7704      | Sorenson  | Joshua     | 1           | Sorenson  | Chelsea  | ChelseaSorenson@powerschool.org |
    | 7704      | Sorenson  | Joshua     | 1           | Sorenson  | Bradley  | BradleySorenson@powerschool.org |
    | 7704      | Sorenson  | Joshua     | 1           | Pettit    | Erik     | ErikPettit@powerschool.org      |
    | 7705      | Kartchner | Nicholas   | 1           | Kartchner | Kacee    | KaceeKartchner@powerschool.org  |


!!! hint "Capitalization and Underscore Conventions"

    The [EdOps SQL Style Guide](../sqlstyleguide.md) specifies that we are a _Pascal Case_ shop. That is, when possible, column aliases should

    -   Start with a capital letter, followed by lowercase letters
    -   Use capital letters to delineate words
    -   Use capital letters for common abbreviations (e.g. `StudentID`)
    -   Not contain underscores unless strictly necessary

    There's nothing about the database engine that necessitates this convention; we just adopt it for consistency.

We can (and should!) also alias any instance where a column is constructed using a function (e.g. our [aggregate functions](./2-aggregation.md)) or a `CASE` statement (stay tuned!). Consider [Module 2, Example B](./2-aggregation.md#example-b), which used a `COUNT()` function:

``` sql
SELECT Grade_Level,
   COUNT(StudentID)
FROM Students
GROUP BY Grade_Level
```

We really should alias our `COUNT(StudentID)` statement to something more compact, like `StudentCount`. Let's do that.

#### Example B

=== "Query"

    ``` sql
    SELECT Grade_Level,
    	 COUNT(StudentID) AS StudentCount
    FROM Students
    GROUP BY Grade_Level
    ```

=== "Results"

    | Grade_Level | StudentCount |
    |---|---|
    | 0 | 100 |
    | 1 | 97  |
    | 2 | 94  |
    | 3 | 105 |
    | 4 | 88  |
    | 5 | 86  |


!!! warn "Forbidden Names"

    Don't name your columns (or tables) anything that you think could be confused with a reserved word. For example, as much as we'd like to name our `COUNT(Student_Number)` column a simple `Count`, this would conflict with the function by the same name and throw an error.


## Cases

SQL doesn't have a built-in `if/then` architecture. Instead, the nearest function is called `CASE`, which functions similarly.

The skeleton of a `CASE` statement consists of:

-   The opening `CASE` declaration
-   One or more conditions and their outcomes, defined using the structure `WHEN [condition] THEN [outcome]`
-   An optional `ELSE` clause that specifies the default behavior if none of the `WHEN` conditions are met
-   An `END` declaration


### Decoding Text and Returning Text

While there are lots of clever uses for `CASE` statements, the most common is to convert among syntaxes. Suppose, for example, that we wanted to present student genders as the words `Male`, `Female`, and `Non-binary` rather than the `M`, `F`, and `X` used by the database:

#### Example C

=== "Query"

    ``` sql
    SELECT StudentID,
    	Last_Name,
    	First_Name,
    	CASE
    		WHEN Gender = 'M' THEN 'Male'
    		WHEN Gender = 'F' THEN 'Female'
    		WHEN Gender = 'X' THEN 'Non-Binary'
    	END AS Gender
    FROM Students
    ```

=== "Results"

    | StudentID      | Last_Name | First_Name | Gender |
    |----------------|-----------|------------|--------|
    | 7223 | Subway       | Michael   | Male   |
    | 7703 | Babcock      | Joshua    | Male   |
    | 7704 | Sorenson     | Joshua    | Male   |
    | 7705 | Kartchner    | Nicholas  | Male   |
    | 7706 | Adams        | Jade      | Female |
    | 7707 | Damico       | Carlos    | Male   |
    | 7708 | Gardner      | Nathan    | Male   |
    | 7709 | Zabriskie    | Alixandra | Female |
    | 7710 | Frederickson | Dallin    | Male   |
    | 7711 | Jacobsen     | Kellimae  | Female |


!!! info "Missing `ELSE` clause"

    You may have noticed that we omitted the `ELSE` clause. Though doing so is generally poor form, we're doing it here because, as often as not, the `ELSE` clause returns a `NULL`, and we haven't covered those yet.

!!! hint "Capitalization and Underscore Conventions"

    The [EdOps SQL Style Guide](../sqlstyleguide.md) specifies that, when writing a `CASE` statement,

    -   `CASE` appears on its own line, indented to match the other columns being called
    -   Each `WHEN/THEN` pair is on its own line, indented one step farther than the `CASE` declaration
    -   `END`, plus the mandatory _alias_, appears on their own line, matching the indent of the `CASE` declaration

### Decoding Text and Returning a Column

In another common use case, we will use a `CASE` statement to decode some - but not all - of the possible values for `Grade_Level`.

In our TrainingDB - which is based on PowerSchool - all grade levels are represented numerically. For grades 1-12 this isn't a big deal, but it's a bit confusing when presented with `-2` in place of `PK3`. So a common use of a `CASE` statement is to decode these grades, while passing all other values unaltered.

#### Example D

=== "Query"

    ``` sql
    SELECT StudentID,
    	Last_Name,
    	First_Name,
      CASE
    		WHEN Grade_Level = -2 THEN 'PK3'
    		WHEN Grade_Level = -1 THEN 'PK4'
    		WHEN Grade_Level = 0 THEN 'KG'
    		ELSE Grade_Level
    	END AS GradeLevel
    FROM Students
    ```

=== "Results"

    | StudentID      | Last_Name | First_Name | GradeLevel |
    |----------------|-----------|------------|------------|
    | 7706 | Adams     | Jade     | 1  |
    | 7371 | Adams     | Brandon  | 1  |
    | 7480 | Adams     | Jennifer | 3  |
    | 7500 | Ainsworth | Cole     | 3  |
    | 7726 | Allred    | Charles  | 3  |
    | 7664 | Allred    | Kathleen | 5  |
    | 7260 | Alu       | Matthew  | KG |
    | 7577 | Alu       | Anthony  | 4  |
    | 7389 | Andersen  | Ashley   | 2  |
    | 7430 | Andersen  | Apaulo   | 2  |


!!! note "Result Set"

    The default result set isn't terribly interesting, so I sorted mine by `Last_Name`. To replicate this behavior, add the line
    ```
    ORDER BY Last_Name ASC
    ```
    to the end of your query.


!!! warn "Data Types"

    What we did here is, strictly speaking, not entirely kosher because we're mixing _text_ (e.g. `PK3`) and _numbers_ (e.g. `2`), which violates our database requirement that each column consist of _one and only one_ data type. However, SQLite is friendly, and when it detects a mix of numbers and text it just converts everything to text automatically.

    Most database engines are far less helpful, and you will need to specify a data type outright. However, since we don't know how to do that yet, we will rely on the benevolence of the SQLite engine.


## `NULL`s

A `NULL` data element means that **no data exists** for that element. This simple statement has some major implications, so let's be very clear about what a `NULL` is, and what it's not.

### `NULL`s As Default Values

Recall that a database is a _table_, formed of _rows_ and _columns_. If you don't have information for a particular column for a particular record, you can't just skip it - there's a cell waiting for that data element. Sometimes, the database has a _default value_ established - for example, a student's _Special Education Indicator_ is generally set to be `False` unless it is explicitly set to `True`. Other times, though, there is no default, so when the database looks for a value and finds none, it returns a `NULL`.

Let's look at such an instance.

In our TrainingDB, the `Calendar_Day` table specifies whether school is in session on a given day, and what "type" of day (e.g. half-day, vacation) it is. The `InSession` flag defaults to `0`; that is, a day is assumed not in session until declared otherwise. There is, however, no default value for the day _type_. Therefore, days that have no type specified will have a `NULL` for that column.

#### Example E

=== "Query"

    ```sql
    SELECT Date,
    	InSession,
    	Type
    FROM Calendar_Day
    ```

=== "Results"

    |Date     |InSession|Type|
    |---------|---------|----|
    | 7/1/2020  | 1 | IN |
    | 7/2/2020  | 1 | IN |
    | 7/3/2020  | 1 | IN |
    | 7/4/2020  | 0 | [NULL] |
    | 7/5/2020  | 0 | [NULL] |
    | 7/6/2020  | 1 | IN |
    | 7/7/2020  | 1 | IN |
    | 7/8/2020  | 1 | IN |
    | 7/9/2020  | 1 | IN |
    | 7/10/2020 | 1 | IN |

### `NULL`s As Unavailable Values

Another common way to end up with `NULL` values is to perform a `JOIN` for which some primary keys in the first table don't appear in the second table.

We've seen this before, actually. Consider [Module 1, Example C](./1-basics.md#example-c), which pulled every student's race code:

``` sql
SELECT s.StudentID,
  s.Last_Name,
  s.First_Name,
  s.Grade_Level,
  r.RaceCD
FROM Students s
LEFT JOIN StudentRace r ON r.StudentID = s.StudentID
```

While every student necessarily has a row in the `Students` table, only students who have their races declared will have a row in the `StudentRace` table. For example, if we search the `StudentRace` table for `StudentID = 24961`, we get no rows returned:

#### Example F

=== "Query"

    ``` sql
    SELECT *
    FROM StudentRace
    WHERE StudentID = 24961
    ```

=== "Results"

    | DCID | ID | RaceCD | StudentID |
    | --- | --- | --- | --- |

When we `JOIN` the two tables our student will have a row by virtue of appearing in the `Students` table, but there is no information to join. Therefore, the database returns a `NULL` for all of the joined columns.

| Student_Number | Last_Name | First_Name | Grade_Level | RaceCd |
|----------------|-----------|------------|-------------|--------|
| 7386  | Henderson | Aaron   | 2 | BL |
| 24974 | Barlow    | Aaron   | 2 | [NULL] |
| 7519  | Firkus    | Aaron   | 3 | HI |
| 24988 | Gold      | Abbie   | 4 | [NULL] |
| 7625  | Haukedahl | Abigail | 5 | AM |
| 7617  | Moen      | Adam    | 5 | WH |
| 7395  | Dewey     | Adam    | 2 | WH |
| 7224  | Demie     | Adam    | 0 | HI |
| 7490  | Spencer   | Adam    | 3 | WH |
| 7563  | Dahl      | Adam S  | 4 | WH |

!!! note "Result Set"

    The default result set lacks the nulls we seek, so I sorted mine by `First_Name`. To replicate this behavior, add the line
    ```
    ORDER BY First_Name ASC
    ```
    to the end of your query.

### Gathering `NULL`s

Quite often, we find ourselves auditing a data set for which there is a bunch of missing data - say, missing grades. Rather than sending the school the entire table and make them sort it out, we want to just send them the rows that have data missing - in other words, we only want the records where some particular column is `NULL`.

We know how to filter data using a `WHERE` clause, but statements like

```sql
SELECT s.Last_Name,
  s.First_Name,
  sg.Course_Number,
  sg.Grade
FROM Students s
LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
WHERE sg.Grade = 'A'
```

only work when the grade is _equal to something_. A `NULL`, by contrast, is equal to _nothing_! Therefore, we need a slight modification to our syntax. Specifically, we use the pair of conditions:

-   `IS NULL` and
-   `IS NOT NULL`

accordingly.

#### Example G

=== "Query"

    ``` sql
    SELECT s.Last_Name,
      s.First_Name,
      sg.Course_Number,
      sg.Grade
    FROM Students s
    LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
    WHERE sg.Grade IS NULL
    ```

=== "Results"

    | Last_Name | First_Name | Course_Number | Grade |
    |-----------|------------|---------------|-------|
    | Adams     | Jade     | [NULL] | [NULL] |
    | Adams     | Brandon  | HR     | [NULL] |
    | Adams     | Brandon  | HR     | [NULL] |
    | Adams     | Jennifer | HR     | [NULL] |
    | Adams     | Jennifer | HR     | [NULL] |
    | Ainsworth | Cole     | HR     | [NULL] |
    | Ainsworth | Cole     | HR     | [NULL] |
    | Allred    | Charles  | [NULL] | [NULL] |
    | Allred    | Kathleen | HR     | [NULL] |
    | Allred    | Kathleen | HR     | [NULL] |

!!! note "Result Set"

    The default result set is less interesting than I'd like, so I sorted mine by `Last_Name`. To replicate this behavior, add the line
    ```
    ORDER BY Last_Name ASC
    ```
    to the end of your query.

### Replacing `NULLs`

The data we just pulled actually has an extra `NULL`: look at _Allred, Charles_ who has a `NULL` under `Course_Number`. This means that, unlike the other students who have missing _grades_ for known courses, Charles has _no courses_ associated with him. This is exactly the kind of issue that we want to make stand out, and `NULL`s, while useful, aren't exactly flashy.

Fortunately, there's a function that turns `NULL` values into whatever we want!

The `COALESCE()` function passes, untouched, any values it encounters. However, if it finds a `NULL`, it replaces that `NULL` with whatever data the user specifies.

#### Example H

=== "Query"

    ``` sql
    SELECT s.Last_Name,
      s.First_Name,
      COALESCE(sg.Course_Number, '---MISSING---') AS Course_Number,
      sg.Grade
    FROM Students s
    LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
    WHERE sg.Grade IS NULL
    ```

=== "Results"

    | Last_Name | First_Name | Course_Number | Grade |
    |-----------|------------|---------------|-------|
    | Adams     | Jade     | ---MISSING--- | [NULL] |
    | Adams     | Brandon  | HR            | [NULL] |
    | Adams     | Brandon  | HR            | [NULL] |
    | Adams     | Jennifer | HR            | [NULL] |
    | Adams     | Jennifer | HR            | [NULL] |
    | Ainsworth | Cole     | HR            | [NULL] |
    | Ainsworth | Cole     | HR            | [NULL] |
    | Allred    | Charles  | ---MISSING--- | [NULL] |
    | Allred    | Kathleen | HR            | [NULL] |
    | Allred    | Kathleen | HR            | [NULL] |

!!! note "Aliases"

    Remember, `COALESCE()` is a function, which means its output must be **aliased**.

### Avoiding `NULL`s with an `INNER JOIN`

Up to this point, every join we've made has been a `LEFT JOIN` - and, indeed, probably 95+% of the joins that you will make will be of this type. Recall from earlier that a `LEFT JOIN`:

-   Keeps every row in the original table
-   Returns every available record from the new table, provided a primary-foreign key relationship can be established
-   Leaves `NULL`s where such a relationship cannot be established.

Suppose, though, that we're only interested in rows that appear **in both tables**.  That's what an `INNER JOIN` is for. Specifically, an `INNER JOIN`:

-   Returns every available record from the new table, provided a primary-foreign key relationship can be established
-   **Rejects** any records **from either table** that do not have such a relationship

!!! hint "Beware accidentally suppressed rows!"

    Remember that an `INNER JOIN` only returns rows where the key is present in **both tables**. As a consequence, an `INNER JOIN` can actually return _fewer_ rows than are found in either table! Especially when you're dealing with large datasets, it can be hard to tell when the `INNER JOIN` is overzealously removing too many rows. So be careful!

Consider the query below, which pulls every student's stored grades, as well as their email addresses so that they can be emailed a progress report:

#### Example I

=== "Query"

    ``` sql
    SELECT s.StudentID,
      s.Last_Name,
      s.First_Name,
      email.EmailAddress,
      sg.Course_Number,
      sg.Grade
    FROM Students s
    LEFT JOIN PersonEmailAddressAssoc peaa ON peaa.PersonID = s.Person_ID
    LEFT JOIN EmailAddress email ON email.EmailAddressID = peaa.EmailAddressID
    LEFT JOIN StoredGrades sg ON sg.StudentID = s.StudentID
      AND sg.StoreCode = 'Q1'
    ```

=== "Results"

    | Student_Number | Last_Name | First_Name | EMAILADDRESS                     | Course_Number | Grade |
    |----------------|-----------|------------|----------------------------------|---------------|-------|
    | 7409 | Zuniga    | Trevor  |                                 | EL1000 |   |
    | 7409 | Zuniga    | Trevor  |                                 | EL2000 | C |
    | 7409 | Zuniga    | Trevor  |                                 | EL201  | B |
    | 7409 | Zuniga    | Trevor  |                                 | EL202  | A |
    | 7409 | Zuniga    | Trevor  |                                 | EL203  | A |
    | 7409 | Zuniga    | Trevor  |                                 | EL204  | A |
    | 7409 | Zuniga    | Trevor  |                                 | EL3000 | A |
    | 7409 | Zuniga    | Trevor  |                                 | HR     |   |
    | 7230 | Zubke     | Amanda  | Zubke.Amanda@powerschool.com    |        |   |
    | 7553 | Zubke     | Matthew | Zubke.Matthew@powerschool.com   | EL1000 |   |
    | 7553 | Zubke     | Matthew | Zubke.Matthew@powerschool.com   | EL2000 | A |
    | 7553 | Zubke     | Matthew | Zubke.Matthew@powerschool.com   | EL3000 | A |
    | 7553 | Zubke     | Matthew | Zubke.Matthew@powerschool.com   | EL401  | A |
    | 7553 | Zubke     | Matthew | Zubke.Matthew@powerschool.com   | EL402  | A |
    | 7553 | Zubke     | Matthew | Zubke.Matthew@powerschool.com   | EL403  | A |
    | 7553 | Zubke     | Matthew | Zubke.Matthew@powerschool.com   | EL404  | A |
    | 7553 | Zubke     | Matthew | Zubke.Matthew@powerschool.com   | HR     |   |
    | 7494 | Zachariah | Jacob   | Zachariah.Jacob@powerschool.com | EL1000 |   |
    | 7494 | Zachariah | Jacob   | Zachariah.Jacob@powerschool.com | EL2000 | A |
    | 7494 | Zachariah | Jacob   | Zachariah.Jacob@powerschool.com | EL3000 | A |

!!! note "Result Set"

    The default result set is less interesting than I'd like, so I sorted mine by `Last_Name`. To replicate this behavior, add the line
    ```
    ORDER BY Last_Name DESC
    ```
    to the end of your query.

There are three sets of `NULL`s to contend with:

1.  Students who have no grades at all (e.g. _Amanda Zubke_), and therefore should receive no progress report
2.  Students who have no email address (e.g. _Trevor Zuniga_), and therefore can't receive a progress report
3.  Students who have no grade for a course (e.g. _Homeroom_), and therefore should not receive a notice for that couse

Could we address all three situations with a complicated `WHERE` clause? Sure - incidentally, we _need_ to use a `WHERE` clause for condition (3) because, while the `Grade` entry is null, the `StoredGrades` _record_ actually exists, meaning an `INNER JOIN` would accomplish nothing of use. But for the other two? Way easier to use an `INNER JOIN`.

#### Example J

=== "Query"

    ``` sql
    SELECT s.StudentID,
      s.Last_Name,
      s.First_Name,
      email.EmailAddress,
      sg.Course_Number,
      sg.Grade
    FROM Students s
    INNER JOIN PersonEmailAddressAssoc peaa ON peaa.PersonID = s.Person_ID
    LEFT JOIN EmailAddress email ON email.EmailAddressID = peaa.EmailAddressID
    INNER JOIN StoredGrades sg ON sg.StudentID = s.StudentID
      AND sg.StoreCode = 'Q1'
    WHERE sg.Grade IS NOT NULL
    ```

=== "Results"

    | Student_Number | Last_Name | First_Name | EMAILADDRESS                   | Course_Number | Grade |
    |----------------|-----------|------------|--------------------------------|---------------|-------|
    | 7485 | Kath       | Heidi     | Kath.Heidi@powerschool.com        | EL301  | F |
    | 7491 | Segura     | Kristina  | Segura.Kristina@powerschool.com   | EL301  | A |
    | 7494 | Zachariah  | Jacob     | Zachariah.Jacob@powerschool.com   | EL301  | B |
    | 7495 | Bradshaw   | Charidee  | Bradshaw.Charidee@powerschool.com | EL301  | B |
    | 7498 | Dowdle     | Joshua    | Dowdle.Joshua@powerschool.com     | EL301  | A |
    | 7431 | Mcallister | Alyson    | Mcallister.Alyson@powerschool.com | EL3000 | B |
    | 7432 | Dossert    | Christina | Dossert.Christina@powerschool.com | EL3000 | A |
    | 7436 | Kinman     | Vickie    | Kinman.Vickie@powerschool.com     | EL3000 | B |
    | 7443 | Giedt      | Bryan     | Giedt.Bryan@powerschool.com       | EL3000 | A |
    | 7444 | Kinney     | Michael   | Kinney.Michael@powerschool.com    | EL3000 | B |

## Practice

In this practice, you're going to create a listing of **how many class sections each teacher is assigned** at _Washington Elementary_. To do this, you will use:

#### `Teachers` Table
The `Teachers` table contains, as you would expect, a record for every teacher in the _Apple Grove Unified School District_, which includes _Washington Elementary_. Since we haven't used this table before, you might want to `SELECT *` and explore the data a little bit. However, of the table's 8 columns, the only ones that will appear in your query are

-   `LastFirst` - the teacher's name, and
-   `TeacherID` - the teacher's ID (and the table's primary key)

#### `Sections` Table
The `Sections` table contains a listing of every class section at _Washington Elementary_. Again, since we haven't used this table before, an exploratory `SELECT *` wouldn't go amiss. However, the only two columns we really need are

-   `ID` - the section ID number, and
-   `Teacher` - the foreign key which relates to `Teachers.TeacherID`.

### Requirements
Your task is to create a query which:

-   Contains two columns: `TeacherName` and `SectionCount`, where
    -   `TeacherName` is the teacher's name (duh), and
    -   `SectionCount` is a count of the `Sections.ID` records assigned to that teacher
-   Displays _only_ teachers who are assigned to at least one section

### Hints

-   You can accomplish that last criterion either with an `INNER JOIN` or a `WHERE` clause - it's up to your level of comfort.
    -   If you go the `WHERE` clause route, I would recommend first making a listing of every section assigned to each teacher, prior to generating the count, to make sure that you have your filters correctly placed.
-   Since your query includes a **count**, you may want to review the module on [Aggregation](./2-aggregation.md), in particular how [grouping](./2-aggregation.md#grouping) works.
-   Obviously, there are not columns named `TeacherName` and `SectionCount`. Fortunately, today you learned how to name a column whatever you want.

Good luck!

When you're ready to check out the solution, it's [here](./solutions/3-aliases-cases-nulls.md).
