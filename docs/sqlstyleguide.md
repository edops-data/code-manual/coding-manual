# SQL Style Guide

## Purpose

Reducing duplication of effort is a key Data Team principle.  While there has been great progress toward standardizing how we *report* data, there has been little attention paid to how we *generate* data.

As more schools ask for advanced reporting and analytics, we would be wise to ensure that our code is interoperable among SISes and modular enough that specialists can mix and match code blocks to rapidly create advanced queries.

## Principles

-   Multiple schools request the same information more often than they realize.
-   Individual schools often request the same information in more formats than they realize.
-   We standardized how the SISes display data; now we should standardize how they report data.

## Conventions

### Capitalization

SQL reserved words (`SELECT`, `FROM`, `IN`...) are always in **UPPERCASE**.

Except where required by the source database, tables, columns, and their aliases should be in **PascalCase**, i.e.

-   Initial letter capitalized; capitalize each new word (e.g. `StoredGrades`)
-   No spaces
-   No underscores or dashes (except as required by the source DB)
-   Sequential caps only for common abbreviations (e.g. `StudentID`, `ISA`)

### Naming

Constants should be in **ALL CAPS** for ease of identification.  Because of all-caps, hyphens are permitted for readability.  Everything else is in **PascalCase**.

When dealing with groups of related tables, name them semantically in order of **increasing specificity**.  That is:

-   The modifier with the *fewest* options goes first
-   The modifier with the *most* options goes last
-   All other modifiers ordered semantically

#### Example

When aliasing a couple tables across multiple years:
``` sql
CC2018
CC2017
CC2016
CC2015
PGFinalGrades2018
PGFinalGrades2017
PGFinalGrades2016
PGFinalGrades2015
```
There are fewer tables than years, so `Table Name` precedes `Year`

#### Example

When aliasing multiple tables across a few years:
``` sql
2018Attendance
2018AttendanceCode
2018DaysPresent
2018DaysEnrolled
2017Attendance
2017AttendanceCode
2017DaysPresent
2017DaysEnrolled
```
There are fewer years than tables, to `Year` precedes `Table Name`. Likewise, there are multiple flavors of `Days` so the modifier follows the noun.

### Dates and Datatypes

Dates, like other parameters or constants, should be defined at the top of the query, in **all caps**.

Whenever possible, manipulate data in its native type instead of casting.  Casting forces formatting that could be inconvenient later, and should only be used to render data for display.  Additionally, sort functions behave strangely with text (e.g. `2 < 10` but `'2' > '10'`).

**Good**

``` sql
SELECT COUNT(Student_Number)
FROM Students
WHERE DATE_PART(ExitDate, DP_YEAR) > 2012
```

**Bad**

``` sql
SELECT COUNT(Student_Number)
FROM Students
WHERE TO_CHAR(ExitDate, 'YYYY') > '2012'
```

Stick to the following datatypes and cast functions:

-   `VARCHAR()` and `TO_CHAR()`
-   `DATE()` and `TO_DATE()`
-   `TIMESTAMP()` and `TO_TIMESTAMP()`
-   `NUMBER()` and `TO_NUMBER()`

### Comments

Use only the block comment notation, i.e. `/* comment */`.  Inline notation (i.e. `-- comment`) interferes with the PowerSchool parser, and thus should not be used. Separate the comment from its barrier with a **single space**, as a favor to both readers and syntax highlighters.

Inline comments are acceptable, however, provided they use the block comment notation and do not include line breaks.

Block comments exist at the same indentation as the statment being commented. Multi-line comments *do not* require an indentation for lines 2-*n*, except where sensible (e.g. lists).

**Good**

``` sql
/* This query uses the following extension columns which will likely need
to be changed for your particular school:
  SPED_Indicator
  OSSE_Attending_School */
SELECT Students.LastFirst,
  usf.SPED_Indicator,
  usf.OSSE_Attending_School /* For most schools, Students.EnrollmentCode */
FROM Students
/* For simplicity, keep the usf alias even when changing the extension table. */
INNER JOIN U_StudentsUserFields usf ON usf.StudentsDCID = Students.DCID
/* Since all PS Installations use this table, for simplicity keep it
even if it's not required by your LEA. */
INNER JOIN StudentCoreFields scf ON scf.StudentsDCID = Students.DCID
```

**Bad**

``` sql
/*This query uses the following extension columns which will likely need
to be changed for your particular school:
  SPED_Indicator
  OSSE_Attending_School*/
SELECT Students.LastFirst,
  usf.SPED_Indicator,
  usf.OSSE_Attending_School -- For most schools, Students.EnrollmentCode
FROM Students
  /* For simplicity, keep the usf alias even when changing the extension table. */
INNER JOIN U_StudentsUserFields usf ON usf.StudentsDCID = Students.DCID
INNER JOIN StudentCoreFields scf ON scf.StudentsDCID = Students.DCID /* Since all
  PS Installations use this table, for simplicity keep it
  even if it's not required by your LEA. */
```

## Whitespace

### Spaces

One space between operators and their operands, but not between parentheses and their contents.
Likewise, commas should always be followed by a single space.

**Good**

``` sql
INNER JOIN ReEnrollments ON ReEnrollments.StudentID = Students.ID
  AND DATE_PART(ReEnrollments.EntryDate, DP_MONTH) < 7
WHERE Grade_Level = 99
  OR Grade_Level BETWEEN 3 AND 6
```

**Bad**

``` sql
INNER JOIN ReEnrollments ON ReEnrollments.StudentID=Students.ID
  AND DATE_PART(ReEnrollments.EntryDate,DP_MONTH)<7
WHERE Grade_Level=99
  OR Grade_Level BETWEEN 3 AND 6
```

### Line Breaks

-   Root keywords (i.e. those that define a statement or a clause; e.g. `SELECT`, `FROM`, `WHERE`, `JOIN`) represent the top-level elements in their respective query, get a new line and are *left-aligned*.

-   Operators (e.g. `ON`, `BETWEEN`, `IN`) are *collinear* with their keyword.
-   Conditions (e.g. `AND`, `OR`) get a new line and are *indented*.
-   Indents are **two spaces**.  Tabs are acceptable, provided the editor fixes them to two spaces.  Do not combine tabs and spaces in a line.

**Good**

``` sql
SELECT DISTINCT Attendance_Date
FROM Attendance
INNER JOIN Students ON Students.ID = Attendance.StudentID
WHERE YearID >= 2700
  AND SchoolID = 163
```

**Bad**

``` sql
SELECT DISTINCT Attendance_Date
FROM Attendance
  INNER JOIN Students
    ON Students.ID = Attendance.StudentID
WHERE YearID >= 27 AND SchoolID = 163
```

### Alignment

Text is always **left-aligned**.  Rivers are a pain to keep consistent and not worth the trouble.

Indent only to match the appropriate number of levels, and **no further**.  Odds are that means the first argument won't align with subsequent arguments. That's okay.  Indents should match the **level**, not the **keyword**.

**Good**
```sql
SELECT Students.LastFirst,
  Students.Grade_Level,
  COUNT(Attendance.Attendance_Date)
FROM Students
INNER JOIN Attendance ON Attendance.StudentID = Student.ID
  AND Attendance.Attendance_Date BETWEEN EntryDate AND ExitDate
WHERE Student.SchoolID = 163
  AND Attendance.ATT_Mode_Code = 'ATT_ModeDaily'
```
**Bad**
```sql
SELECT Students.LastFirst,
       Students.Grade_Level,
       COUNT(Attendance.Attendance_Date)
FROM Students
INNER JOIN Attendance ON Attendance.StudentID = Student.ID
           AND Attendance.Attendance_Date BETWEEN EntryDate AND ExitDate
WHERE Student.SchoolID = 163
      AND Attendance.ATT_Mode_Code = 'ATT_ModeDaily'
```
Any organization gained in the `SELECT` clause is immediately lost in the `JOIN` and `WHERE` clauses.

### Width

Lines should not exceed 90 characters.  When breaking a line, break at either a:

-   comma
-   keyword
-   parenthesis

**Good**
``` sql
SELECT LastFirst,
  Grade_Level,
  U_StudentsUserFields.SPED_Indicator,
  MAX(Attendance.Attendance_Date) OVER (
    PARTITION BY Students.Grade_Level,
      U_StudentsUserFields.SPED_Indicator
    ORDER BY LastFirst ASC
  ) AS LastAttendance,
  SUM(IFF(Attendance_Code.Present = 'Present', 1, 0))
  OVER (
    PARTITION BY Students.Grade_Level,
      U_StudentsUserFields.SPED_Indicator
    ORDER BY LastFirst ASC
  ) AS DaysPresent
```

**Bad**
``` sql
SELECT LastFirst,
  Grade_Level,
  U_StudentsUserFields.SPED_Indicator,
  MAX(Attendance.Attendance_Date) OVER (PARTITION BY
    Students.Grade_Level,
    U_StudentsUserFields.SPED_Indicator ORDER BY
      LastFirst ASC) AS LastAttendance,
  SUM(IFF(Attendance_Code.Present = 'Present', 1, 0)) OVER
    (PARTITION BY Students.Grade_Level, U_StudentsUserFields.SPED_Indicator
      ORDER BY LastFirst ASC) AS DaysPresent
```

## Grouping / Placement

### Commas
Commas should **follow** list items.

**Good**

``` sql
SELECT Student.ID,
  LastFirst,
  EntryDate,
  SchoolID
FROM Students
ORDER BY SchoolID ASC,
  LastFirst ASC
```

**Bad**

``` sql
SELECT Student.ID
  , LastFirst
  , EntryDate
FROM Students
ORDER BY SchoolID ASC
  , LastFirst ASC
```

### Subqueries

Subqueries are allowed; however, if you think that your subquery could be reappropriated elsewhere, consider using a Common Table Expression for ease of use.

The first line of a subquery consists of:

-   The clause introducing the subquery
-   The open parenethesis

The last line of a subquery consists of:
-   The last clause/line of the subquery itself
-   The close parenthesis
-   The alias

**Good**

``` sql
INNER JOIN (
  SELECT a,
    b,
    c
  FROM d
  WHERE e > 0) AS Subquery
```

**Bad**

``` sql
INNER JOIN (SELECT
    a,
    b,
    c
  FROM d
  WHERE e > 0
) AS Subquery
INNER JOIN
  (SELECT a,
    b
  FROM c
  WHERE d > 0
  ) AS OtherSubquery
```

In short, the subquery should be transplantable without breaking indentation.

### Naked Keywords

Keywords should have their **first argument** on the same line.  Arguments 2 through *n* appear on their own lines.

**Good**

``` sql
SELECT Students.LastFirst,
  Students.Grade_Level,
  Students.TransferComment AS EntryComment,
  COUNT(Attendance.Attendance_Date)
FROM Students
INNER JOIN Attendance ON Attendance.StudentID = Student.ID
WHERE Attendance.YearID >= 27
  AND Student.SchoolID = 163
  AND Attendance.ATT_Mode_Code = 'ATT_ModeDaily'
```

**Bad**

``` sql
SELECT
  Students.LastFirst,
  Students.Grade_Level,
  Students.TransferComment tc,
  COUNT(Attendance.Attendance_Date)
FROM Students
INNER JOIN Attendance ON Attendance.StudentID = Student.ID
WHERE
  Attendance.YearID >= 27
  AND Student.SchoolID = 163
  AND Attendance.ATT_Mode_Code = 'ATT_ModeDaily'
```

## Explicitness

### Joins

Always declare `INNER JOIN` and `LEFT JOIN` explicitly - **never** use `JOIN` on its own.  `RIGHT JOIN` is to be avoided.

Use `LEFT JOIN` whenever possible.  Group `INNER JOIN`s first, followed by `LEFT JOIN`s, grouped semantically.
Wherever possible, place restrictions on a join in the `WHERE` clause.

**Good**

``` sql
/* Assignments using nonstandard Teacher Categories */
SELECT Sections.Teacher
  ASec.Name,
  TCat.Name
FROM Sections
/* Eliminate deactivated sections */
INNER JOIN AssignmentSection AS ASec ON ASec.SectionsDCID = Sections.DCID
LEFT JOIN Assignment ON Assignment.AssignmentID = ASec.AssignmentID
LEFT JOIN AssignmentCategoryAssoc AS ACat
  ON ACat.AssignmentSectionID = ASec.AssignmentSectionID
LEFT JOIN TeacherCategory AS TCat.TeacherCategoryID = ACat.TeacherCategoryID
LEFT JOIN DistrictTeacherCategory AS DCat
  ON DCat.DistrictTeacherCategoryID = TCat.DistrictTeacherCategoryID
WHERE Assignment.YearID = 28
  AND DCat.Name IS NULL /* Keep only teacher categories */
```

**Bad**

``` sql
/* Assignments using nonstandard Teacher Categories */
SELECT Sections.Teacher
  ASec.Name,
  TCat.Name
FROM TeacherCategory AS TCat
LEFT JOIN DistrictTeacherCategory AS DCat
  ON DCat.DistrictTeacherCategoryID = TCat.DistrictTeacherCategoryID
INNER JOIN AssignmentCategoryAssoc AS ACat
  ON ACat.TeacherCategoryID = TeacherCategory.TeacherCategoryID
LEFT JOIN AssignmentSection AS ASec ON ASec.AssignmentSectionID = ACat.AssignmentSectionID
/* Eliminate Deactivated Sections with Inner Join*/
INNER JOIN Sections ON Sections.DCID = ASec.SectionsDCID
LEFT JOIN Assignment ON Assignment.AssignmentID = ASec.AssignmentID
  AND Assignment.YearID = 28
```

### Aliases

There are only two instances when a table or column needs to be aliased:

-   To maintain uniqueness (i.e. when a table is joined twice)
-   To shorten the name

While there is no hard rule for when a name needs to be shortened, anything under ~10 characters probably doesn't require it.

If the source DB has a table/column name that is not compliant with our case conventions (e.g. `Attendance_Code`), there is **no need** to alias.

Outputs of functions (e.g. `SUM()`) or `CASE` statements should **always** be aliased.

Aliases should

-   Respect our case conventions
-   **Always** include the `AS` keyword, since doing so makes aliases more visible due to syntax highlighting.

**Good**

``` sql
SELECT SCF.c_504_flag,
  Students.LastFirst,
  COUNT(Att.Attendance_Date) AS EnrolledDays
FROM Students
INNER JOIN StudentCoreFields AS SCF ON SCF.StudentsDCID = Students.DCID
INNER JOIN Attendnace AS Att ON Att.StudentID = Students.ID
```

**Bad**

``` sql
SELECT scf.c_504_flag,
  s.LastFirst,
  COUNT(Attendance_Date)
FROM Students s
INNER JOIN StudentCoreFields scf ON scf.StudentsDCID = s.DCID
INNER JOIN Attendance a ON a.StudentID = s.ID
```

### Asterisks (Wildcards)

Never use an asterisk as a table wildcard (e.g. `SELECT *`) in production code.

Asterisks are restricted to regular expressions, text parsing (e.g. `LIKE`), and multiplication.

## References

-   **Kickstarter** https://gist.github.com/fredbenenson/7bb92718e19138c20591
-   **Mozilla** https://docs.telemetry.mozilla.org/concepts/sql_style.html
-   **SQLStyle.Guide** https://www.sqlstyle.guide/
-   **Dat Le** https://medium.com/@lenguyenthedat/sql-style-guide-9d2d1ae805d2
-   **PharmaSUG** https://www.pharmasug.org/proceedings/2014/TT/PharmaSUG-2014-TT05.pdf
-   **GitLab** https://about.gitlab.com/handbook/business-ops/data-team/sql-style-guide/
-   **ApexSQL** https://solutioncenter.apexsql.com/sql-formatting-standards-capitalization-indentation-comments-parenthesis/
-   **C2** http://wiki.c2.com/?SqlCodingStyle
-   **Baron Schwartz** https://www.xaprb.com/blog/2006/04/26/sql-coding-standards/
-   **Pedantic SQL** https://czep.net/15/pedantic-sql.html
-   **PEP8** (not technically SQL but whatever) https://pep8.org/
