# How to Excel at Spreadsheets

There are currently two courses: **Intermediate** and **Advanced**. Each course consists of four _modules_, which are expected to be taken consecutively.

To earn the coveted **Certificate of Excel-lence**, you must complete a _performance task_, which can be requested from Adam.

## Intermediate Course
All Intermediate materials take place in Microsoft Excel.

### [Review of the Basics](./How to Excel at Spreadsheets/Intermediate/1-basics.md)
A review of how to sort, filter, and format data using Excel's user interface.

### [Lookup Functions](./How to Excel at Spreadsheets/Intermediate/2-lookups.md)
How to look up data using `VLOOKUP()`, `HLOOKUP()`, and the `INDEX()/MATCH()` pair, including exact and inexact matches.

### [Aggregation Functions](./How to Excel at Spreadsheets/Intermediate/3-aggregation.md)
Conditionally counting, summing, and averaging data using `COUNTIFS()`, `SUMIFS()`, and `AVERAGEIFS()`; counting blanks using both `COUNTIFS()` and `COUNTBLANK()`; and formulating subtotals using `SUMPRODUCT()`.

### [Text Functions](./How to Excel at Spreadsheets/Intermediate/4-text.md)
Extraction of text using `LEFT()`, `RIGHT()`, `MID()` and `SEARCH()`; formatting using `TRIM()`, `UPPER()`, `LOWER()`, and `PROPER()`; and concatenation using `&`.

## Advanced Course
All Advanced materials take place in Google Sheets, owing to the use of array formulas.

!!! note "Prerequsite"

    Successful completion of the Intermediate course, or its performance task, should be considered a prerequisite.

### [Array Literals](./How to Excel at Spreadsheets/Advanced/1-arrays.md)
Basic use of arrays to stack sets of data on top of or next to one another.

### [Date and Time Functions](./How to Excel at Spreadsheets/Advanced/2-datetime.md)
Converting between text and datetime objects; calculating durations between dates or times, with and without excluded dates.

### [Advanced Lookup](./How to Excel at Spreadsheets/Advanced/3-advancedlookup.md)
Use of array literals to look up values to the left of the key column or return multiple values; uses of `INDEX()/MATCH()` to perform multi-dimensional lookups.

### [Filter Functions](./How to Excel at Spreadsheets/Advanced/4-filter.md)
Functionally filtering and sorting data; auto-filling formulas based on criteria.
