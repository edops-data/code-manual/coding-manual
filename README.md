# Coding Manual

EdOps internal practices and guides for code Management

## Documentation has moved
Please visit [https://edops-data.gitlab.io/code-manual/coding-manual/](https://edops-data.gitlab.io/code-manual/coding-manual/)

## Changelog

#### 2024-09-10
#### Added
-   DBeaver Installation instructions
#### Changed
-   Updated OpenVPN MSI installer
-   Removed Doc Tales

#### 2022-10-14
#### Added
-   Excel `.text` instructions

#### 2022-08-17
#### Changed
-   Deprecate Atom
-   Update OpenVPN Config

#### 2022-03-24
#### Changed
-   Deprecate SonicWall VPN

#### 2022-03-22
#### Added
-   DocTales

#### 2022-01-27
#### Added
-   SQL Training Module 5

### 2022-01-19
#### Changed
-   F5 VPN Installation Instructions

### 2022-01-12
#### Added
-   SQL Training Modules 3-4

### 2021-11-19
#### Added
-   SQL Training Module 2

### 2021-01-13
#### Added
-   GitLab MFA

### 2020-09-21
#### Added
-   Video links for Left Lookup and Lookup with Multiple Keys

### 2020-09-17
#### Changed
-   Style guide rules on line breaks

### 2020-08-11
#### Added
-   Video links for Array Literals and Basic Filter

### 2020-08-03
#### Added
-   Spreadsheet Function Guide

### 2020-07-24
#### Added
-   Spreadsheet Function Guide (shell)

### 2020-07-23
#### Added
-   Installation guide for PGAdmin, SSMS
-   Spreadsheet Function Library

### 2020-07-22
#### Added
-   Installation guides for Anaconda, Git, Atom, F5, SQLDeveloper, SonicWall
#### Changed
-   SQL Style Guide moved to docs
-   Index renamed to TOC for navigation

### 2020-07-21
#### Changed
-   Port to MkDocs
